﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Vuforia_control : MonoBehaviour {

	public Camera	ARCamera;
	[Range(0.1f,10)]
	public float MaxCameraNearPlane;
	public Slider	ARCameraSlider;

	public Toggle	ModeSwitcher;
	public DeployStageOnce		_DeployStageOnce;

	bool	SliceOn;

	void Start () {
		SliceMode(false);
	}

	void OnEnable()
	{
		ARCameraSlider.onValueChanged.AddListener(OnSliderValueChange);
		ModeSwitcher.onValueChanged.AddListener(SliceMode);
	}

	void OnDisable(){
		ARCameraSlider.onValueChanged.RemoveAllListeners();
		ModeSwitcher.onValueChanged.RemoveAllListeners();
	}
	void OnSliderValueChange(float val){

		ARCamera.nearClipPlane=Mathf.Clamp(val*MaxCameraNearPlane,0.1f,MaxCameraNearPlane);
	}
	public void OnInteractiveHitTest(HitTestResult result)
    {
		if (!SliceOn){
			_DeployStageOnce.OnInteractiveHitTest(result);
		}
	}
	void SliceMode(bool Status){
		SliceOn=Status;
		ARCameraSlider.gameObject.SetActive(Status);

	}

}
