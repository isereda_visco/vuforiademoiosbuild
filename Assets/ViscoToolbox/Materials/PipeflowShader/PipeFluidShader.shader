﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PipeFluidShader" 
{
	Properties {
		_MainTex ("Base layer (RGB)", 3D) = "white" {}
		_3DTextureZspeed("Morphing Speed", Range(0.0, 10.0)) = 1.0
		//_DetailTex ("2nd layer (RGB)", 2D) = "white" {}
		_ScrollX ("Base layer Scroll speed X", Float) = 1.0
		_ScrollY ("Base layer Scroll speed Y", Float) = 0.0
		//_Scroll2X ("2nd layer Scroll speed X", Float) = 1.0
		//_Scroll2Y ("2nd layer Scroll speed Y", Float) = 0.0
		_BaseColor ("BaseColor", Color) = ( 0.3, 0.3, 1.0, 1.0)
		_Alpha ("Alpha", Range(0.0, 1.0)) = 1.0
	

		brightness ("Brightness", Range(0.0, 10.0)) = 1.0
		saturation ("saturation", Range(0.0, 10.0)) = 1.0
		contrast   ("contrast", Range(0.0, 10.0)) = 1.0


		// Specular
		minReflection ("Min Reflection N/A", Range(0.0,1.0)) = 0.35
		maxReflection ("Max Reflection N/A", Range(0.0,1.0)) = 0.55
		SpecularPercent ("Specular percent", Range(0.0,5.0)) = 0.35

		SpecularOffset ("SpecularOffset", Range( -1.0, 1.0)) = 0
		SpecularMultiplier ("SpecularMultiplier", Range(-5.0, 5.0)) = 1.0

//		TurbulenceOffset ("TurbulenceOffset", Range(-1.0 , 1.0)) = 0
//		TurbulenceMultiplier ("TurbulenceMultiplier", Range(-5.0 , 5.0)) = 1


		// exposedField SFNode SpecularEnvMap IS specularMap
		SpecularEnvMap ("SpecularEnvMap", Cube) = "white" {}



	}
	SubShader 
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off
		Fog { Mode Off }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100
		CGINCLUDE
		#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
		#include "UnityCG.cginc"

		sampler3D _MainTex;
		float _3DTextureZspeed;
	//	float _Slice;
		//sampler2D _DetailTex;
		float4 _MainTex_ST;
		//float4 _DetailTex_ST;
		float _ScrollX;
		float _ScrollY;
		//float _Scroll2X;
		//float _Scroll2Y;
		half4 _BaseColor;
		float _Alpha;


		float brightness;
		float saturation;
		float contrast;

		
		float minReflection;
		float maxReflection;       
		float SpecularPercent;
		//TextureCube SpecularEnvMap;
		samplerCUBE SpecularEnvMap;


//		float TurbulenceMultiplier;  we have bri/sat/contrast for turbulence anyway
//		float TurbulenceOffset;
		float SpecularMultiplier;
		float SpecularOffset;




		struct vertexOutput {
			float4 pos : SV_POSITION;
			float3 uv : TEXCOORD0;
			//float2 uv2 : TEXCOORD1;
			fixed4 color : TEXCOORD1;
			float3 reflectionDir : TEXCOORD2;
			float3 ViewDirection: TEXCOORD3;
			float3 EyeSpaceNormal: TEXCOORD4;


		};

		vertexOutput vert (appdata_full v)
		{
			vertexOutput output;
			output.pos = UnityObjectToClipPos(v.vertex);
			output.uv.xy = TRANSFORM_TEX(v.texcoord.xy,_MainTex) + frac(float2(_ScrollX, _ScrollY) * _Time);
			output.uv.z = frac( _SinTime * _3DTextureZspeed); //_Slice;
			//output.uv2 = TRANSFORM_TEX(v.texcoord.xy,_DetailTex) + frac(float2(_Scroll2X, _Scroll2Y) * _Time);
			//output.color =  fixed4(_Intensity, _Intensity, _Intensity, _Alpha);
   		   output.color =  _BaseColor; //  fixed4(_Intensity.x, _Intensity.y, _Intensity.z, _Alpha);


            // compute world space position of the vertex
            float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
            // compute world space view direction
            float3 worldViewDir = normalize(UnityWorldSpaceViewDir(worldPos));
            // world space normal
            float3 worldNormal = UnityObjectToWorldNormal(v.normal);
            // world space reflection vector
            output.reflectionDir = reflect(-worldViewDir, worldNormal);


			output.EyeSpaceNormal= normalize(v.normal);
			output.ViewDirection = worldViewDir;


			return output;
		}
		ENDCG

		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest


               float4 turbulence4(sampler3D NoiseMap, float3 P)
                {
	                float4 sum = tex3D(NoiseMap, P)*0.5 +
 				        tex3D(NoiseMap, P*2.0)*0.25 +
 				        tex3D(NoiseMap, P*4.0)*0.125 +
				        tex3D(NoiseMap, P*8.0)*0.0625;
	                return  sum;
                }


                float3 ContrastSaturationBrightness(float3 color, float brt, float sat, float con)
                {
	                // Increase or decrease theese values to adjust r, g and b color channels seperately
	                const float AvgLumR = 0.5;
	                const float AvgLumG = 0.5;
	                const float AvgLumB = 0.5;

	                const float3 LumCoeff = float3(0.2125, 0.7154, 0.0721);

	                float3 AvgLumin = float3(AvgLumR, AvgLumG, AvgLumB);
	                float3 brtColor = color * brt;
	                float3 intensity = dot(brtColor, LumCoeff);
	                float3 satColor = lerp(intensity, brtColor, sat);
	                float3 conColor = lerp(AvgLumin, satColor, con);
	                return conColor;
                }




			fixed4 frag (vertexOutput input) : COLOR
			{
				fixed4 o;

				float4 turbulenceColor = turbulence4(_MainTex, input.uv);
				//turbulenceColor = turbulenceColor * TurbulenceMultiplier + TurbulenceOffset;
				

                float3 adjustedTurbulenceColor = ContrastSaturationBrightness(turbulenceColor, brightness, saturation, contrast);

				float3 envReflectColor  = texCUBE(SpecularEnvMap,  input.reflectionDir);
				envReflectColor = envReflectColor * SpecularMultiplier + SpecularOffset;
				
				
				
				float NdotL = 1.0 - max(dot(input.EyeSpaceNormal,input.ViewDirection),0.0);

				//envReflectColor = envReflectColor;


				//half3 turbulenceAndReflectionColor = lerp(turbulenceColor.rgb, envReflectColor, SpecularPercent);
				//float3 turbulenceAndReflectionColor = lerp(adjustedTurbulenceColor, envReflectColor,  NdotL * SpecularPercent);
				float3 turbulenceAndReflectionColor = (1.0 - SpecularPercent) * adjustedTurbulenceColor + envReflectColor * NdotL; // lerp(adjustedTurbulenceColor, envReflectColor,  NdotL * SpecularPercent);
				
				//half3 adjustedTurbulenceAndReflectionColor = ContrastSaturationBrightness(turbulenceAndReflectionColor.rgb, brightness, saturation, contrast);


				o = float4(turbulenceAndReflectionColor, turbulenceColor.a) * input.color;
				return o;
			}
			ENDCG
		}

	}

}