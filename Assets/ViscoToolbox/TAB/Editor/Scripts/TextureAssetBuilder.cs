﻿// TAB Version 1.2.1
// - Added saving/loading of settings in EditorPrefs
// - Bug Fix (Compressed Texture2D with mip maps didn't save properly)
// 
// TAB Version 1.2
// - Added multi-texture2D import to Texture Constructor
// -- Modified shortcuts for +/- buttons in Texture Constructor
// --- (no modifier) +: Add a copy of selected Layer (blank with no Layer selected)
// --- (ctrl) +: Add Asset Selection to Texture Constructor
// --- (alt) +: Add blank Layer
// --- (no modifier) -: Remove selected Layer (with confirmation if layer is not blank)
// --- (shift) -: Remove selected Layer without confirmation
// --- (alt) -: Clear all layers (confirmation required)
// - Fixed overwriting files breaking references
// 
// Previous updates:
// TAB Version 1.1
// - Added Drag and Drop file name support

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace TextureAssetBuilder
{
	public class TAB : EditorWindow
	{
		private static ViewerWindow viewerWindow;

		public enum EligibleTextureFormat
		{
			Alpha8 = TextureFormat.Alpha8,
			ARGB4444 = TextureFormat.ARGB4444,
			RGB24 = TextureFormat.RGB24,
			RGBA32 = TextureFormat.RGBA32,
			ARGB32 = TextureFormat.ARGB32,
			RGB565 = TextureFormat.RGB565,
			RGBA4444 = TextureFormat.RGBA4444,
			BGRA32 = TextureFormat.BGRA32,
			RHalf = TextureFormat.RHalf,
			RGHalf = TextureFormat.RGHalf,
			RGBAHalf = TextureFormat.RGBAHalf,
			RFloat = TextureFormat.RFloat,
			RGFloat = TextureFormat.RGFloat,
			RGBAFloat = TextureFormat.RGBAFloat
		}
		public enum UncompressedTextureFormat
		{
			Alpha8 = TextureFormat.Alpha8,
			ARGB4444 = TextureFormat.ARGB4444,
			RGB24 = TextureFormat.RGB24,
			RGBA32 = TextureFormat.RGBA32,
			ARGB32 = TextureFormat.ARGB32,
			RGB565 = TextureFormat.RGB565,
			R16 = TextureFormat.R16,
			RGBA4444 = TextureFormat.RGBA4444,
			BGRA32 = TextureFormat.BGRA32,
			RHalf = TextureFormat.RHalf,
			RGHalf = TextureFormat.RGHalf,
			RGBAHalf = TextureFormat.RGBAHalf,
			RFloat = TextureFormat.RFloat,
			RGFloat = TextureFormat.RGFloat,
			RGBAFloat = TextureFormat.RGBAFloat
		}

		private enum FileExtension { ASSET, PNG, TGA }
		private static readonly string[] fileExtensionNames = new string[] { ".asset", ".png", ".tga" };
		private FileExtension extension = FileExtension.ASSET;
		private int iFileExtensionSelection = 0;
		private int fileExtensionSelection
		{
			get
			{
				return iFileExtensionSelection;
			}
			set
			{
				if(value != iFileExtensionSelection)
				{
					iFileExtensionSelection = value;
					extension = (FileExtension)value;
				}
			}
		}

		private enum ResizeTextureMode { NO_SCALE, SCALE_UP, SCALE_DOWN, SCALE_TO_0, EXPLICIT }
		private static readonly string[] resizeTextureNames = new string[] { "Don't Resize", "Scale Up to Match Largest", "Scale Down to Match Smallest", "Scale to Match Layer 0", "User Defined" };
		private ResizeTextureMode resizeMode = ResizeTextureMode.NO_SCALE;
		private int iResizeTextureSelection = 0;
		private int resizeTextureSelection
		{
			get
			{
				return iResizeTextureSelection;
			}
			set
			{
				if(value != iResizeTextureSelection)
				{
					if(resizeMode == ResizeTextureMode.EXPLICIT)
					{
						lastResizeX = resizeX;
						lastResizeY = resizeY;
					}
					iResizeTextureSelection = value;
					resizeMode = (ResizeTextureMode)value;
					if(resizeMode == ResizeTextureMode.EXPLICIT)
					{
						UpdateResizeValuesToLast();
					}
					ResetResizeValues();
				}
			}
		}
		private bool bResizePowerOfTwo = true;
		private bool resizePowerOfTwo
		{
			get
			{
				return bResizePowerOfTwo;
			}
			set
			{
				if(value != bResizePowerOfTwo)
				{
					bResizePowerOfTwo = value;
					if(bResizePowerOfTwo)
					{
						iResizeX = NextPowerOfTwo(iResizeX);
						iResizeY = NextPowerOfTwo(iResizeY);
					}
				}
			}
		}
		private int iResizeX = 256;
		private int resizeX
		{
			get
			{
				return iResizeX;
			}
			set
			{
				if(value != iResizeX)
				{
					if(resizePowerOfTwo)
					{
						iResizeX = NextPowerOfTwo(value);
					}
					else
					{
						iResizeX = value;
					}
				}
			}
		}
		private int iResizeY = 256;
		private int resizeY
		{
			get
			{
				return iResizeY;
			}
			set
			{
				if(value != iResizeY)
				{
					if(resizePowerOfTwo)
					{
						iResizeY = NextPowerOfTwo(value);
					}
					else
					{
						iResizeY = value;
					}
				}
			}
		}
		private int lastResizeX = 256;
		private int lastResizeY = 256;

		private enum ResizeFilter { NEAREST_NEIGHBOR, BILINEAR }
		private static readonly string[] resizeFilterNames = new string[] { "Nearest Neighbor", "Bilinear Interpolation" };
		private ResizeFilter resizeFilter = ResizeFilter.BILINEAR;
		private int iResizeFilterSelection = 1;
		private int resizeFilterSelection
		{
			get
			{
				return iResizeFilterSelection;
			}
			set
			{
				if(value != iResizeFilterSelection)
				{
					iResizeFilterSelection = value;
					resizeFilter = (ResizeFilter)value;
				}
			}
		}

		private enum ResizeWrap { AS_IS, REPEAT, CLAMP }
		private static readonly string[] resizeWrapNames = new string[] { "Use Individual Texture Wrap Mode", "Always Repeat", "Always Clamp" };
		private ResizeWrap resizeWrap = ResizeWrap.AS_IS;
		private int iResizeWrapSelection = 0;
		private int resizeWrapSelection
		{
			get
			{
				return iResizeWrapSelection;
			}
			set
			{
				if(value != iResizeWrapSelection)
				{
					iResizeWrapSelection = value;
					resizeWrap = (ResizeWrap)value;
				}
			}
		}

		private static EditorWindow thisWindow;
		private static readonly float windowMinHeight = 427;
		private static readonly float windowWidth = 656;
		private static readonly float listWidth = 258;
		private static readonly float detailsWidth = windowWidth - listWidth;
		private float prefixWidth = 146;
		private System.Collections.Generic.List<Texture2D> txlLayers;
		private Vector2 scrollPosition;
		private ReorderableList roList;
		private Rect roListFooterRect = new Rect();
		private bool roListFooterHide = true;
		
		private Object assetSelection;
		private Texture txSelectionBase;
		private Texture txSelection
		{
			get
			{
				return txSelectionBase;
			}
			set
			{
				if(value != txSelectionBase)
				{
					txSelectionBase = value;
					if(txlLayers == null)
					{
						txlLayers = new System.Collections.Generic.List<Texture2D>();
					}
					else
					{
						for(int i = txlLayers.Count - 1; i >= 0; i--)
						{
							if(txlLayers[i] != null && !AssetDatabase.Contains(txlLayers[i]))
							{
								DestroyImmediate(txlLayers[i], false);
							}
						}
						txlLayers.Clear();
						errorState = 0;
						roListFooterHide = true;
					}
					if(txSelectionBase != null)
					{
						InitializeTextureData(txSelectionBase);
						ResetResizeValues();
					}
				}
			}
		}

		private bool bUnlockSelection = true;
		private bool unlockSelection
		{
			get
			{
				return bUnlockSelection;
			}
			set
			{
				if(value != bUnlockSelection)
				{
					bUnlockSelection = value;
					OnSelectionChange();
				}
			}
		}

		private const int ERROR_NULL_TEXTURE = 1 << 0;
		private const int ERROR_DIMENSION_MISMATCH = 1 << 1;
		private const int ERROR_NON_POWER_OF_TWO = 1 << 2;
		private const int WARNING_COMPRESSED_INPUT = 1 << 3;

		private int iErrorState = 0;
		private int errorState
		{
			get
			{
				return iErrorState;
			}
			set
			{
				if(value != iErrorState)
				{
					iErrorState = value;
					sErrorText = GetError(iErrorState);
				}
			}
		}

		private string sErrorText;

		private string fileName = "";
		private int fileNamePadding = 4;
		private int txAnisoLevel = 0;
		private FilterMode txFilterMode = FilterMode.Bilinear;
		private EligibleTextureFormat txTextureFormat = EligibleTextureFormat.ARGB32;
		private TextureWrapMode txTextureWrapMode = TextureWrapMode.Repeat;
		private bool txGenerateMipMaps = false;
		private bool txMakeReadable = true;
		private bool txCompressImage = false;

		private static readonly string[] buttonTooltips = new string[] { "", "Only available as *.asset file", "Power-of-2 layer count is required for Texture3D" };

		private int iDivisionsX = 1;
		private int iDivisionsY = 1;

		Rect txSelectionRect;

		// --- Drag and Drop File Name ---
		bool bDragging = false;
		// -------------------------------

		[MenuItem("Tools/Texture Asset Builder")]
		public static void ShowWindow()
		{
			thisWindow = EditorWindow.GetWindow<TAB>(true, "Texture Asset Builder", true);
			thisWindow.minSize = new Vector2(windowWidth, windowMinHeight);
			thisWindow.maxSize = new Vector2(windowWidth, thisWindow.maxSize.y);
		}

		void OnEnable()
		{
			if(txlLayers == null)
			{
				txlLayers = new System.Collections.Generic.List<Texture2D>();
			}

			if(roList == null)
			{
				roList = new ReorderableList(txlLayers, typeof(Texture2D), true, true, true, true);
				roList.elementHeight = 66;
				roList.drawHeaderCallback = new ReorderableList.HeaderCallbackDelegate(DrawHeader);
				roList.drawElementCallback = new ReorderableList.ElementCallbackDelegate(DrawTextureElement);
				roList.onAddCallback = new ReorderableList.AddCallbackDelegate(AddTextureElement);
				roList.onRemoveCallback = new ReorderableList.RemoveCallbackDelegate(RemoveTextureElement);
				roList.drawFooterCallback = new ReorderableList.FooterCallbackDelegate(DrawFooter);
			}

			if(assetSelection == null)
			{
				OnSelectionChange();
			}
		}
		
		void Awake()
		{
			if(EditorPrefs.HasKey("TAB_UnlockSelection"))
			{
				bUnlockSelection = EditorPrefs.GetBool("TAB_UnlockSelection");
			}

			if(EditorPrefs.HasKey("TAB_ResizePowerOfTwo"))
			{
				bResizePowerOfTwo = EditorPrefs.GetBool("TAB_ResizePowerOfTwo");
			}

			if(EditorPrefs.HasKey("TAB_ResizeX"))
			{
				lastResizeX = EditorPrefs.GetInt("TAB_ResizeX");
			}

			if(EditorPrefs.HasKey("TAB_ResizeY"))
			{
				lastResizeY = EditorPrefs.GetInt("TAB_ResizeY");
			}
			UpdateResizeValuesToLast();

			if(EditorPrefs.HasKey("TAB_ResizeFilterSelection"))
			{
				resizeFilterSelection = EditorPrefs.GetInt("TAB_ResizeFilterSelection");
			}

			if(EditorPrefs.HasKey("TAB_ResizeWrapSelection"))
			{
				resizeWrapSelection = EditorPrefs.GetInt("TAB_ResizeWrapSelection");
			}

			if(EditorPrefs.HasKey("TAB_ResizeTextureSelection"))
			{
				resizeTextureSelection = EditorPrefs.GetInt("TAB_ResizeTextureSelection");
			}

			if(EditorPrefs.HasKey("TAB_FileName"))
			{
				fileName = EditorPrefs.GetString("TAB_FileName");
			}

			if(EditorPrefs.HasKey("TAB_FileExtensionSelection"))
			{
				fileExtensionSelection = EditorPrefs.GetInt("TAB_FileExtensionSelection");
            }

			if(EditorPrefs.HasKey("TAB_FileNamePadding"))
			{
				fileNamePadding = EditorPrefs.GetInt("TAB_FileNamePadding");
			}
		}

		void OnDestroy()
		{
			EditorPrefs.SetBool("TAB_UnlockSelection", bUnlockSelection);
			if(resizeMode == ResizeTextureMode.EXPLICIT)
			{
				EditorPrefs.SetInt("TAB_ResizeX", resizeX);
				EditorPrefs.SetInt("TAB_ResizeY", resizeY);
			}
			else
			{
				EditorPrefs.SetInt("TAB_ResizeX", lastResizeX);
				EditorPrefs.SetInt("TAB_ResizeY", lastResizeY);
			}
			EditorPrefs.SetBool("TAB_ResizePowerOfTwo", bResizePowerOfTwo);
			EditorPrefs.SetInt("TAB_ResizeFilterSelection", iResizeFilterSelection);
			EditorPrefs.SetInt("TAB_ResizeWrapSelection", iResizeWrapSelection);
			EditorPrefs.SetInt("TAB_ResizeTextureSelection", iResizeTextureSelection);
			EditorPrefs.SetString("TAB_FileName", fileName);
			EditorPrefs.SetInt("TAB_FileExtensionSelection", iFileExtensionSelection);
			EditorPrefs.SetInt("TAB_FileNamePadding", fileNamePadding);
		}

		void DrawHeader(Rect rect)
		{
			EditorGUI.LabelField(rect, "Texture Constructor (drag to reorder)");

			if(!roListFooterHide)
			{
				ReorderableList.defaultBehaviours.DrawFooter(roListFooterRect, roList);
			}
			else if(Event.current.type == EventType.Repaint)
			{
				roListFooterHide = false;
			}
		}

		void DrawTextureElement(Rect rect, int index, bool selected, bool focused)
		{
			rect.x = 26;
			rect.width = 64;
			rect.height = 64;
			Texture2D tex = txlLayers[index];
			tex = (Texture2D)EditorGUI.ObjectField(rect, tex, typeof(Texture2D), false);
			if(tex != txlLayers[index])
			{
				txlLayers[index] = tex;
				ResetResizeValues();
			}
			rect.x = 100;
			rect.y += 4;
			rect.width = 72;
			rect.height = 16;
			EditorGUI.LabelField(rect, ("Layer " + index));
			if(txlLayers[index] != null)
			{
				rect.y += 20;
				rect.width = 102;
				EditorGUI.LabelField(rect, (txlLayers[index].width + "x" + txlLayers[index].height));
				if(!System.Enum.IsDefined(typeof(UncompressedTextureFormat), (UncompressedTextureFormat)txlLayers[index].format))
				{
					rect.y += 20;
					EditorGUI.LabelField(rect, "Compressed");
				}
			}
		}

		void AddTextureElement(ReorderableList list)
		{
			EventModifiers mod = Event.current.modifiers;
			if((mod == EventModifiers.Control || mod == EventModifiers.Command))
			{
				Object[] selectedObjects = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
				int soLength = selectedObjects.Length;
				string[] fullpathtest = new string[soLength];
				for(int i = 0; i < soLength; i++)
				{
					fullpathtest[i] = AssetDatabase.GetAssetPath(selectedObjects[i]);
				}
				System.Array.Sort(fullpathtest, selectedObjects);
				for(int i = 0; i < soLength; i++)
				{
					list.list.Add((Texture2D)selectedObjects[i]);
				}
			}
			else if(mod == EventModifiers.Alt || list.index < 0)
			{
				list.list.Add(null);
				list.index = list.list.Count - 1;
			}
			else if(list.index < list.list.Count && list.index >= 0)
			{
				list.list.Add(list.list[list.index]);
				list.index = list.list.Count - 1;
			}
			roListFooterHide = true;
		}

		void RemoveTextureElement(ReorderableList list)
		{
			if(Event.current.modifiers == EventModifiers.Alt)
			{
				if(EditorUtility.DisplayDialog("Clear Layers", "Are you sure you want to clear all Layers?", "Yes", "No"))
				{
					list.list.Clear();
					list.index = -1;
				}
				thisWindow.Focus();
			}
			else if(list.serializedProperty != null)
			{
                if(Event.current.modifiers != EventModifiers.Shift)
				{
					if(EditorUtility.DisplayDialog("Remove Element", "Are you sure you want to remove Layer " + list.index + "?\n\n(hold shift to auto-confirm or alt to clear all)", "Yes", "No"))
					{
						list.serializedProperty.DeleteArrayElementAtIndex(list.index);
						if(list.index >= list.serializedProperty.arraySize - 1)
						{
							list.index = list.serializedProperty.arraySize - 1;
						}
					}
					thisWindow.Focus();
				}
			}
			else
			{
				if(list.list[list.index] == null || Event.current.modifiers == EventModifiers.Shift || EditorUtility.DisplayDialog("Remove Element", "Are you sure you want to remove Layer " + list.index + "?\n\n(hold shift to auto-confirm)", "Yes", "No"))
				{
					list.list.RemoveAt(list.index);
					if(list.index >= list.list.Count - 1)
					{
						list.index = list.list.Count - 1;
					}
				}
				thisWindow.Focus();
			}
			roListFooterHide = true;
		}

		void DrawFooter(Rect rect)
		{
			if((rect.y + rect.height) > thisWindow.position.height + scrollPosition.y)
			{
				rect.y = thisWindow.position.height + scrollPosition.y - rect.height;
			}
			if(rect.width > 1)
			{
				roListFooterRect = rect;
			}
			ReorderableList.defaultBehaviours.DrawFooter(rect, roList);
		}

		void OnGUI()
		{
			if(thisWindow == null)
			{
				thisWindow = EditorWindow.GetWindow<TAB>();
				OnEnable();
			}

			Event e = Event.current;
			EventType et = e.type;
			if(et == EventType.DragUpdated)
			{
				bDragging = true;
			}
			else if(et == EventType.DragExited)
			{
				bDragging = false;
			}

			EditorGUILayout.BeginHorizontal();
			{
				txSelectionRect = new Rect(GUI.skin.window.margin.left + GUI.skin.label.margin.left, GUI.skin.window.margin.top + GUI.skin.label.margin.top + 18, detailsWidth - (GUI.skin.window.margin.horizontal + GUI.skin.label.margin.horizontal), 64);
				txSelection = (Texture)EditorGUI.ObjectField(txSelectionRect, "Selected Asset", txSelection, typeof(Texture), false);
				if(txSelection != null)
				{
					GUIContent textureName = new GUIContent(txSelection.name);
					Vector2 textureNameSize = GUI.skin.label.CalcSize(textureName);
					//float textureNameWidth = Mathf.Min(textureNameSize.x, (detailsWidth - prefixWidth - GUI.skin.button.margin.left - GUI.skin.window.padding.left) - 68);
					float textureNameWidth = Mathf.Min(textureNameSize.x, (detailsWidth - prefixWidth - GUI.skin.button.margin.left - GUI.skin.window.padding.left - 24));
					//float textureNamePosition = Mathf.Min(textureNameSize.x + 68, (detailsWidth - prefixWidth - GUI.skin.button.margin.left - GUI.skin.window.padding.left));
					float textureNamePosition = Mathf.Min(textureNameSize.x, (detailsWidth - prefixWidth - GUI.skin.button.margin.left - GUI.skin.window.padding.left - 24));
					//EditorGUI.LabelField(new Rect(lastRect.x + lastRect.width - textureNamePosition, controlRect.y, textureNameWidth, textureNameSize.y), textureName);
					EditorGUI.LabelField(new Rect(txSelectionRect.x + txSelectionRect.width - textureNamePosition, txSelectionRect.y - 18, textureNameWidth, textureNameSize.y), textureName);
					GUI.enabled = !bDragging;

					if(GUI.Button(new Rect(txSelectionRect.x + txSelectionRect.width - 158, txSelectionRect.y + 24, 90, textureNameSize.y), "View Full-Size"))
					{
						int width = txSelection.width;
						if(viewerWindow == null)
						{
							viewerWindow = ScriptableObject.CreateInstance<ViewerWindow>();
						}
						else
						{
							viewerWindow.Close();
							viewerWindow = ScriptableObject.CreateInstance<ViewerWindow>();
						}
						viewerWindow.titleContent = new GUIContent("Full-Size View");
						viewerWindow.txTexture = txSelection;
						viewerWindow.ShowUtility();
						Vector2 windowWidthHeight;
						if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
						{
							if(((Texture2D)txSelection).mipmapCount > 1)
							{
								width = (int)(width * 1.5f) + 2;
							}
							windowWidthHeight = new Vector2(Mathf.Min(width + GUI.skin.label.padding.horizontal, (Screen.currentResolution.width * 0.75f)), Mathf.Min(txSelection.height + 18 + GUI.skin.label.padding.horizontal, (Screen.currentResolution.height * 0.75f)));
							viewerWindow.position = new Rect(Mathf.Max(0, Mathf.Min(thisWindow.position.x, Screen.currentResolution.width - windowWidthHeight.x)), Mathf.Min(txSelectionRect.y + 68 + thisWindow.position.y, Screen.currentResolution.height - windowWidthHeight.y), windowWidthHeight.x, windowWidthHeight.y);
						}
						else if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
						{
							windowWidthHeight = new Vector2(Mathf.Min(width + GUI.skin.label.padding.horizontal, (Screen.currentResolution.width * 0.75f)), Mathf.Min(txSelection.height + 40 + GUI.skin.label.padding.horizontal, (Screen.currentResolution.height * 0.75f)));
							viewerWindow.position = new Rect(Mathf.Max(0, Mathf.Min(thisWindow.position.x, Screen.currentResolution.width - windowWidthHeight.x)), Mathf.Min(txSelectionRect.y + 68 + thisWindow.position.y, Screen.currentResolution.height - windowWidthHeight.y), windowWidthHeight.x, windowWidthHeight.y);
						}
						else// if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
						{
							windowWidthHeight = new Vector2(Mathf.Min(width + GUI.skin.label.padding.horizontal, (Screen.currentResolution.width * 0.75f)), Mathf.Min(txSelection.height + 40 + GUI.skin.label.padding.horizontal, (Screen.currentResolution.height * 0.75f)));
							viewerWindow.position = new Rect(Mathf.Max(0, Mathf.Min(thisWindow.position.x, Screen.currentResolution.width - windowWidthHeight.x)), Mathf.Min(txSelectionRect.y + 68 + thisWindow.position.y, Screen.currentResolution.height - windowWidthHeight.y), windowWidthHeight.x, windowWidthHeight.y);
						}
						viewerWindow.Repaint();
						viewerWindow.Focus();
					}
					if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
					{
						if(iDivisionsX == iDivisionsY && iDivisionsX == 1)
						{
							GUI.enabled = false;
						}
						if(GUI.Button(new Rect(txSelectionRect.x + txSelectionRect.width - 250, txSelectionRect.y + 24, 90, textureNameSize.y), new GUIContent("Slice Texture", "Divide the texture into even slices on the X and Y axes")))
						{
							if(e.modifiers != EventModifiers.Shift)
							{
								if(txlLayers.Count > 1)
								{
									if(!EditorUtility.DisplayDialog("Confirm Layer Replace", "Are you sure you want to overwrite existing layers?\n\n(hold shift to auto-confirm)", "Yes", "No"))
									{
										thisWindow.Focus();
										return;
									}
									thisWindow.Focus();
								}
							}
							DivideTexture((Texture2D)txSelection, iDivisionsX, iDivisionsY);
						}
						GUI.enabled = !bDragging;
						EditorGUI.LabelField(new Rect(txSelectionRect.x + txSelectionRect.width - 250, txSelectionRect.y + 6, 13, textureNameSize.y), "X");
						iDivisionsX = Mathf.Max(EditorGUI.IntField(new Rect(txSelectionRect.x + txSelectionRect.width - 239, txSelectionRect.y + 6, 34, textureNameSize.y), iDivisionsX), 1);
						EditorGUI.LabelField(new Rect(txSelectionRect.x + txSelectionRect.width - 205, txSelectionRect.y + 6, 13, textureNameSize.y), "Y");
						iDivisionsY = Mathf.Max(EditorGUI.IntField(new Rect(txSelectionRect.x + txSelectionRect.width - 194, txSelectionRect.y + 6, 34, textureNameSize.y), iDivisionsY), 1);
						EditorGUI.LabelField(new Rect(txSelectionRect.x, txSelectionRect.y + 16, 108, textureNameSize.y), "(Texture2D)");
					}
					else if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
					{
						EditorGUI.LabelField(new Rect(txSelectionRect.x, txSelectionRect.y + 16, 108, textureNameSize.y), "(Texture3D)");
					}
					else if(txSelection.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
					{
						EditorGUI.LabelField(new Rect(txSelectionRect.x, txSelectionRect.y + 16, 108, textureNameSize.y), "(Texture2DArray)");
					}
				}

				GUI.enabled = true;

				GUI.BeginGroup(new Rect(detailsWidth, 0, listWidth, thisWindow.position.height));
				{
					scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.Width(258));
					{
						EditorGUILayout.BeginHorizontal();
						{
							EditorGUILayout.LabelField(GUIContent.none, GUILayout.Height(thisWindow.position.height + 0.001f), GUILayout.Width(0));
							EditorGUILayout.BeginVertical();
							{
								roList.DoLayoutList();
							}
							EditorGUILayout.EndVertical();
						}
						EditorGUILayout.EndHorizontal();
					}
					EditorGUILayout.EndScrollView();
				}
				GUI.EndGroup();

				GUI.enabled = !bDragging;

				GUI.BeginGroup(new Rect(-listWidth, 0, windowWidth + detailsWidth, thisWindow.position.height));
				{
					EditorGUILayout.BeginVertical();
					{
						unlockSelection = EditorGUILayout.Toggle(new GUIContent("Automatic Asset Selection", "Changing selection in the project will change the contents of this window"), unlockSelection);

						// Compress ObjectField position slightly for better use of space
						Rect lastRect = GUILayoutUtility.GetLastRect();
						GUILayoutUtility.GetRect(lastRect.width, 40);

						// ---

						if(extension != FileExtension.ASSET)
						{
							GUI.enabled = false;
						}
						EditorGUILayout.LabelField("New Texture Settings:");

						txMakeReadable = EditorGUILayout.Toggle("Read/Write Enabled", txMakeReadable);

						txTextureFormat = (EligibleTextureFormat)EditorGUILayout.EnumPopup("Format", txTextureFormat);

						txCompressImage = EditorGUILayout.Toggle(new GUIContent("Compress Image (DXT)", "Only available for Texture2D"), txCompressImage);

						txGenerateMipMaps = EditorGUILayout.Toggle("Generate Mip Maps", txGenerateMipMaps);

						txTextureWrapMode = (TextureWrapMode)EditorGUILayout.EnumPopup("Wrap Mode", txTextureWrapMode);

						txFilterMode = (FilterMode)EditorGUILayout.EnumPopup("Filter Mode", txFilterMode);

						if(txFilterMode == FilterMode.Point)
						{
							GUI.enabled = false;
						}
						txAnisoLevel = EditorGUILayout.IntSlider("Aniso Level", txAnisoLevel, 0, 16);
						GUI.enabled = !bDragging;

						//---

						EditorGUILayout.LabelField("", EditorStyles.objectFieldThumb, GUILayout.Height(4)); // Divider

						resizeTextureSelection = EditorGUILayout.Popup("Resize Textures", resizeTextureSelection, resizeTextureNames);

						if(resizeMode == ResizeTextureMode.EXPLICIT)
						{
							GUI.enabled = !bDragging;
						}
						else
						{
							GUI.enabled = false;
						}
						EditorGUILayout.BeginHorizontal();
						{
							resizePowerOfTwo = EditorGUILayout.Toggle("Lock to Power-of-2", resizePowerOfTwo);
							GUILayout.FlexibleSpace();
							EditorGUILayout.BeginVertical();
							{
								EditorGUILayout.LabelField("Width", GUILayout.MaxWidth(80));
								resizeX = EditorGUILayout.IntField(resizeX, GUILayout.MaxWidth(80));
							}
							EditorGUILayout.EndVertical();
							EditorGUILayout.BeginVertical();
							{
								EditorGUILayout.LabelField("Height", GUILayout.MaxWidth(80));
								resizeY = EditorGUILayout.IntField(resizeY, GUILayout.MaxWidth(80));
							}
							EditorGUILayout.EndVertical();
						}
						EditorGUILayout.EndHorizontal();

						if(resizeMode != ResizeTextureMode.NO_SCALE)
						{
							GUI.enabled = !bDragging;
						}
						resizeFilterSelection = EditorGUILayout.Popup("Texture Scaling Filter", resizeFilterSelection, resizeFilterNames);

						if(resizeFilter == ResizeFilter.NEAREST_NEIGHBOR || resizeMode == ResizeTextureMode.SCALE_DOWN)
						{
							GUI.enabled = false;
						}
						resizeWrapSelection = EditorGUILayout.Popup("Upscaling Wrap Override", resizeWrapSelection, resizeWrapNames);

						GUI.enabled = !bDragging;

						// ---

						EditorGUILayout.LabelField("", EditorStyles.objectFieldThumb, GUILayout.Height(4)); // Divider

						EditorGUILayout.PrefixLabel("Save Texture As:");
						if(GUILayoutUtility.GetLastRect().width > 1)
						{
							prefixWidth = GUILayoutUtility.GetLastRect().width;
						}

						Rect fileNameRect = EditorGUILayout.BeginHorizontal();
						{
							GUIContent pathPrefix = new GUIContent("Assets/");
							float pathPrefixWidth = GUI.skin.label.CalcSize(pathPrefix).x;
							EditorGUILayout.LabelField(pathPrefix, GUILayout.Width(pathPrefixWidth));
							GUI.SetNextControlName("FNTF"); // FileNameTextField
							GUI.enabled = true;
							fileName = EditorGUILayout.TextField(fileName);
							GUI.enabled = !bDragging;

							// --- Drag and Drop File Name ---
							Rect nameDragRect = GUILayoutUtility.GetLastRect();
							if(nameDragRect.Contains(e.mousePosition))
							{
								if(et == EventType.DragUpdated)
								{
									DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
								}
								else if(et == EventType.DragPerform)
								{
									if(DragAndDrop.paths.Length > 0)
									{
										string dragDropPath = DragAndDrop.paths[0];
										if(dragDropPath.Length > 0)
										{
											if(dragDropPath.Substring(0, 6) == "Assets")
											{
												int textPos = dragDropPath.Length - 1;
												for(int i = textPos - 1; i >= 0; i--)
												{
													if(dragDropPath[i] == '.')
													{
														textPos = i - 7;
														break;
													}
													else if(dragDropPath[i] == '/')
													{
														textPos = dragDropPath.Length - 7;
														break;
													}
                                                }
												fileName = dragDropPath.Substring(7, textPos); // Removes "Assets/" and existing file extension
											}
											else
											{
												fileName = dragDropPath; // A Fallback: If the Assets folder isn't in the string, copy whatever is available
											}
										}
									}
									DragAndDrop.AcceptDrag();
								}
							}
							// -------------------------------

							if(fileName.Length <= 0 && !string.Equals(GUI.GetNameOfFocusedControl(), "FNTF"))
							{
								GUI.enabled = false;
								if(bDragging)
								{
									EditorGUI.LabelField(new Rect(fileNameRect.x + pathPrefixWidth + GUI.skin.label.padding.horizontal + 1, fileNameRect.y, 210, 16), "<Drop File Here To Copy File Path>");
								}
								else
								{
									EditorGUI.LabelField(new Rect(fileNameRect.x + pathPrefixWidth + GUI.skin.label.padding.horizontal + 1, fileNameRect.y, 144, 16), "<Enter File Name Here>");
								}
								GUI.enabled = !bDragging;
							}
							fileExtensionSelection = EditorGUILayout.Popup(fileExtensionSelection, fileExtensionNames, GUILayout.MaxWidth(60));
						}
						EditorGUILayout.EndHorizontal();

						EditorGUILayout.BeginHorizontal();
						{
							// How many currently-listed images are eligible for use?
							int nonNullCount = 0;
							bool compressionFound = false;
							for(int i = 0; i < txlLayers.Count; i++)
							{
								if(txlLayers[i] != null)
								{
									nonNullCount++;
									if(!System.Enum.IsDefined(typeof(UncompressedTextureFormat), (UncompressedTextureFormat)txlLayers[i].format))
									{
										errorState |= WARNING_COMPRESSED_INPUT;
										compressionFound = true;
									}
								}
							}
							if(!compressionFound)
							{
								errorState &= ~WARNING_COMPRESSED_INPUT;
							}
							if(nonNullCount == txlLayers.Count)
							{
								errorState &= ~ERROR_NULL_TEXTURE;
							}
							else
							{
								errorState |= ERROR_NULL_TEXTURE;
							}

							EditorGUILayout.BeginVertical(GUILayout.Width(prefixWidth));
							{
								string paddingExample = "D" + fileNamePadding.ToString();

								if(nonNullCount > 0)
								{
									GUI.enabled = !bDragging;
								}
								else
								{
									GUI.enabled = false;
								}

								string multiFileTooltip = "File name" + (nonNullCount != 1 ? "s" : "") + " will end with " + fileNamePadding.ToString() + "-digit padding:\n" + fileName + (0).ToString(paddingExample) + fileExtensionNames[(int)extension];
								if(nonNullCount > 1)
								{
									if(nonNullCount > 2)
									{
										multiFileTooltip += "\n...";
									}
									multiFileTooltip += "\n" + fileName + (nonNullCount - 1).ToString(paddingExample) + fileExtensionNames[(int)extension];
								}

								// Create Multiple Texture 2D
								if(GUILayout.Button(new GUIContent(nonNullCount + " Texture2D File" + (nonNullCount != 1 ? "s" : ""), multiFileTooltip)))
								{
									if(fileName.Length <= 0)
									{
										EditorUtility.DisplayDialog("No File Name", "No file name was entered.\n\nUnable to save file" + (nonNullCount != 1 ? "s" : "") + ".", "OK");
										thisWindow.Focus();
									}
									else
									{
										int finalNonNullCount = 0;
										for(int i = 0; i < txlLayers.Count; i++)
										{
											if(txlLayers[i] != null)
											{
												string finalPaddingText = "D" + fileNamePadding.ToString();
												finalPaddingText = finalNonNullCount.ToString(finalPaddingText);

												if(System.IO.File.Exists(Application.dataPath + "/" + fileName + finalPaddingText + fileExtensionNames[(int)extension]))
												{
													if(e.modifiers != EventModifiers.Shift)
													{
														if(!EditorUtility.DisplayDialog("Confirm File Replace", "Are you sure you want to overwrite " + fileName + finalPaddingText + fileExtensionNames[(int)extension] + "?\n\n(hold shift to auto-confirm)", "Yes", "No"))
														{
															finalNonNullCount++;
															thisWindow.Focus();
															continue;
														}
														thisWindow.Focus();
													}
												}
												Texture2D outputTexture;

												int finalWidth;
												int finalHeight;
												if(resizeMode == ResizeTextureMode.NO_SCALE)
												{
													finalWidth = txlLayers[i].width;
													finalHeight = txlLayers[i].height;
												}
												else
												{
													finalWidth = resizeX;
													finalHeight = resizeY;
												}

												if(extension == FileExtension.TGA)
												{
													PrepareTextureOutput(txlLayers[i], out outputTexture, TextureFormat.BGRA32, false, finalWidth, finalHeight);
												}
												else if(extension == FileExtension.PNG)
												{
													PrepareTextureOutput(txlLayers[i], out outputTexture, TextureFormat.ARGB32, false, finalWidth, finalHeight);
												}
												else
												{
													PrepareTextureOutput(txlLayers[i], out outputTexture, (TextureFormat)txTextureFormat, txGenerateMipMaps, finalWidth, finalHeight);

													outputTexture.anisoLevel = txAnisoLevel;
													outputTexture.filterMode = txFilterMode;
													outputTexture.wrapMode = txTextureWrapMode;

													if(txCompressImage)
													{
														// Generate mip maps prior to compression, then perform finalization as necessary
														if(txGenerateMipMaps)
														{
															outputTexture.Apply(true, false);
														}
														outputTexture.Compress(true);
													}

													outputTexture.Apply(txGenerateMipMaps, !txMakeReadable);
												}

												WriteFile(outputTexture, fileName + finalPaddingText, extension);
												finalNonNullCount++;
											}
										}
									}
								}

								GUI.enabled = !bDragging;
								EditorGUILayout.PrefixLabel("File Numbering Padding");

								fileNamePadding = EditorGUILayout.IntSlider(fileNamePadding, 1, 4, GUILayout.Width(prefixWidth));
							}
							EditorGUILayout.EndVertical();

							Rect singleFileRect = EditorGUILayout.BeginVertical(GUILayout.Width(detailsWidth - prefixWidth - GUI.skin.button.margin.horizontal - GUI.skin.window.padding.left));
							{
								// Create Single Texture 2D
								// Is the selected image eligible for use?
								if(txlLayers.Count > 0 && roList.index >= 0 && roList.index < txlLayers.Count && txlLayers[roList.index] != null)
								{
									GUI.enabled = !bDragging;
								}
								else
								{
									GUI.enabled = false;
								}
								if(GUILayout.Button(GUIContent.none))
								{
									if(fileName.Length <= 0)
									{
										EditorUtility.DisplayDialog("No File Name", "No file name was entered.\n\nUnable to save file.", "OK");
										thisWindow.Focus();
									}
									else
									{
										if(System.IO.File.Exists(Application.dataPath + "/" + fileName + fileExtensionNames[(int)extension]))
										{
											if(e.modifiers != EventModifiers.Shift)
											{
												if(!EditorUtility.DisplayDialog("Confirm File Replace", "Are you sure you want to overwrite " + fileName + fileExtensionNames[(int)extension] + "?\n\n(hold shift to auto-confirm)", "Yes", "No"))
												{
													thisWindow.Focus();
													return;
												}
												thisWindow.Focus();
											}
										}
										Texture2D outputTexture;

										int finalWidth;
										int finalHeight;
										if(resizeMode == ResizeTextureMode.NO_SCALE)
										{
											finalWidth = txlLayers[roList.index].width;
											finalHeight = txlLayers[roList.index].height;
										}
										else
										{
											finalWidth = resizeX;
											finalHeight = resizeY;
										}

										if(extension == FileExtension.TGA)
										{
											PrepareTextureOutput(txlLayers[roList.index], out outputTexture, TextureFormat.BGRA32, false, finalWidth, finalHeight);
										}
										else if(extension == FileExtension.PNG)
										{
											PrepareTextureOutput(txlLayers[roList.index], out outputTexture, TextureFormat.ARGB32, false, finalWidth, finalHeight);
										}
										else
										{
											PrepareTextureOutput(txlLayers[roList.index], out outputTexture, (TextureFormat)txTextureFormat, txGenerateMipMaps, finalWidth, finalHeight);

											outputTexture.anisoLevel = txAnisoLevel;
											outputTexture.filterMode = txFilterMode;
											outputTexture.wrapMode = txTextureWrapMode;
											if(txCompressImage)
											{
												// Generate mip maps prior to compression, then perform finalization as necessary
												if(txGenerateMipMaps)
												{
													outputTexture.Apply(true, false);
												}
												outputTexture.Compress(true);
											}

											outputTexture.Apply(txGenerateMipMaps, !txMakeReadable);
										}

										WriteFile(outputTexture, fileName, extension);
									}
								}

								EditorGUI.LabelField(new Rect(singleFileRect.x + 20, singleFileRect.y + 1, 100, 16), "Texture2D");
								EditorGUI.LabelField(new Rect(singleFileRect.x + 145, singleFileRect.y + 1, 100, 16), (roList.index < 0 ? "(Selection)" : "(Layer " + roList.index + ")"));

								// Is the total image count a power of 2?
								int validCount = NextPowerOfTwo(txlLayers.Count);
								// When not scaling textures, are all images the same height/width?
								int safeWidth = 2;
								int safeHeight = 2;
								bool equalDimensions = false;
								bool po2Dimensions = false;
								if(nonNullCount == txlLayers.Count && nonNullCount > 0)
								{
									equalDimensions = true;
									if(txlLayers[0] != null)
									{
										safeWidth = txlLayers[0].width;
										safeHeight = txlLayers[0].height;
									}
									for(int i = 1; i < nonNullCount; i++)
									{
										if(txlLayers[i].width != safeWidth || txlLayers[i].height != safeHeight)
										{
											equalDimensions = false;
											errorState |= ERROR_DIMENSION_MISMATCH;
										}
									}
								}
								if(resizeMode == ResizeTextureMode.NO_SCALE)
								{
									if(safeWidth == NextPowerOfTwo(safeWidth) && safeHeight == NextPowerOfTwo(safeHeight))
									{
										po2Dimensions = true;
									}
								}
								else
								{
									if(resizeX == NextPowerOfTwo(resizeX) && resizeY == NextPowerOfTwo(resizeY))
									{
										po2Dimensions = true;
									}
								}

								if(resizeMode != ResizeTextureMode.NO_SCALE || equalDimensions)
								{
									errorState &= ~ERROR_DIMENSION_MISMATCH;
								}

								if(po2Dimensions)
								{
									errorState &= ~ERROR_NON_POWER_OF_TWO;
								}
								else
								{
									errorState |= ERROR_NON_POWER_OF_TWO;
								}

								// Create Texture3D
								int tex3DTooltip = 0;
								if((errorState & ERROR_DIMENSION_MISMATCH) == 0 && extension == FileExtension.ASSET && nonNullCount == validCount && validCount > 0 && po2Dimensions)
								{
									GUI.enabled = !bDragging;
								}
								else
								{
									GUI.enabled = false;
									if(extension != FileExtension.ASSET)
									{
										tex3DTooltip = 1;
									}
									else if(txlLayers.Count != validCount)
									{
										tex3DTooltip = 2;
									}
								}
								if(GUILayout.Button(new GUIContent("", buttonTooltips[tex3DTooltip])))
								{
									if(fileName.Length <= 0)
									{
										EditorUtility.DisplayDialog("No File Name", "No file name was entered.\n\nUnable to save file.", "OK");
										thisWindow.Focus();
									}
									else
									{
										if(System.IO.File.Exists(Application.dataPath + "/" + fileName + fileExtensionNames[(int)extension]))
										{
											if(e.modifiers != EventModifiers.Shift)
											{
												if(!EditorUtility.DisplayDialog("Confirm File Replace", "Are you sure you want to overwrite " + fileName + fileExtensionNames[(int)extension] + "?\n\n(hold shift to auto-confirm)", "Yes", "No"))
												{
													thisWindow.Focus();
													return;
												}
												thisWindow.Focus();
											}
										}

										int finalWidth;
										int finalHeight;
										if(resizeMode == ResizeTextureMode.NO_SCALE)
										{
											finalWidth = safeWidth;
											finalHeight = safeHeight;
										}
										else
										{
											finalWidth = resizeX;
											finalHeight = resizeY;
										}

										Color32[] fullTextureAssembly = new Color32[finalWidth * finalHeight * validCount];
										Texture3D outputTexture = new Texture3D(finalWidth, finalHeight, validCount, (TextureFormat)txTextureFormat, txGenerateMipMaps);
										for(int i = 0; i < validCount; i++)
										{
											Texture2D layerTexture;
											PrepareTextureOutput(txlLayers[i], out layerTexture, TextureFormat.ARGB32, txGenerateMipMaps, finalWidth, finalHeight);

											Color32[] currentTexture = layerTexture.GetPixels32();
											System.Array.Copy(currentTexture, 0, fullTextureAssembly, currentTexture.Length * i, currentTexture.Length);
										}

										outputTexture.SetPixels32(fullTextureAssembly);

										outputTexture.anisoLevel = txAnisoLevel;
										outputTexture.filterMode = txFilterMode;
										outputTexture.wrapMode = txTextureWrapMode;

										outputTexture.Apply(txGenerateMipMaps, !txMakeReadable);

										WriteFile(outputTexture, fileName, extension);
									}
								}

								EditorGUI.LabelField(new Rect(singleFileRect.x + 20, singleFileRect.y + 22, 100, 16), "Texture3D");
								EditorGUI.LabelField(new Rect(singleFileRect.x + 145, singleFileRect.y + 22, 100, 16), "(" + validCount + " Depth)");

								// Create Texture2DArray
								int tex2DArrayTooltip = 0;
								if((errorState & ERROR_DIMENSION_MISMATCH) == 0 && extension == FileExtension.ASSET && SystemInfo.supports2DArrayTextures && nonNullCount == txlLayers.Count && po2Dimensions)
								{
									GUI.enabled = !bDragging;
								}
								else
								{
									GUI.enabled = false;
									if(extension != FileExtension.ASSET)
									{
										tex2DArrayTooltip = 1;
									}
								}
								if(SystemInfo.supports2DArrayTextures)
								{

									if(GUILayout.Button(new GUIContent("", buttonTooltips[tex2DArrayTooltip])))
									{
										if(fileName.Length <= 0)
										{
											EditorUtility.DisplayDialog("No File Name", "No file name was entered.\n\nUnable to save file.", "OK");
											thisWindow.Focus();
										}
										else
										{
											if(System.IO.File.Exists(Application.dataPath + "/" + fileName + fileExtensionNames[(int)extension]))
											{
												if(e.modifiers != EventModifiers.Shift)
												{
													if(!EditorUtility.DisplayDialog("Confirm File Replace", "Are you sure you want to overwrite " + fileName + fileExtensionNames[(int)extension] + "?\n\n(hold shift to auto-confirm)", "Yes", "No"))
													{
														thisWindow.Focus();
														return;
													}
													thisWindow.Focus();
												}
											}
											int finalWidth;
											int finalHeight;
											if(resizeMode == ResizeTextureMode.NO_SCALE)
											{
												finalWidth = safeWidth;
												finalHeight = safeHeight;
											}
											else
											{
												finalWidth = resizeX;
												finalHeight = resizeY;
											}

											Texture2DArray outputTexture = new Texture2DArray(finalWidth, finalHeight, txlLayers.Count, (TextureFormat)txTextureFormat, txGenerateMipMaps);
											for(int i = 0; i < txlLayers.Count; i++)
											{
												Texture2D layerTexture;
												PrepareTextureOutput(txlLayers[i], out layerTexture, TextureFormat.ARGB32, txGenerateMipMaps, finalWidth, finalHeight);

												Color32[] currentTexture = layerTexture.GetPixels32();
												outputTexture.SetPixels32(currentTexture, i);
											}

											outputTexture.anisoLevel = txAnisoLevel;
											outputTexture.filterMode = txFilterMode;
											outputTexture.wrapMode = txTextureWrapMode;

											outputTexture.Apply(txGenerateMipMaps, !txMakeReadable);

											WriteFile(outputTexture, fileName, extension);
										}
									}
								}
								else
								{
									GUILayout.Button(new GUIContent("Texture2DArray Not Supported", "Hardware and/or Operating System are preventing any use of this image format"));
								}

								EditorGUI.LabelField(new Rect(singleFileRect.x + 20, singleFileRect.y + 43, 100, 16), "Texture2DArray");
								EditorGUI.LabelField(new Rect(singleFileRect.x + 145, singleFileRect.y + 43, 100, 16), "(" + txlLayers.Count + " Depth)");
							}
							EditorGUILayout.EndVertical();
						}
						EditorGUILayout.EndHorizontal();

						GUI.enabled = true;

						if(errorState != 0)
						{
							Rect errorRect = new Rect(lastRect.x, thisWindow.position.height - 18, lastRect.width, 20);
							Rect errorTextRect = new Rect(lastRect.x + 2, thisWindow.position.height - 16, lastRect.width - 4, 16);
							GUI.Box(errorRect, GUIContent.none);
							EditorGUI.LabelField(errorTextRect, sErrorText);
						}
					}
					EditorGUILayout.EndVertical();
				}
				GUI.EndGroup();
			}
			EditorGUILayout.EndHorizontal();

			// Prevent horizontal resizing when Editor Window maxSize is not enforced
			if(thisWindow.position.width > thisWindow.maxSize.x)
			{
				thisWindow.position = new Rect(thisWindow.position.x, thisWindow.position.y, thisWindow.maxSize.x, thisWindow.position.height);
			}
		}//OnGUI()

		string GetError(int errorCode)
		{
			// Return codes (in order of priority)
			// 0: no error
			// 1: error		- Undefined textures in list
			// 2: error		- Texture dimension mismatch
			// 4: error		- Non-Power-of-2 alert
			// 8: warning	- Compressed texture quality alert

			if(errorCode == 0)
			{
				return "";
			}
			else if((errorCode & ERROR_NULL_TEXTURE) != 0)
			{
				return "Cannot create Tex3D or Tex2DArray with undefined textures";
			}
			else if((errorCode & ERROR_DIMENSION_MISMATCH) != 0)
			{
				return "All texture sizes must match when \"Don't Resize\" is selected";
			}
			else if((errorCode & ERROR_NON_POWER_OF_TWO) != 0)
			{
				return "Power-of-2 size is required for Tex2DArray(x,y) and Tex3D(x,y,z)";
			}
			else if((errorCode & WARNING_COMPRESSED_INPUT) != 0)
			{
				return "Warning: Compressed textures copy with reduced image quality";
			}
			else
			{
				return "unknown error";
			}
		}

		void UpdateResizeValuesToLast()
		{
			resizeX = lastResizeX;
			resizeY = lastResizeY;
		}

		void ResetResizeValues()
		{
			if(txlLayers != null && txlLayers.Count > 0)
			{
				if(resizeMode == ResizeTextureMode.SCALE_TO_0)
				{
					if(txlLayers[0] != null)
					{
						resizeX = txlLayers[0].width;
						resizeY = txlLayers[0].height;
					}
				}
				else if(resizeMode == ResizeTextureMode.SCALE_UP)
				{
					resizeX = 0;
					resizeY = 0;
					for(int i = 0; i < txlLayers.Count; i++)
					{
						if(txlLayers[i] != null)
						{
							if(txlLayers[i].width > resizeX)
							{
								resizeX = txlLayers[i].width;
							}
							if(txlLayers[i].height > resizeY)
							{
								resizeY = txlLayers[i].height;
							}
						}
					}
				}
				else if(resizeMode == ResizeTextureMode.SCALE_DOWN)
				{
					resizeX = 8192;
					resizeY = 8192;
					for(int i = 0; i < txlLayers.Count; i++)
					{
						if(txlLayers[i] != null)
						{
							if(txlLayers[i].width < resizeX)
							{
								resizeX = txlLayers[i].width;
							}
							if(txlLayers[i].height < resizeY)
							{
								resizeY = txlLayers[i].height;
							}
						}
					}
				}
			}
		}

		void PrepareTextureOutput(Texture2D inputTexture, out Texture2D outputTexture, TextureFormat outputFormat, bool generateMipmaps)
		{
			PrepareTextureOutput(inputTexture, out outputTexture, outputFormat, generateMipmaps, inputTexture.width, inputTexture.height);
		}

		// Use RenderTextures to copy images to circumvent the requirement for textures to be flagged as "read/write enabled"
		void PrepareTextureOutput(Texture2D inputTexture, out Texture2D outputTexture, TextureFormat outputFormat, bool generateMipmaps, int newX, int newY)
		{
			// Fallback won't distort image when upscaling
			Shader s = Shader.Find("Hidden/TAB/ScaleTexture2DPoint");
			if(resizeFilter == ResizeFilter.BILINEAR)
			{
				s = Shader.Find("Hidden/TAB/ScaleTexture2DBilinear");
			}
			Material m = new Material(s);

			outputTexture = new Texture2D(newX, newY, TextureFormat.ARGB32, false);

			// Point filter for manual pixel interpolation in the shaders
			FilterMode filter = inputTexture.filterMode;
			TextureWrapMode wrapMode;
			inputTexture.filterMode = FilterMode.Point;
			TextureWrapMode baseWrap = inputTexture.wrapMode;

			if(resizeWrap == ResizeWrap.AS_IS)
			{
				wrapMode = inputTexture.wrapMode;
			}
			else if(resizeWrap == ResizeWrap.REPEAT)
			{
				wrapMode = TextureWrapMode.Repeat;
			}
			else// if(resizeWrap == ResizeWrap.CLAMP)
			{
				wrapMode = TextureWrapMode.Clamp;
			}
			inputTexture.wrapMode = wrapMode;

			RenderTexture rt = RenderTexture.GetTemporary(inputTexture.width, inputTexture.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
			rt.filterMode = FilterMode.Point;
			rt.wrapMode = wrapMode;
			RenderTexture previous = RenderTexture.active;
			Graphics.Blit(inputTexture, rt);

			// Multiple passes provide a smoother downscaling. Upscaling is performed on the first pass.
			float differenceX = (float)newX / inputTexture.width;
			float differenceY = (float)newY / inputTexture.height;
			int passesX;
			int passesY;
			if(differenceX >= 1)
			{
				passesX = 1;
			}
			else
			{
				passesX = Mathf.CeilToInt(Mathf.Log(1.0f / differenceX, 2));
			}
			if(differenceY >= 1)
			{
				passesY = 1;
			}
			else
			{
				passesY = Mathf.CeilToInt(Mathf.Log(1.0f / differenceY, 2));
			}

			int currentX = inputTexture.width;
			int currentY = inputTexture.height;
			for(int x = 0, y = 0; x < passesX || y < passesY; x++, y++)
			{
				currentX = (currentX / 2) < newX ? newX : (currentX / 2);
				currentY = (currentY / 2) < newY ? newY : (currentY / 2);
				RenderTexture rt2 = RenderTexture.GetTemporary(currentX, currentY, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
				rt2.filterMode = FilterMode.Point;
				rt2.wrapMode = wrapMode;
				Graphics.Blit(rt, rt2, m);
				RenderTexture.ReleaseTemporary(rt);
				rt = rt2;
			}

			RenderTexture.active = rt;
			outputTexture.filterMode = FilterMode.Point;

			outputTexture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
			outputTexture.Apply(false, false);

			// Recreate outputTexture to translate into the intended image format
			Color32[] colorToConvert = outputTexture.GetPixels32();
			outputTexture = new Texture2D(newX, newY, outputFormat, generateMipmaps);
			outputTexture.SetPixels32(colorToConvert);

			inputTexture.filterMode = filter;
			outputTexture.filterMode = filter;

			outputTexture.Apply(false, false);

			inputTexture.wrapMode = baseWrap;

			RenderTexture.active = previous;
			RenderTexture.ReleaseTemporary(rt);
		}

		// Break down selection into 2D texture slices
		void InitializeTextureData(Texture selection)
		{
			errorState = 0;
			//assumeDivision = false;
			if(selection.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
			{
				Texture2D tex = (Texture2D)selection;
				iDivisionsX = 1;
				iDivisionsY = 1;
				if(tex.width != tex.height)
				{
					int gcd = GreatestCommonDivisor(tex.width, tex.height);
					gcd = GreatestCommonDivisor(tex.height, tex.width);

					iDivisionsX = tex.width / gcd;
					iDivisionsY = tex.height / gcd;
				}

				txlLayers.Add(tex);

				if(tex.mipmapCount > 1)
				{
					Shader s = Shader.Find("Hidden/TAB/Texture2Dmipmap");
					Material m = new Material(s);
					int lod = Shader.PropertyToID("_LOD");

					FilterMode filter = selection.filterMode;
					selection.filterMode = FilterMode.Point;

					TextureFormat format = tex.format;
					int minimumSize = -1;
					if(System.Enum.IsDefined(typeof(TAB.UncompressedTextureFormat), (TAB.UncompressedTextureFormat)format))
					{
						minimumSize = 1;
					}
					int loops = (int)Mathf.Min(tex.mipmapCount, Mathf.Log(Mathf.Min(tex.width, tex.height), 2) + minimumSize);

					for(int i = 1, texWidth = (tex.width / 2), texHeight = (tex.height / 2); i < loops; i++, texWidth /= 2, texHeight /= 2)
					{
						RenderTexture rt = RenderTexture.GetTemporary(texWidth, texHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
						RenderTexture previous = RenderTexture.active;

						m.SetInt(lod, i);
						Graphics.Blit(tex, rt, m);
						RenderTexture.active = rt;
						Texture2D newTex2D = new Texture2D(texWidth, texHeight, TextureFormat.ARGB32, false);
						newTex2D.filterMode = FilterMode.Point;

						newTex2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
						newTex2D.Apply(false, false);

						newTex2D.filterMode = filter;

						newTex2D.hideFlags = HideFlags.HideAndDontSave;

						txlLayers.Add(newTex2D);

						RenderTexture.active = previous;
						RenderTexture.ReleaseTemporary(rt);
					}
					selection.filterMode = filter;
				}
			}
			else if(selection.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
			{
				Shader s = Shader.Find("Hidden/TAB/Texture3Dto2D");
				Material m = new Material(s);
				int slice = Shader.PropertyToID("_Slice");

				Texture3D tex = (Texture3D)selection;

				FilterMode filter = selection.filterMode;
				selection.filterMode = FilterMode.Point;

				RenderTexture rt = RenderTexture.GetTemporary(tex.width, tex.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
				RenderTexture previous = RenderTexture.active;
				int totalDepth = tex.depth;
				for(int i = 0; i < totalDepth; i++)
				{
					m.SetFloat(slice, (float)i / totalDepth);
					Graphics.Blit(tex, rt, m);
					RenderTexture.active = rt;
					Texture2D newTex2D = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false);
					newTex2D.filterMode = FilterMode.Point;

					newTex2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
					newTex2D.Apply(false, false);

					newTex2D.filterMode = filter;

					newTex2D.hideFlags = HideFlags.HideAndDontSave;

					txlLayers.Add(newTex2D);
				}
				RenderTexture.active = previous;
				RenderTexture.ReleaseTemporary(rt);

				selection.filterMode = filter;
			}
			else if(selection.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
			{
				Texture2DArray tex = (Texture2DArray)selection;
				if(SystemInfo.supports2DArrayTextures)
				{
					// Mostly identical to Texture3D
					Shader s = Shader.Find("Hidden/TAB/Texture2DArrayTo2D");
					Material m = new Material(s);
					int arrayPos = Shader.PropertyToID("_ArrayPosition");

					FilterMode filter = selection.filterMode;
					selection.filterMode = FilterMode.Point;

					RenderTexture rt = RenderTexture.GetTemporary(tex.width, tex.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
					RenderTexture previous = RenderTexture.active;
					int totalDepth = tex.depth;
					for(int i = 0; i < totalDepth; i++)
					{
						m.SetFloat(arrayPos, i);
						Graphics.Blit(tex, rt, m);
						RenderTexture.active = rt;
						Texture2D newTex2D = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false);
						newTex2D.filterMode = FilterMode.Point;

						newTex2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
						newTex2D.Apply(false, false);

						newTex2D.filterMode = filter;

						newTex2D.hideFlags = HideFlags.HideAndDontSave;

						txlLayers.Add(newTex2D);
					}
					RenderTexture.active = previous;
					RenderTexture.ReleaseTemporary(rt);

					selection.filterMode = filter;
				}
				else
				{
					// Texture2DArray isn't able to be created or accessed in any form when not supported by hardware/software.
					Debug.LogError("Texture2DArray is not available in the current rendering mode.");
				}
			}
			if(roList.index > txlLayers.Count)
			{
				roList.index = txlLayers.Count - 1;
			}
		}

		void OnSelectionChange()
		{
			if(unlockSelection && assetSelection != Selection.activeObject)
			{
				assetSelection = Selection.activeObject;
				if(assetSelection != null)
				{
					System.Type assetType = assetSelection.GetType();
					if(assetType == typeof(Texture2D) || assetType == typeof(Texture3D) || assetType == typeof(Texture2DArray))
					{
						txSelection = (Texture)assetSelection;
					}
				}
				else
				{
					txSelection = null;
				}
			}

			Repaint();
		}

		void DivideTexture(Texture2D tex, int divisionsX, int divisionsY)
		{
			txlLayers.Clear();

			Shader s = Shader.Find("Hidden/TAB/Texture2Ddivide");
			Material m = new Material(s);
			int divcur = Shader.PropertyToID("_DivideCurrentXY");
			Vector4 divideCurrent;

			FilterMode filter = tex.filterMode;
			tex.filterMode = FilterMode.Point;

			divideCurrent.x = divisionsX;
			divideCurrent.y = divisionsY;

			RenderTexture rt = RenderTexture.GetTemporary(tex.width / divisionsX, tex.height / divisionsY, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
			RenderTexture previous = RenderTexture.active;

			for(int y = 0; y < divisionsY; y++)
			{
				divideCurrent.w = y;
				for(int x = 0; x < divisionsX; x++)
				{
					divideCurrent.z = x;

					m.SetVector(divcur, divideCurrent);

					Graphics.Blit(tex, rt, m);
					RenderTexture.active = rt;
					Texture2D newTex2D = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false);
					newTex2D.filterMode = FilterMode.Point;

					newTex2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
					newTex2D.Apply(false, false);

					newTex2D.filterMode = filter;

					newTex2D.hideFlags = HideFlags.HideAndDontSave;

					txlLayers.Add(newTex2D);
				}
			}
			tex.filterMode = filter;

			RenderTexture.active = previous;
			RenderTexture.ReleaseTemporary(rt);
		}

		static void WriteFile(Texture texture, string localFilePath, FileExtension extension)
		{
			if(texture.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
			{
				Texture2D tex = (Texture2D)texture;

				if(extension == FileExtension.ASSET)
				{
					if(System.IO.Directory.Exists(Application.dataPath + "/" + localFilePath + fileExtensionNames[(int)extension]))
					{
						Debug.LogError("Error: Assets/" + localFilePath + fileExtensionNames[(int)extension] + " already exists as a folder.");
					}
					else
					{
						Texture2D existingTex = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/" + localFilePath + fileExtensionNames[(int)extension], typeof(Texture2D));
						if(existingTex != null)
						{
							EditorUtility.CopySerialized(tex, existingTex);
							AssetDatabase.SaveAssets();
						}
						else
						{
							AssetDatabase.CreateAsset(tex, ("Assets/" + localFilePath + fileExtensionNames[(int)extension]));
						}
					}
				}
				else if(extension == FileExtension.PNG)
				{
					byte[] bytes = tex.EncodeToPNG();
					System.IO.File.WriteAllBytes(Application.dataPath + "/" + localFilePath + fileExtensionNames[(int)extension], bytes);
				}
				else if(extension == FileExtension.TGA)
				{
					byte[] bytes = tex.GetRawTextureData();
					int returnCode = CreateTGAImage(Application.dataPath + "/" + localFilePath + fileExtensionNames[(int)extension], ref bytes, tex.width, tex.height);
					if(returnCode != 0)
					{
						Debug.LogError("CreateTGAImage returned error code: " + returnCode);
					}
				}
				else
				{
					// You should not see this. If you do see this, something is not in a normal state
					Debug.LogError("Output Texture2D cannot be exported with an unknown extension.");
				}
			}
			else if(texture.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
			{
				Texture3D tex = (Texture3D)texture;

				if(extension == FileExtension.ASSET)
				{
					if(System.IO.Directory.Exists(Application.dataPath + "/" + localFilePath + fileExtensionNames[(int)extension]))
					{
						Debug.LogError("Error: Assets/" + localFilePath + fileExtensionNames[(int)extension] + " already exists as a folder.");
					}
					else
					{
						Texture3D existingTex = (Texture3D)AssetDatabase.LoadAssetAtPath("Assets/" + localFilePath + fileExtensionNames[(int)extension], typeof(Texture3D));
						if(existingTex != null)
						{
							EditorUtility.CopySerialized(tex, existingTex);
							AssetDatabase.SaveAssets();
						}
						else
						{
							AssetDatabase.CreateAsset(tex, ("Assets/" + localFilePath + fileExtensionNames[(int)extension]));
						}
					}
				}
				else
				{
					// You should not see this. If you do see this, something is not in a normal state
					Debug.LogError("Output Texture3D cannot be exported as a *" + fileExtensionNames[(int)extension] + " file.");
				}
			}
			else if(texture.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
			{
				Texture2DArray tex = (Texture2DArray)texture;

				if(extension == FileExtension.ASSET)
				{
					if(System.IO.Directory.Exists(Application.dataPath + "/" + localFilePath + fileExtensionNames[(int)extension]))
					{
						Debug.LogError("Error: Assets/" + localFilePath + fileExtensionNames[(int)extension] + " already exists as a folder.");
					}
					else
					{
						Texture2DArray existingTex = (Texture2DArray)AssetDatabase.LoadAssetAtPath("Assets/" + localFilePath + fileExtensionNames[(int)extension], typeof(Texture2DArray));
						if(existingTex != null)
						{
							EditorUtility.CopySerialized(tex, existingTex);
							AssetDatabase.SaveAssets();
						}
						else
						{
							AssetDatabase.CreateAsset(tex, ("Assets/" + localFilePath + fileExtensionNames[(int)extension]));
						}
					}
				}
				else
				{
					// You should not see this. If you do see this, something is not in a normal state
					Debug.LogError("Output Texture2DArray cannot be exported as a *" + fileExtensionNames[(int)extension] + " file.");
				}
			}
			else
			{
				// You should not see this. If you do see this, something is not in a normal state
				Debug.LogError("Output texture format is invalid. It must be Tex2D, Tex3D, or Tex2DArray.");
			}
			AssetDatabase.Refresh();
		}

		static int CreateTGAImage(string outTGAPath, ref byte[] inBGRA, int width, int height)
		{
			// Return codes
			// 0: success
			// 1: error - bad arguments
			// 2: error - file open failure
			// 3: error - bad header
			// 4: error - input buffer wrong size
			// 6: error - file write failure

			// check arguments
			if(outTGAPath == null || inBGRA == null || outTGAPath.Length == 0 || inBGRA.Length == 0 || width <= 0 || height <= 0)
			{
				return 1;
			}
			// check buffer size
			if(inBGRA.Length != width * height * 4)
			{
				return 4;
			}

			// open file for writing
			System.IO.FileStream fsTGAFile = null;
			try
			{
				fsTGAFile = System.IO.File.Create(outTGAPath);
			}
			catch
			{
				if(fsTGAFile != null)
				{
					fsTGAFile.Dispose();
				}
				return 2;
			}

			// create header
			System.IO.BinaryWriter bwTGAFile = null;
			try
			{
				bwTGAFile = new System.IO.BinaryWriter(fsTGAFile);
			}
			catch
			{
				if(bwTGAFile != null)
				{
					bwTGAFile.Close();
				}
				fsTGAFile.Dispose();
				return 3;
			}
			try
			{
				bwTGAFile.Write((byte)0);                           // BYTE IDLength		00h Size of Image ID field
				bwTGAFile.Write((byte)0);                           // BYTE ColorMapType	01h Color map type
				bwTGAFile.Write((byte)2);   // uncompressed, BGRA32	// BYTE ImageType		02h Image type code
				bwTGAFile.Write((short)0);                          // WORD CMapStart		03h Color map origin
				bwTGAFile.Write((short)0);                          // WORD CMapLength		05h Color map length
				bwTGAFile.Write((byte)0);                           // BYTE CMapDepth		07h Depth of color map entries
				bwTGAFile.Write((short)0);                          // WORD XOffset			08h X origin of image
				bwTGAFile.Write((short)0);                          // WORD YOffset			0Ah Y origin of image
				bwTGAFile.Write((short)width);                      // WORD Width			0Ch Width of image
				bwTGAFile.Write((short)height);                     // WORD Height			0Eh Height of image
				bwTGAFile.Write((byte)32);                          // BYTE PixelDepth		10h Image pixel size
				bwTGAFile.Write((byte)8);                           // BYTE ImageDescriptor	11h Image descriptor byte
			}
			catch
			{
				bwTGAFile.Close();
				fsTGAFile.Dispose();
				return 3;
			}

			// write image data
			try
			{
				bwTGAFile.Write(inBGRA);
			}
			catch
			{
				bwTGAFile.Close();
				fsTGAFile.Dispose();
				return 6;
			}

			bwTGAFile.Close();
			fsTGAFile.Dispose();
			return 0;
		}

		static int NextPowerOfTwo(int input)
		{
			uint n = (uint)input;
			n--;
			n |= n >> 1;
			n |= n >> 2;
			n |= n >> 4;
			n |= n >> 8;
			n |= n >> 16;
			n++;
			if(n > int.MaxValue)
			{
				return (int)(((uint)int.MaxValue + 1) / 2);
			}
			return (int)n;
		}

		static int GreatestCommonDivisor(int a, int b)
		{
			return b == 0 ? a : GreatestCommonDivisor(b, a % b);
		}
	}//class TAB

	public class ViewerWindow : EditorWindow
	{
		private static EditorWindow thisWindow;

		public Texture txTexture;

		private Vector2 scrollView;
		private Shader s;
		private Material m;
		private int slice;
		private int lod;
		private int arrayPos;
		private bool ignoreAlpha = true;
		private float sliceDepth;
		private int arrayDepth;
		private bool sliceToggle = true;
		private RectOffset padding;

		void OnEnable()
		{
			thisWindow = this;
			if(txTexture != null)
			{
				if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
				{
					s = Shader.Find("Hidden/TAB/ClipTexture2D");
				}
				else if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
				{
					s = Shader.Find("Hidden/TAB/ClipTexture3D");
				}
				else if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
				{
					s = Shader.Find("Hidden/TAB/ClipTexture2DArray");
				}
				m = new Material(s);
				slice = Shader.PropertyToID("_Slice");
				lod = Shader.PropertyToID("_LOD");
				arrayPos = Shader.PropertyToID("_ArrayPosition");
				
				m.hideFlags = HideFlags.HideAndDontSave;
			}

			thisWindow.minSize = new Vector2(245, 245); // Minimum possible horizontal slider with scrollbar
		}

		void OnGUI()
		{
			if(s == null || thisWindow == null || padding == null)
			{
				OnEnable();
				padding = GUI.skin.label.padding;
			}
			if(txTexture != null && s != null)
			{
				int width = txTexture.width;
				if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
				{
					if(((Texture2D)txTexture).mipmapCount > 1)
					{
						width = (int)(width * 1.5f) + 2;
					}
				}

				scrollView = EditorGUILayout.BeginScrollView(scrollView);
				Vector2 pos = Vector2.zero;

				if(ignoreAlpha)
				{
					m.EnableKeyword("IGNORE_ALPHA");
				}
				else
				{
					m.DisableKeyword("IGNORE_ALPHA");
				}

				if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex2D)
				{
					int textureHeightOffset = txTexture.height + 18;
					EditorGUILayout.LabelField(GUIContent.none, GUILayout.Width(width - padding.horizontal), GUILayout.Height(textureHeightOffset));
					Texture2D texBase = (Texture2D)txTexture;

					ignoreAlpha = EditorGUI.ToggleLeft(new Rect(padding.left, padding.bottom, 140, 16), "Ignore Alpha Channel", ignoreAlpha);

					TextureFormat format = texBase.format;
					int minimumSize = -1;
					if(System.Enum.IsDefined(typeof(TAB.UncompressedTextureFormat), (TAB.UncompressedTextureFormat)format))
					{
						minimumSize = 1;
					}
					int loops = (int)Mathf.Min(texBase.mipmapCount, Mathf.Log(Mathf.Min(texBase.width, texBase.height), 2) + minimumSize);
					for(int i = 0, texWidth = texBase.width, texHeight = texBase.height; i < loops; i++, texWidth /= 2, texHeight /= 2)
					{
						m.SetInt(lod, i);
						EditorGUI.DrawPreviewTexture(new Rect(padding.left + pos.x, 22 + padding.bottom + pos.y, texWidth, texHeight), texBase, m, ScaleMode.ScaleToFit);
						if((i & 1) == 0) // even
						{
							pos.x += texWidth + 2;
						}
						else // odd
						{
							pos.y += texHeight + 2;
						}
					}
				}
				else if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex3D)
				{
					if(SystemInfo.supports3DTextures)
					{
						int textureHeightOffset = txTexture.height + 40;
						EditorGUILayout.LabelField(GUIContent.none, GUILayout.Width(width - padding.horizontal), GUILayout.Height(textureHeightOffset));
						Texture3D texBase = (Texture3D)txTexture;

						// Resize slider from offset position while honoring scrollbars appearing on demand
						int sliderXPosition = padding.left + 16 + padding.horizontal + 90;
						int sliderWidthOffset = (textureHeightOffset + padding.vertical >= thisWindow.position.height) || ((width + padding.horizontal > thisWindow.position.width) && (textureHeightOffset + padding.vertical >= thisWindow.position.height - GUI.skin.horizontalScrollbar.fixedHeight)) ? 18 : 2;
						float sliderWidth = Mathf.Max(115, Mathf.Min(width + padding.left - sliderXPosition, thisWindow.position.width - sliderXPosition - sliderWidthOffset));
						ignoreAlpha = EditorGUI.ToggleLeft(new Rect(padding.left, padding.bottom, 140, 16), "Ignore Alpha Channel", ignoreAlpha);
						sliceToggle = EditorGUI.Toggle(new Rect(padding.left, padding.bottom + 18, 16, 16), sliceToggle); // Default "Toggle" size (16, 16)
						EditorGUI.LabelField(new Rect(padding.left, padding.bottom + 18, 16, 16), new GUIContent("", "Snap to Nearest Layer"));
						EditorGUI.PrefixLabel(new Rect(padding.horizontal + 14, padding.bottom + 18, 90, 16), new GUIContent("Texture Depth"));
						sliceDepth = EditorGUI.Slider(new Rect(sliderXPosition, padding.bottom + 18, sliderWidth, 20), sliceDepth, 0, 1);

						if(sliceToggle)
						{
							sliceDepth = (Mathf.Floor(Mathf.Min(sliceDepth * texBase.depth, texBase.depth - 1)) + 0.5f) / texBase.depth;
						}
						m.SetFloat(slice, sliceDepth);
						EditorGUI.DrawPreviewTexture(new Rect(padding.left, 40 + padding.bottom, texBase.width, texBase.height), texBase, m, ScaleMode.ScaleToFit);
					}
					else
					{
						EditorGUILayout.LabelField("Texture3D format is not available for viewing.", GUILayout.Width(310), GUILayout.Height(18));
					}
				}
				else if(txTexture.dimension == UnityEngine.Rendering.TextureDimension.Tex2DArray)
				{
					if(SystemInfo.supports2DArrayTextures)
					{
						int textureHeightOffset = txTexture.height + 40;
						EditorGUILayout.LabelField(GUIContent.none, GUILayout.Width(width - padding.horizontal), GUILayout.Height(textureHeightOffset));
						Texture2DArray texBase = (Texture2DArray)txTexture;

						// Resize slider from offset position while honoring scrollbars appearing on demand
						int sliderXPosition = padding.left + 16 + padding.horizontal + 90;
						int sliderWidthOffset = (textureHeightOffset + padding.vertical >= thisWindow.position.height) || ((width + padding.horizontal > thisWindow.position.width) && (textureHeightOffset + padding.vertical >= thisWindow.position.height - GUI.skin.horizontalScrollbar.fixedHeight)) ? 18 : 2;
						float sliderWidth = Mathf.Max(115, Mathf.Min(width + padding.left - sliderXPosition, thisWindow.position.width - sliderXPosition - sliderWidthOffset));
						ignoreAlpha = EditorGUI.ToggleLeft(new Rect(padding.left, padding.bottom, 140, 16), "Ignore Alpha Channel", ignoreAlpha);
						EditorGUI.PrefixLabel(new Rect(padding.horizontal + 14, padding.bottom + 18, 90, 16), new GUIContent("Texture Depth"));
						arrayDepth = EditorGUI.IntSlider(new Rect(sliderXPosition, padding.bottom + 18, sliderWidth, 20), arrayDepth, 0, texBase.depth - 1);

						m.SetInt(arrayPos, arrayDepth);
						EditorGUI.DrawPreviewTexture(new Rect(padding.left, 40 + padding.bottom, texBase.width, texBase.height), texBase, m, ScaleMode.ScaleToFit);
					}
					else
					{
						EditorGUILayout.LabelField("Texture2DArray format is not available for viewing.", GUILayout.Width(310), GUILayout.Height(18));
					}
				}
				EditorGUILayout.EndScrollView();
			}
		}//OnGUI()
	}//class ViewerWindow
}//namespace TextureAssetBuilder
