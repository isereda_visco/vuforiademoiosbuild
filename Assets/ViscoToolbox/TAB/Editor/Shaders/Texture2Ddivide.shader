﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Read slices from a Texture2D
// Perform in iterations
Shader "Hidden/TAB/Texture2Ddivide"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "" {}
		//_DivideX("X-axis Division", int) = 0
		//_DivideY("Y-axis Division", int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			sampler2D _MainTex;
			//int _DivideX;
			//int _DivideY;
			//int _CurrentX;
			//int _CurrentY;
			float4 _DivideCurrentXY;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				//float4 texDivided = tex2D(_MainTex, float2((input.tex.x + _CurrentX) / _DivideX, (input.tex.y + _CurrentY) / _DivideY));
				float4 texDivided = tex2D(_MainTex, float2((input.tex.x + _DivideCurrentXY.z) / _DivideCurrentXY.x, (input.tex.y + _DivideCurrentXY.w) / _DivideCurrentXY.y));

				return texDivided;
			}
			ENDCG
		}
	}
}
