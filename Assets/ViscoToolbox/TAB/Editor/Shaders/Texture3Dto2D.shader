﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// A single layer of a Texture3D is read using
// Graphics.Blit() to transfer contents to a Texture2D
Shader "Hidden/TAB/Texture3Dto2D"
{
	Properties
	{
		_MainTex ("Texture", 3D) = "" {}
		_Slice ("Slice Depth", range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			sampler3D _MainTex;
			float _Slice;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				float4 slicedTex = tex3D(_MainTex, float3(input.tex.xy, _Slice));

				return slicedTex;
			}
			ENDCG
		}
	}
}
