﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Scale a texture2D from source to destination
// using Bilinear Interpolation image filtering
Shader "Hidden/TAB/ScaleTexture2DBilinear"
{
	Properties
	{
		_MainTex("Texture", 2D) = "" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				// Use with point sampling for proper results
				// Subtract half a texel to recenter the four sampled pixels
				float4 texC = tex2D(_MainTex, (input.tex.xy - (_MainTex_TexelSize.xy * 0.5)));
				float4 texX = tex2D(_MainTex, (float2(input.tex.x + _MainTex_TexelSize.x, input.tex.y) - (_MainTex_TexelSize.xy * 0.5)));
				float4 texY = tex2D(_MainTex, (float2(input.tex.x, input.tex.y + _MainTex_TexelSize.y) - (_MainTex_TexelSize.xy * 0.5)));
				float4 texXY = tex2D(_MainTex, (float2(input.tex.xy + _MainTex_TexelSize.xy) - (_MainTex_TexelSize.xy * 0.5)));
				float2 blendAmount = frac((input.tex.xy - (_MainTex_TexelSize.xy * 0.5)) * _MainTex_TexelSize.zw);
				float4 blend1 = lerp(texC, texX, blendAmount.x);
				float4 blend2 = lerp(texY, texXY, blendAmount.x);
				float4 scaledTex = lerp(blend1, blend2, blendAmount.y);

				return scaledTex;
			}
			ENDCG
		}
	}
}
