﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Format a Texture2D to be viewable
// at any LOD in an editor window
Shader "Hidden/TAB/ClipTexture2D"
{
	Properties
	{
		_MainTex("Texture", 2D) = "" {}
		_LOD("Level of Detail", int) = 0
	}

	SubShader
	{
		Tags { "ForceSupported" = "True" }

		Lighting Off 
		Blend SrcAlpha OneMinusSrcAlpha 
		Cull Off 
		ZWrite Off 
		Fog { Mode Off } 
		ZTest Always

		Pass
		{	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma target 3.0
			#pragma glsl
			#pragma multi_compile OFF IGNORE_ALPHA

			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
				float2 texgen : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _GUIClipTexture;

			uniform float4 _MainTex_ST;
			uniform float4x4 unity_GUIClipTextureMatrix;

			int _LOD;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				float4 texgen = mul(UNITY_MATRIX_MV, input.vertex);
				output.texgen = mul(unity_GUIClipTextureMatrix, texgen);
				output.tex = TRANSFORM_TEX(input.texcoord, _MainTex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float4 col = tex2Dlod(_MainTex, float4(input.tex.xy, 0, _LOD));
#if IGNORE_ALPHA
				col.a = 1;
#endif
				col.a *= tex2D(_GUIClipTexture, input.texgen).a;
				return col;
			}
			ENDCG 
		}
	}
}
