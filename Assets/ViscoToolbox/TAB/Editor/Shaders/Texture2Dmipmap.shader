﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// A texture2D is read at a specified mipmap level of detail
Shader "Hidden/TAB/Texture2Dmipmap"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "" {}
		_LOD("Level of Detail", int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			sampler2D _MainTex;
			int _LOD;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				float4 texLOD = tex2Dlod(_MainTex, float4(input.tex.xy, 0, _LOD));

				return texLOD;
			}
			ENDCG
		}
	}
}
