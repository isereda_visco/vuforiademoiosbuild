﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// A single layer of a Texture2DArray is read using
// Graphics.Blit() to transfer contents to a Texture2D
Shader "Hidden/TAB/Texture2DArrayTo2D"
{
	Properties
	{
		_MainTex ("Texture", 2DArray) = "" {}
		_ArrayPosition ("Array Position", Float) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// To use texture arrays we need to target DX10/OpenGLES3 which
			// requires shader model 3.5 minimum
			#pragma target 3.5
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			UNITY_DECLARE_TEX2DARRAY(_MainTex);
			float _ArrayPosition;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				float4 slicedTex = UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(input.tex.xy, _ArrayPosition));

				return slicedTex;
			}
			ENDCG
		}
	}
}
