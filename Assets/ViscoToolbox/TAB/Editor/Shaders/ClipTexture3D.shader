﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Format a Texture3D to be viewable
// at any depth in an editor window
Shader "Hidden/TAB/ClipTexture3D"
{
	Properties
	{
		_MainTex("Texture", 3D) = "" {}
		_Slice("Slice Depth", range(0.0, 1.0)) = 0.0
	}

	SubShader
	{
		Tags { "ForceSupported" = "True" }

		Lighting Off 
		Blend SrcAlpha OneMinusSrcAlpha 
		Cull Off 
		ZWrite Off 
		Fog { Mode Off } 
		ZTest Always

		Pass
		{	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile OFF IGNORE_ALPHA

			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
				float2 texgen : TEXCOORD1;
			};

			sampler3D _MainTex;
			sampler2D _GUIClipTexture;
			float _Slice;

			uniform float4 _MainTex_ST;
			uniform float4x4 unity_GUIClipTextureMatrix;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				float4 texgen = mul(UNITY_MATRIX_MV, input.vertex);
				output.texgen = mul(unity_GUIClipTextureMatrix, texgen);
				output.tex = input.texcoord;
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				fixed4 col = tex3D(_MainTex, float3(input.tex.xy, _Slice));
#if IGNORE_ALPHA
				col.a = 1;
#endif
				col.a *= tex2D(_GUIClipTexture, input.texgen).a;
				return col;
			}
			ENDCG 
		}
	}
}
