﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Format a Texture2DArray to be viewable
// at any element in an editor window
Shader "Hidden/TAB/ClipTexture2DArray"
{
	Properties
	{
		_MainTex("Texture", 2DArray) = "" {}
		_ArrayPosition("Array Position", int) = 0.0
	}

	SubShader
	{
		Tags { "ForceSupported" = "True" }

		Lighting Off 
		Blend SrcAlpha OneMinusSrcAlpha 
		Cull Off 
		ZWrite Off 
		Fog { Mode Off } 
		ZTest Always

		Pass
		{	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest

			// To use texture arrays we need to target DX10/OpenGLES3 which
			// requires shader model 3.5 minimum
			#pragma target 3.5

			#pragma multi_compile OFF IGNORE_ALPHA

			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 tex : TEXCOORD0;
				float2 texgen : TEXCOORD1;
			};

			UNITY_DECLARE_TEX2DARRAY(_MainTex);
			sampler2D _GUIClipTexture;
			int _ArrayPosition;

			uniform float4 _MainTex_ST;
			uniform float4x4 unity_GUIClipTextureMatrix;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				float4 texgen = mul(UNITY_MATRIX_MV, input.vertex);
				output.texgen = mul(unity_GUIClipTextureMatrix, texgen);
				output.tex = TRANSFORM_TEX(input.texcoord, _MainTex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				fixed4 col = UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(input.tex.xy, _ArrayPosition));
#if IGNORE_ALPHA
				col.a = 1;
#endif
				col.a *= tex2D(_GUIClipTexture, input.texgen).a;
				return col;
			}
			ENDCG 
		}
	}
}
