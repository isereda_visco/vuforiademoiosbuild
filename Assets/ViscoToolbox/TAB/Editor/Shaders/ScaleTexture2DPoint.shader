﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Scale a texture2D from source to destination
// using Nearest Neighbor image filtering
Shader "Hidden/TAB/ScaleTexture2DPoint"
{
	Properties
	{
		_MainTex("Texture", 2D) = "" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord: TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			
			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.tex = input.texcoord;
				return output;
			}
			
			float4 frag(vertexOutput input) : COLOR
			{
				// Bypasses Bilinear Filtering by sampling pixel centers
				float4 scaledTex = tex2D(_MainTex, (floor(input.tex.xy * _MainTex_TexelSize.zw) * _MainTex_TexelSize.xy) + (_MainTex_TexelSize.xy * 0.5));

				return scaledTex;
			}
			ENDCG
		}
	}
}
