﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Visco
{
    [Serializable]
    public class EventPropertyClass
    {
        public struct ScriptsInfo
        {
            public MonoBehaviour Script;
            public MethodInfo Method;
            public ScriptsInfo(MonoBehaviour script, MethodInfo method)
            {
                Script = script;
                Method = method;
            }
        }

        public GameObject targetObject; // обєкт який натягуємо в інспекторі 

        public string MethodName;
        public string valuesString;

        private List<ScriptsInfo> ScriptInfoList = new List<ScriptsInfo>();

        /// <summary>
        /// invokes method,defined in editor
        /// </summary>
        public void Invoke()
        {
            Debug.Log("method run" + valuesString);
            object[] obj = valuesString.UnparseArgumentsFromString();
            GetReflections(obj);
        }

        void Run() { }

        public void GetReflections(object[] parameters)
        {
            if (targetObject)
            {
                MonoBehaviour[] monoBehaviours = targetObject.GetComponents<MonoBehaviour>();

                List<string> methods = new List<string>();
                List<MethodInfo> methodInfosList = new List<MethodInfo>();

                for (int i = 0; i < monoBehaviours.Length; i++)
                {
                    Type t = monoBehaviours[i].GetType();
                    MethodInfo[] mInfo = t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).ToArray();

                    foreach (MethodInfo info in mInfo)
                    {
                        ScriptsInfo scriptsInfo = new ScriptsInfo(monoBehaviours[i], info);
                        ScriptInfoList.Add(scriptsInfo);
                    }
                    methodInfosList.AddRange(mInfo.ToArray());

                    methods.AddRange(mInfo.Select(x => x.Name).ToList());
                }

                for (int i = 0; i < ScriptInfoList.Count; i++)
                {
                    if (ScriptInfoList[i].Method.Name == MethodName)
                    {
                        try
                        {
                            ScriptInfoList[i].Method.Invoke(ScriptInfoList[i].Script, parameters);
                        }
                        catch
                        {
                            throw new Exception("problem invoking method");
                        }

                        break;
                    }
                }
            }
            else
            {
                throw new Exception("no Gameobject attached");
            }
        }
    }
}