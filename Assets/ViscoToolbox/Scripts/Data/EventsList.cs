﻿    using System;
    using UnityEngine;

    [CreateAssetMenu(fileName = "EventsList", menuName = "Visco/Create Events List")]
    public class EventsList : ScriptableObject
    {
        [SerializeField] private string[] _list;
        public string[] List { get { return _list; } }
    }

    [Serializable]
    public class EventId
    {
        [SerializeField] private string _id;
        public string Value { get { return _id; } }
    }
