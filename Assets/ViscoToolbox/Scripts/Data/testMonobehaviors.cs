﻿using UnityEngine;

namespace Visco
{
    public enum _enum
    {
        enum1,
        enum2
    }

    public class testMonobehaviors : MonoBehaviour
    {
        
        void Start() { }

        public void Meth0()
        {
            print("Meth0 called");
        }

        public void Meth1(string oneint)
        {
            print(string.Format("!!!Meth1 called!!!\n int {0}", oneint));
        }

        public void Meth2(int oneInt, string oneString)
        {
            print(string.Format("!!!Meth2 called!!! int {0}  int {1}", oneInt, oneString));
        }

        public void Meth3(string onestring, int oneint, EventId id, Color onefloat, AnimationClip anim,_enum e)
        {
            print(string.Format("!!!Meth3 called!!!\n string {0}  int {1}  float {2} event {3}", onestring, oneint, onefloat, id.Value));
        }
        public void Meth4(int i)
        {
            print("!!!Meth4 called!!!\n ");
        }
        public void Meth5(int oneint)
        {
            print("!!!Meth5 called!!!\n string");
        }

    }
}