﻿using UnityEngine.Events;
using System;

namespace Visco
{
    [Serializable]
    public class UnityEventWrapper
    {
        public bool UseUnityEvents;
        public bool WaitForCamera;
        public float Delay;
        public UnityEvent UnityEvents;

        public void InvokeEvents() { UnityEvents.Invoke(); }
    }
}