﻿namespace Visco
{
    public class Constants
    {
        public static string HOTSPOT_PREFAB_PATH = "ToolboxPrefabs/HotSpotPrefab";
        public static string TEXT_BOX_PREFAB_PATH = "ToolboxPrefabs/TextBoxPrefab";
        public static string MAIN_PREFAB_PATH = "ToolboxPrefabs/_main";
        public static string MAIN_CAMERA_PREFAB_PATH = "ToolboxPrefabs/MainCameraPrefab";
        public static string CANVAS_PREFAB_PATH = "ToolboxPrefabs/CanvasPrefab";
        public static string ROOT_STATE_REFAB_PATH = "ToolboxPrefabs/RootStateObjectPrefab";

        public static string CONTEXT_MENU_PREFAB_PATH = "ToolboxPrefabs/ContextMenuPrefab";
        public static string CONTEXT_BUTTON_SINGLE_PREFAB_PATH = "ToolboxPrefabs/ContextButtonSinglePrefab";
        public static string CONTEXT_BUTTON_LEFT_PREFAB_PATH = "ToolboxPrefabs/ContextButtonLeftPrefab";
        public static string CONTEXT_BUTTON_MIDDLE_PREFAB_PATH = "ToolboxPrefabs/ContextButtonMiddlePrefab";
        public static string CONTEXT_BUTTON_RIGHT_PREFAB_PATH = "ToolboxPrefabs/ContextButtonRightPrefab";

        public static string VIDEO_PLAYER_PREFAB_PATH = "ToolboxPrefabs/VideoPlayerPrefab";
        public static string VIDEO_BUTTON_PREFAB_PATH = "ToolboxPrefabs/VideoButtonPrefab";
        public static string IMAGE_BUTTON_PREFAB_PATH = "ToolboxPrefabs/ImageButtonPrefab";

        public static string BACK_BUTTON_PREFAB_PATH = "ToolboxPrefabs/BackButtonPrefab";
        public static string SIDE_MENU_PREFAB_PATH = "ToolboxPrefabs/SideMenuPrefab";

        public static string STATE_PATH = "ToolboxPrefabs/StatePrefab";
        public static string PREVIOUS_STATE_BUTTON_PREFAB_PATH = "ToolboxPrefabs/PreviousStateButtonPrefab";
        public static string NEXT_STATE_BUTTON_PREFAB_PATH = "ToolboxPrefabs/NextStateButtonPrefab";

        public static string CALLOUT_CANVAS_PATH = "ToolboxPrefabs/CalloutCanvas";
        public static string CALLOUT_POINT_PATH = "ToolboxPrefabs/CalloutPoint";
        public static string CALLOUT_DEFAULT_TEXTPANEL = "ToolboxPrefabs/CalloutDefaultTextPanel";
    }

    // Enum where each item is the exact name of the hotspot prefab.
    [System.Serializable]
    public enum HotspotPrefabNames
    {
        HotspotButtonDefault, HotspotButtonCustom1, HotspotButtonCustom2
    }


}