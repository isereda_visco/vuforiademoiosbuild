﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Visco
{
    public class StatesManager : MonoBehaviour
    {
        public static UnityAction<EventId> SharedSpotAction;

        [HideInInspector]
        public State currentState;
        [HideInInspector]
        public State previousState;

        public bool presentationIsLinear;

        [Tooltip("Assign if presentation type is linear; if presentation type is hierarchy it will be automatically added")]
        [SerializeField]
        public GameObject rootHotSpotObject;


        private GameObject currentHotspot;
        private List<State> States;

        private bool StateChangeUnderway;
        private bool StateChangeIsForward;
        private State currentExecutingState;
        private State upcomingState;

        private MainCameraManager mainCameraManager;



        private void Start()
        {
            mainCameraManager = Main.Instance.GetMainCameraManager();

            States = new List<State>(gameObject.transform.childCount);
            foreach (Transform t in gameObject.transform)
            {
                var state = t.gameObject.GetComponent<State>();
                if (state && state.gameObject.activeSelf)
                {
                        States.Add(state);
                }
            }


            Invoke("invokeStartState", 0.5f);
            //invokeStartState();

        }


        public void OpenScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        #region HotSpots 

        private void OnEnable()
        {
            BackButton.ButtonClickAction += ReceiveClickBackbutton;
        }

        private void OnDisable()
        {
            BackButton.ButtonClickAction -= ReceiveClickBackbutton;
        }

        public void ReceiveClickBackbutton()
        {
            currentHotspot.GetComponent<Hotspot>().ExecuteBackButtonActions();
            currentHotspot.GetComponent<Hotspot>().ExecuteCameraLeaveActions();
            if (currentHotspot != rootHotSpotObject)
            {
                ApplySoloHotspot(currentHotspot.transform.parent.gameObject);
            }
                
        }

        /// <summary>
        /// Whenever a hotspot's button is clicked, this is the main entrance in StatesManager where the event gets processed.
        /// The hotspot itself might have additional events added to the buttons click-event (specifically cameraStartGoingToActions)
        /// </summary>
        /// <param name="goHotspot"></param>
        public void ReceiveClickFromHotSpot(GameObject goHotspot)
        {
            // Did user click on child hotspot?
            if (goHotspot.transform.parent.gameObject == currentHotspot)
            {
                goHotspot.GetComponent<Hotspot>().ExecuteCameraGoToChildActions();
            }

            // Did user click on Sibling hotspot?
            if (goHotspot.transform.parent.gameObject == currentHotspot.transform.parent.gameObject)
            {
                goHotspot.GetComponent<Hotspot>().ExecuteCameraGoToSiblingActions();
            }


            if (goHotspot != currentHotspot)
            {
                ApplySoloHotspot(goHotspot);
            }
        }

        public void ReceiveClickFromContextButton(EventId action)
        {
            DoSharedAction(action);
        }


        /// <summary>
        /// This is essentially GotoHotspot(hs) - move the camera there, set up buttons, menus and other components the hotspot might have defined on its gameobject.
        /// </summary>
        /// <param name="goHotspot"></param>


        private void ApplySoloHotspot(GameObject goHotspot)
        {
            Debug.Log("User-navigation initiated 'Apply hotspot '" + goHotspot.name + "'\n");

            currentHotspot = goHotspot;
            Hotspot hotspot = goHotspot.GetComponent<Hotspot>();
            UIMain.Instance.HideContextMenu();

            bool teleportMode = hotspot.hotspotMoveToMode == HotspotMoveToMode.Teleport;
            mainCameraManager.SetNewTarget(hotspot, teleportMode);

            // Old eventsystem. To be phased out
            DoSharedAction(hotspot.PressHotSpotEvent);

            // Show/hide other hotspots when going to this hotspot.
            SetHotspotTreeVisibilityState(goHotspot);


            // Show tools attached to hotspot - text popups, context menus etc.
            if (hotspot.OverrideStateViscoComponents)
                ApplyCommonTools(hotspot.MyCommonTools);
            else
                ApplyCommonTools(currentState.GetCommonTools());

            // Hide or show backbutton
            UIMain.Instance.ActivateBackButton(!hotspot.hideBackButton);

            // Finally - does the hotspot trigger a state-change
            if (hotspot.State != null)
            {
                if (hotspot.State.MyHotspot == goHotspot)
                {
                    throw new Exception("Warning: Infinite loop condition State's hotspot and hotspot's State. ::" + goHotspot.name);
                    return; // i suppose this return will never be hit
                }
                BeginStateChange(hotspot.State, true);
            }
        }

        /// <summary>
        /// When changing state, if a hotspot change is also needed, this is the call is used to set up the state's hotspot's buttons/menus etc it might have.
        /// It does NOT trigger the actual camera move - that is done elsewhere in StateManagers change-state code.
        /// </summary>
        /// <param name="state"></param>
        private void ApplyStatesHotspot(State state)
        {
            Debug.Log("State-initiated 'Apply hotspot' (" + state.MyHotspot.name + ")\n");

            GameObject goHotspot = state.MyHotspot;
            currentHotspot = goHotspot;
            Hotspot hotspot = goHotspot.GetComponent<Hotspot>();

            UIMain.Instance.HideContextMenu();

            // Legacy event system - trigger signal
            DoSharedAction(hotspot.PressHotSpotEvent);

            // Hide or show other hotspots when going to this one
            SetHotspotTreeVisibilityState(goHotspot);

            // Show tools attached to hotspot - text popups, context menus etc.
            if (hotspot.OverrideStateViscoComponents)
                ApplyCommonTools(hotspot.MyCommonTools);
            else
                ApplyCommonTools(state.GetCommonTools());

            // Hide/show backbutton
            UIMain.Instance.ActivateBackButton(!hotspot.hideBackButton);

            if (hotspot.State != null)
            {
                if (hotspot.State == state)
                {
                    throw new Exception("Warning: Infinite loop condition State's hotspot and hotspot's State. ::" + goHotspot.name);
                    return; // Nevr reached. Anyways, there can still be infinite loops if states-hotspots, if they form a longer chain.
                    // TODO: Make startup-verification there are no suc loops - they hang the editor. Only kill editor process works.
                }
                BeginStateChange(hotspot.State, true);
            }

        }





        // Set visibilty on the hotspots depending on which one is selected, presentation type etc.
        private void SetHotspotTreeVisibilityState(GameObject selectedHotspot)
        {
            GameObject rootNode = GetHotspotTreeRootNode(selectedHotspot);

            //Debug.Log("Active hotspot is: " + selectedHotspot.name + "\n");
            SetHotspotVisibilityRecursive(rootNode, false); // First hide ALL

            //Debug.Log("Turning on children of selected\n");
            SetHotspotVisibilityRecursive(selectedHotspot, true, 1); // Now show the children of selected hotspot

            // Finally - if selected hotspot has "AlwaysShowSiblings" then let's show the siblings - code below.
            if (selectedHotspot != rootNode)
            {
                if (selectedHotspot.GetComponent<Hotspot>().HideSiblingsWhenActive == false)
                {
                    //      Debug.Log("Possibly turning on some siblings of " + selectedHotspot.name + "\n");
                    GameObject parentOfCurrentHotspot = selectedHotspot.transform.parent.gameObject;
                    if (parentOfCurrentHotspot.GetComponent<Hotspot>())
                    {
                        foreach (Transform child in parentOfCurrentHotspot.transform)
                        {
                            Hotspot siblingHotspot = child.gameObject.GetComponent<Hotspot>();
                            if (child != selectedHotspot && siblingHotspot)
                            {
                                siblingHotspot.Visible = true;
                            }
                        }
                    }
                }
            }

            // The selected hotspot itself - show or hide?
            if (selectedHotspot.GetComponent<Hotspot>().ButtonShowMode != hotspotButtonShowMode.WhenParentOrSelfActive)
            {
                selectedHotspot.GetComponent<Hotspot>().Visible = false;
            }

        }


        #region HotspotHierarchyHelpFunctions
        // Travel hotspot tree upwards until we find the topmost parent.   (remember we can have multiple, separate hotspot hierarchies )
        private GameObject GetHotspotTreeRootNode(GameObject hs)
        {

            while (hs.transform.parent != null && hs.transform.parent.gameObject.GetComponent<Hotspot>() != null)
            {
                hs = hs.transform.parent.gameObject;
            }
            return hs;
        }


        // Recursively set visibility on ALL childnodes to node
        private void SetHotspotVisibilityRecursive(GameObject node, bool Visible, int depth = 9)
        {
            Hotspot hsm = node.GetComponent<Hotspot>();
            if (!hsm) return;

            hsm.Visible = Visible;

            if (depth > 0)
            {
                foreach (Transform child in node.transform)
                {
                    if (child.gameObject.activeInHierarchy)
                        SetHotspotVisibilityRecursive(child.gameObject, Visible, depth - 1);
                }
            }
        }



        #endregion

        public void DoSharedAction(EventId action)
        {
            if (SharedSpotAction != null) SharedSpotAction(action);
        }

        #endregion

        #region States 

        /// <summary>
        /// This function is called half a second after program start, to set up start state of scene.
        /// </summary>
        private void invokeStartState() // invoking form Start
        {
            currentState = States[0];

            // This is essentially the "Init Scene".
            currentState.ExecuteOnArrivalEventsIgnoreAnyWait();

            if (currentState.showStateDescription)
            { 
                UIMain.Instance.ShowStateStatus(States.IndexOf(currentState), States.Count, currentState.StateDescription);
            }

            if (currentState.GetHotSpot() != null)
            {
                ApplyStatesHotspot(currentState);
                mainCameraManager.SetNewTarget(currentState.GetHotSpot().GetComponent<Hotspot>(), CalculateCameraMoveMode(currentState));
            }
                

            
            // Set start-states animations at their starting-positions.
            foreach (Transform child in currentState.transform)
            {
                GameObject go = child.gameObject;
                var viscoAnimation = go.GetComponent<IViscoAnimatable>();
                if (viscoAnimation != null)
                {
                    viscoAnimation.SetAnimationClipToBegin();
                }
            }

            


            if (currentState.autoPlayForward)
            {
                GoToNextState();
            }
        }

        public void GoToNextState()
        {
            if (StateChangeUnderway == false) 
            {
                // If we are standing still at state, then next state is trivial.
                if (currentState != States[States.Count - 1])
                {
                    BeginStateChange(States[States.IndexOf(currentState) + 1], true);
                }
            }
            else                                
            {
                // But if we are already transitioning towards next state, then next means 'nextnext'
                if (upcomingState != States[States.Count - 1])
                {
                    BeginStateChange(States[States.IndexOf(upcomingState) + 1], true);
                }
            }
        }

        public void GoToNextStateLoopMode()
        {
            Debug.Log("Warning: GotoNextStateLoopMode is disabled in code for now\n");
            GoToNextState();
            //State nextState = States[(States.IndexOf(currentState) + 1) % States.Count];
            //BeginStateChange(nextState, true);
        }

        public void GoToPreviousState()
        {
            if (StateChangeUnderway == false)
            {
                if (currentState != States[0])
                    BeginStateChange(States[States.IndexOf(currentState) - 1], false);
            }
            else
            {
                if (upcomingState != States[0])
                    BeginStateChange(States[States.IndexOf(upcomingState) - 1], false);
            }
        }

        public void GoToPreviousStateLoopMode()
        {
            Debug.Log("Warning: GotoPreviousStateLoopMode is disabled in code for now\n");
            GoToPreviousState();
            //State prevState = States[(States.IndexOf(currentState) - 1 + States.Count) % States.Count];
            //BeginStateChange(prevState, false);
        }


        private void BeginStateChange(State newState, bool ForwardInSequence)
        {

            if (StateChangeUnderway)
            {
                Debug.Log("------ FASTSKIP START: Premature skip - fast worwards,backwards ----\n");
                FinishStateChange();
                Debug.Log("------ FASTSKIP END:   Premature skip - fast worwards,backwards ----\n");
            }

            Debug.Log("-------------------------START  " + currentState.name + "  -> " + newState.name + "-------------------\n");

            StateChangeIsForward = ForwardInSequence;
            if (ForwardInSequence)
                currentExecutingState = currentState;
            else
                currentExecutingState = newState;

            StateChangeUnderway = true;
            upcomingState = newState;

            // If there are old events that are still waiting to run, run them immediately
            foreach (State s in States)
                s.InstantlyExecuteAllQuedEvents();


            // If the state has a hotspot - begin moving camera there. 
            // Hotspot's tools/buttons/textboxes will be setup when cam arrived, in FinishStateChange()
            GameObject hotspot = newState.GetHotSpot();
            if (hotspot)
            {
                bool teleportMode = CalculateCameraMoveMode(newState);
                mainCameraManager.SetNewTarget(hotspot.GetComponent<Hotspot>(), teleportMode);
            }

            if (ForwardInSequence)
                currentExecutingState.ExecuteStateForwardEventsWithOptionalDelays();
            else
                currentExecutingState.ExecuteStateBackwardsEventsWithOptionalDelays(); // new state

        }



        // TODO:  Consoloidate Update() and CameraArrivedAtViewpoint()
        string dbgMsg;
        private void Update()  // Here we check if any activities are still going - animations, camera move etc, before calling it state transition finished.
        {
            if (StateChangeUnderway == true)
            {
                bool cameraIsInTransition = mainCameraManager.CameraHasArrivedAtHotspot == false;

                string tmp = "Transition to state " + upcomingState.name + " waiting for: Animation: " + currentExecutingState.HasActiveAnimation + "\t Camera-move: " + cameraIsInTransition + "\n";
                if (tmp != dbgMsg)
                {
                    dbgMsg = tmp;
                    Debug.Log(dbgMsg);

                }

                // Check if we are done with animations, camera movement etc.
                if (currentExecutingState.HasActiveAnimation == true)
                    return;

                if ( cameraIsInTransition )
                    return;

                FinishStateChange();
            }
        }


        // This is the callback that the camera manager calls when it feels that the camera has arrived at the target place.
        // StatesManager can now look at the state and make sure it's "wait for camera" animations are triggered.
        public void CameraArrivedAtViewpoint(GameObject goHotspot)
        {
            // Only if it is state-forward move or program start.

            // TODO: should we check on currentState or currentExecutingState here?????
            if (previousState == null || States.IndexOf(currentState) > States.IndexOf(previousState) )
            {
                Debug.Log("StatesManager:  Camera arrived at destination [" + goHotspot.name + "] says CameraManager\n");

                // First execute State's actions that are to trigger when camera arrives.
                if (currentState.MyHotspot == goHotspot)
                {
                    foreach (Transform child in currentState.transform)
                    {
                        GameObject go = child.gameObject;
                        IViscoAnimatable animationClip = go.GetComponent<IViscoAnimatable>();

                        if (animationClip != null)
                        {
                            if (animationClip.WaitForCamera)
                            {
                                Debug.Log("Playing 'wait-for-cam' animation clip '" + animationClip.GameObject.name + "'\n");
                                animationClip.PlayAnimationClip();
                            }
                        }

                    }
                }
            }
            // Execute events that are assigned directly to the hotsåpt
            Debug.Log("StatesManager: Executing hotspot [" + goHotspot.name +  "] Camera Arrive Actions\n");
            goHotspot.GetComponent<Hotspot>().ExecuteCameraArriveActions();

        }


        private void FinishStateChange()
        {

            Debug.Log("StatesManager: Finishing state change [" + currentExecutingState.name + "] -> ["  + upcomingState.name + "]\n");

            //ApplyCommonTools(upcomingState.GetCommonTools());

            GameObject hotspot = upcomingState.GetHotSpot();
            if (hotspot != null)
            {
                ApplyStatesHotspot(upcomingState);
            }

            
            if (upcomingState.showStateDescription)
            {
                UIMain.Instance.ShowStateStatus(States.IndexOf(upcomingState), States.Count, upcomingState.StateDescription);
            }



            previousState = currentExecutingState;
            currentState = upcomingState;

            currentExecutingState = null;
            upcomingState = null;
            StateChangeUnderway = false;
            Debug.Log("StatesMananger: Arrived at " + currentState.name + "\n");
            Debug.Log("------------------------- END    ------------------------------------------\n");

            // From here on, State is considered 100% changed. 

            currentState.ExecuteOnArrivalEventsIgnoreAnyWait();

            if (currentState.autoPlayForward && StateChangeIsForward)
            {
                Debug.Log("Auto Play Forward state ' " + currentState.name + "'\n");
                GoToNextState();
            }
            else if (currentState.autoPlayBackward && StateChangeIsForward == false)
            {
                Debug.Log("Auto Play Backwards state ' " + currentState.name + "'\n");
                GoToPreviousState();
            }

        }







        public void AnimationScrubberJumpRequest(GameObject goAnimation, float positionInClipToStartAt)
        {
            // First find what state the requested animation clip belongs to
            State requestedState = goAnimation.transform.parent.gameObject.GetComponent<State>();
            if (requestedState == null)
            {
                Debug.Log("ERROR: ViscoAnimationScrubber indicating an animation gameobject that does not seem to be a child of a State");
                return;
            }

            int curStateIdx = States.IndexOf(currentState);
            int reqStateIdx = States.IndexOf(requestedState);

            if ( reqStateIdx < curStateIdx )    // Scrubbing FAST REWIND over multiple states
            {
                // Clear pending events on currently playing state
                if (StateChangeUnderway)
                {
                    currentExecutingState.InstantlyExecuteAllQuedEvents();
                    currentState = upcomingState;
                    StateChangeUnderway = false;
                    currentExecutingState = null;
                    curStateIdx = States.IndexOf(currentState); // update curStateIdx in case we were just playing a state forward.
                }

                for (int i = curStateIdx - 1; i >= reqStateIdx; i--)
                {
                    States[i].ExecuteAllStateBackwardsEventsIgnoreAnyWait();
                    currentState = States[i];
                }
                GoToNextState();  
                requestedState.SetAnimationClipsTime(positionInClipToStartAt);
            }
            else if (reqStateIdx > curStateIdx)    //  SCRUBBING FAST FORWARD OVER MULTIPLE STATES
            {

                if (StateChangeUnderway)   // Clear pending events
                {
                    currentExecutingState.InstantlyExecuteAllQuedEvents();
                    currentState = upcomingState;
                    StateChangeUnderway = false;
                    currentExecutingState = null;
                }
                curStateIdx = States.IndexOf(currentState); // update curStateIdx in case we were just playing a state forward.

                for (int i = curStateIdx; i < reqStateIdx; i++)
                {
                    States[i].ExecuteAllStateForwardEventsIgnoreAnyWait();
                    currentState = States[i];
                }
                GoToNextState(); 
                requestedState.SetAnimationClipsTime(positionInClipToStartAt);
            }
            
            else if (reqStateIdx == curStateIdx) //   SCRUBBING WITHIN STATE
            {
                // Just update the time within the current clip.
                Debug.Log("Update time pos withing same state/clip\n");
                requestedState.SetAnimationClipsTime(positionInClipToStartAt);
            }

        }

        /// <summary>
        /// Go directly to specified state, executing any events on the way, ignoring any wait-for-cam or delays.
        /// </summary>
        public void GotoState(State requestedState)
        {
            currentExecutingState = null;
            upcomingState = null;
            StateChangeUnderway = false;


            int curStateIdx = States.IndexOf(currentState);
            int reqStateIdx = States.IndexOf(requestedState);

            if (reqStateIdx < curStateIdx)    // Scrubbing FAST REWIND over multiple states
            {
                // Clear pending events on currently playing state
                if (StateChangeUnderway)
                {
                    currentExecutingState.InstantlyExecuteAllQuedEvents();
                    currentState = upcomingState;
                    StateChangeUnderway = false;
                    currentExecutingState = null;
                    curStateIdx = States.IndexOf(currentState); // update curStateIdx in case we were just playing a state forward.
                }

                for (int i = curStateIdx - 1; i >= reqStateIdx; i--)
                {
                    States[i].ExecuteAllStateBackwardsEventsIgnoreAnyWait();
                    currentState = States[i];
                }

                GameObject hotspot = currentState.GetHotSpot();
                if (hotspot)
                {
                    bool teleportMode = CalculateCameraMoveMode(currentState);
                    mainCameraManager.SetNewTarget(hotspot.GetComponent<Hotspot>(), teleportMode);
                }

                ApplyStatesHotspot(requestedState);
                //Main.Instance.GetMainCameraManager().SetNewTarget(requestedState.MyHotspot.GetComponent<Hotspot>(), requestedState.teleportMode);
                requestedState.SetAnimationClipsTime(0);
            }
            else if (reqStateIdx > curStateIdx)    //  SCRUBBING FAST FORWARD OVER MULTIPLE STATES
            {

                if (StateChangeUnderway)   // Clear pending events
                {
                    currentExecutingState.InstantlyExecuteAllQuedEvents();
                    currentState = upcomingState;
                    StateChangeUnderway = false;
                    currentExecutingState = null;
                }
                curStateIdx = States.IndexOf(currentState); // update curStateIdx in case we were just playing a state forward.

                for (int i = curStateIdx; i < reqStateIdx; i++)
                {
                    currentState = States[i];
                    currentState.ExecuteAllStateForwardEventsIgnoreAnyWait();
                    
                }
                currentState = requestedState;
                requestedState.SetAnimationClipsTime(0);

                GameObject hotspot = currentState.GetHotSpot();
                if (hotspot)
                {
                    bool teleportMode = CalculateCameraMoveMode(currentState);
                    mainCameraManager.SetNewTarget(hotspot.GetComponent<Hotspot>(), teleportMode);
                }
                ApplyStatesHotspot(requestedState);
            }

            else if (reqStateIdx == curStateIdx) //   SCRUBBING WITHIN STATE
            {
                // Just update the time within the current clip.
                Debug.Log("Update time pos withing same state/clip\n");
                requestedState.SetAnimationClipsTime(0);
            }

        }


        /// <summary>
        /// 'Teleport' to a state, ignoring any state'events on states between current and target state.
        /// </summary>
        /// <param name="state"></param>
        public void GotoStateSkippingIntermediaryStateEvents(State state)
        {
            Debug.Log("---------------------------------------\n");
            Debug.Log("StatesManager:  Goto state '" + state.name + "' skipping intermediary statee events\n");

            // Clear any qued events from current state. (so they dont happen AFTER we have arrived)
            currentState.InstantlyExecuteAllQuedEvents();


            currentExecutingState = null;
            upcomingState = null;
            StateChangeUnderway = false;

            currentState = state; // Voila! This needs to be changed from a global var to something more rock solid.
            currentState.SetAnimationClipsTime(0);

            GameObject hotspot = currentState.GetHotSpot();
            if (hotspot)
            {
                bool teleportMode = CalculateCameraMoveMode(currentState);
                mainCameraManager.SetNewTarget(hotspot.GetComponent<Hotspot>(), teleportMode);
            }
            currentState.ExecuteOnArrivalEventsIgnoreAnyWait();
            ApplyStatesHotspot(state);
        }




        // Going to a state with either GotoNextSsate or GotoState(), camera will go to the States hotspot, 
        // either sliding or in teleport mode, depending on what is specified on the state.
        // When user free-navigates hotspots without changing states, moving the camera to a click-on hotspot
        // can also be smooth or teleport, depending on option on the hotspot.
        // So, both State and Hotspot has a setting for Slide vs Teleport. 
        // State calling hotspot uses state's preference, unless State explicitly uses the "UseHotspotPreference" option.

        private bool CalculateCameraMoveMode(State state)
        {
            Hotspot hotspot = state.MyHotspot.GetComponent<Hotspot>();
            bool teleportMode = false;

            if (state.statesHotspotMoveToMode == StatesHotspotMoveToMode.ForceTeleport)
                teleportMode = true;
            else if (state.statesHotspotMoveToMode == StatesHotspotMoveToMode.UseHotspotSetting)
            {
                if (hotspot.hotspotMoveToMode == HotspotMoveToMode.Teleport)
                    teleportMode = true;
            }

            return teleportMode;
        }






        #endregion

        #region Common Methods 
        public void ApplyCommonTools(CommonTools commonTools) { UIMain.Instance.ApplyCommonTools(commonTools); }

        #endregion
    }

    [Serializable]
    public class CommonTools
    {
        // public int intprop;
        public ViscoButtonList viscoButtonList;
        public Vector2 viscoButtonsSize;
        public ViscoDescriptionManager viscoDescriptionManager;
        public ViscoVideoManager viscoVideoManager;
        public ViscoImageManager viscoImageManager;
        public ViscoSideMenuManager sideMenuManager;

        public CommonTools(ViscoButtonList vButtonList, ViscoDescriptionManager vDescriptionManager,
                           ViscoVideoManager vVideoManager, ViscoImageManager vImageManager, ViscoSideMenuManager sMenuManager)
        {
            viscoButtonList = vButtonList;
            viscoButtonsSize = vButtonList != null ? vButtonList.GetButtonsSize() : Vector2.zero;
            viscoDescriptionManager = vDescriptionManager;
            viscoVideoManager = vVideoManager;
            viscoImageManager = vImageManager;
            sideMenuManager = sMenuManager;
        }

        public CommonTools(CommonTools cTools)
        {
            viscoButtonList = cTools.viscoButtonList;
            viscoDescriptionManager = cTools.viscoDescriptionManager;
            viscoVideoManager = cTools.viscoVideoManager;
            viscoImageManager = cTools.viscoImageManager;
            sideMenuManager = cTools.sideMenuManager;
        }

        public List<GameObject> GetCustomDescriptionTextBoxesList() { return viscoDescriptionManager != null ? viscoDescriptionManager.GetCustomTextBoxsList() : null; }

        public VideoStruct GetVideo() { return viscoVideoManager != null ? viscoVideoManager.GetClip() : null; }

        public ImageStruct GetImage() { return viscoImageManager != null ? viscoImageManager.GetImage() : null; }

        public List<ContextButtonsStructure> GetContentButtonsList()
        {
            // Original code below - horror example of bad code. 

            //return viscoButtonList != null ? (viscoButtonList.ButtonsList.Count != 0 ? viscoButtonList.ButtonsList : null) : null;

            // The fix:

            if ( viscoButtonList==null || viscoButtonList.ButtonsList.Count == 0)
                return null;
            else
                return viscoButtonList.ButtonsList;


        }
        public Vector2 GetContentButtonsSize() { return viscoButtonsSize; }

        public List<VideoStruct> GetVideosListFromSideMenu() { return sideMenuManager != null ? sideMenuManager.GetVideosList() : null; }

        public List<ImageStruct> GetImagesListFromSideMenu() { return sideMenuManager != null ? sideMenuManager.GetImagesList() : null; }

    }
}

