﻿using UnityEngine;
namespace Visco
{
    public class ViscoAnimationManager : MonoBehaviour
    {
        private Animation Animation { get { return GetAnimation(); } }
        private Animation _animation;

        private float customSpeed = 1f;

        Animation GetAnimation()
        {
            if (!_animation) _animation = GetComponent<Animation>();
            return _animation;
        }

        public void PlayAnimationClip(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation.Play(animation.name);
        }

        /// <summary>
        /// SetCustomAnimationSpeed first needed, if not, normal speed will be used
        /// </summary>
        /// <param name="animation"></param>
        public void PlayAnimationClipWithCustomSpeed(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation[animation.name].speed = customSpeed;
            Animation.Play(animation.name);
            ResetCustomSpeed();
        }

        public void PlayAnimationClipBackwards(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation[animation.name].normalizedTime = 1f;
            Animation[animation.name].speed = -1f;
            Animation.Play(animation.name);
        }

        public void PlayAnimationClipBackwardsWithCustomSpeed(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation[animation.name].normalizedTime = 1f;
            Animation[animation.name].speed = customSpeed;
            Animation.Play(animation.name);
            ResetCustomSpeed();
        }

        public void SetAnimationClipToBegin(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation[animation.name].speed = 0.0f;
            Animation.Play(animation.name);
        }

        public void SetAnimationClipToEnd(AnimationClip animation)
        {
            Animation.AddClip(animation, animation.name);
            Animation[animation.name].normalizedTime = 1f;
            Animation[animation.name].speed = 0.0f;
            Animation.Play(animation.name);
        }

        public void SetCustomAnimationSpeed(float speed) { customSpeed = speed; }

        void ResetCustomSpeed() { customSpeed = 1; }

        public void Stop()
        {
            if (Animation) Animation.Stop();
        }

    }
}
