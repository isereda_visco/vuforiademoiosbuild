﻿using System;
using UnityEngine;
using UnityEngine.Video;

namespace Visco
{
    [Serializable]
    public class VideoStruct
    {
        public VideoClip VideoClip;
        public Sprite ThumbnailSprite;
        public bool ShowName;
        public string VideoName;
        public bool useVideoPlayer;
        public VideoAspectRatio VidApectRatio;

        public VideoStruct(VideoStruct vs)
        {
            VideoClip = vs.VideoClip;
            ShowName = vs.ShowName;
            VideoName = vs.VideoName;
            VidApectRatio = vs.VidApectRatio;
            useVideoPlayer = vs.useVideoPlayer;
        }
    }

    public class ViscoVideoManager : MonoBehaviour
    {
        [Header("The clip will be shown in default video button")] [SerializeField] private VideoStruct MyClip;

        /// <summary>
        /// returns null if VideoClip field is empty
        /// </summary>
        /// <returns></returns>
        public VideoStruct GetClip()
        {
            return MyClip.VideoClip != null ? MyClip : null;
        }

        public void ShowVideo() { UIMain.Instance.ShowVideo(MyClip); }

        public void HideVideo() { UIMain.Instance.ShowStateVideo(); }

    }
}