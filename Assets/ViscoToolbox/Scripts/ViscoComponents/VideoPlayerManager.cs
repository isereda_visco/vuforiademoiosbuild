﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Visco
{
    public class VideoPlayerManager : MonoBehaviour, IChangePlace
    {
        [SerializeField] private Slider ForwardingSlider;
        [SerializeField] private Button PlayButton;
        [SerializeField] private Image PlayButtonImage;
        [SerializeField] private Sprite PlaySprite;
        [SerializeField] private Sprite PauseSprite;
        [SerializeField] private Button FullscreenModeButton;
        [SerializeField] private Button MutedButton;
        [SerializeField] private Image MutedButtonImage;
        [SerializeField] private Sprite MutedSprite;
        [SerializeField] private Sprite UnMutedSprite;

        private Image ThumbNailImage;
        [SerializeField] private RawImage _rawImage;
        private VideoStruct _myVideoStruct;
        private VideoClip _myVideoClip;
        private VideoPlayer _videoPlayer;


        private bool _isPlaying;
        private bool _fullScreenMode;
        private bool _muted;
        private bool _sliderPressed;
        public bool ShowName;
        public string Name;
        private Text _nameText;

        private Vector2 _startPosition, _centeredPosition, _targetPosition;
        private Vector3 _startScale, _showScale, _targetScale;
        private RectTransform _myRectTransform;
        private RectTransform _CanvasRect;
        [SerializeField] private Transform _myParent;
        public float Speed;
        public float ScaleSpeed;
        public float ScaleDuration;
        private int _siblingIndex;
        private VideoAspectRatio _myAspectRatio;

        void Awake() { Init(); }

        void OnEnable()
        {
            PlayButton.onClick.AddListener(PlayClick);
            MutedButton.onClick.AddListener(MutedClick);
            FullscreenModeButton.onClick.AddListener(FullscreenModeClick);
            ForwardingSlider.onValueChanged.AddListener(HandleSliderChange);
            SliderHandler.PressedAction += SliderPressHandle;
        }

        void OnDisable()
        {
            PlayButton.onClick.RemoveAllListeners();
            MutedButton.onClick.RemoveAllListeners();
            FullscreenModeButton.onClick.RemoveAllListeners();
            ForwardingSlider.onValueChanged.RemoveAllListeners();
            SliderHandler.PressedAction -= SliderPressHandle;
        }

        void Init()
        {
            InitNewPosition();

            Speed = Speed != 0 ? Speed : 3f;
            ScaleSpeed = ScaleSpeed != 0 ? ScaleSpeed : 3f;
            ScaleDuration = ScaleDuration != 0 ? ScaleDuration : 0.5f;
        }
        public void ResetVideoStruct() { _myVideoStruct = null; }
        public VideoStruct GetVideoStruct() { return _myVideoStruct; }

        public void InitButton(VideoStruct vStruct, int sibling)
        {
            _muted = false;
            _myVideoStruct = vStruct;
            _myAspectRatio = vStruct.VidApectRatio;
            if (vStruct.ThumbnailSprite != null) ThumbNailImage.sprite = vStruct.ThumbnailSprite;
            if (vStruct == null)
            {
                throw new System.Exception(name + "vStruct");
            }
            _siblingIndex = sibling;
            _myVideoClip = vStruct.VideoClip;

            ShowName = vStruct.ShowName;
            Name = vStruct.VideoName;
            StartCoroutine(InitVideo());
            InitSlider();
        }

        public IEnumerator InitVideo()
        {
            if (ShowName)
            {
                _nameText = GetComponentInChildren<Text>();
                _nameText.text = Name;
            }
            // _videoPlayer.playOnAwake = true;
            _videoPlayer.clip = _myVideoClip;

            RenderTexture rt = new RenderTexture(256, 256, 0, RenderTextureFormat.Default);
            _rawImage.texture = rt;
            _videoPlayer.targetTexture = rt;
            ulong length = _myVideoClip.frameCount;
            _videoPlayer.frame = (long) length / 2;
            yield return new WaitForEndOfFrame();
            //  _videoPlayer.Stop();
            _videoPlayer.Pause();
            _videoPlayer.aspectRatio = _myAspectRatio;
        }

        private void FullscreenModeClick()
        {
            _fullScreenMode = !_fullScreenMode;
            _targetPosition = _fullScreenMode ? _centeredPosition : _startPosition;
            _targetScale = _fullScreenMode ? _showScale : _startScale;

            if (_fullScreenMode)
            {
                _myRectTransform.SetParent(_CanvasRect);
                _startPosition = _myRectTransform.position;
                _myRectTransform.SetAsLastSibling();
                if (ShowName && _nameText != null) _nameText.gameObject.SetActive(false);

                _myRectTransform.DOMove(_targetPosition, ScaleDuration);
                _myRectTransform.DOScale(_targetScale, ScaleDuration); //.OnComplete(PlayVideo);
            }
            else
            {
                if (ThumbNailImage != null) ThumbNailImage.gameObject.SetActive(true);
                if (ShowName && _nameText != null)
                {
                    _nameText.gameObject.SetActive(true);
                }
                _myRectTransform.DOMove(_targetPosition, ScaleDuration).OnComplete(EndMove);
                _myRectTransform.DOScale(_targetScale, ScaleDuration);
                //StartCoroutine(StopVideo());
            }
        }

        private void MutedClick()
        {
            _muted = !_muted;
            MutedButtonImage.sprite = _muted ? MutedSprite : UnMutedSprite;
            // _videoPlayer.SetDirectAudioMute(0,_muted);
        }

        private void PlayClick()
        {
            _isPlaying = !_isPlaying;
            PlayButtonImage.sprite = _isPlaying ? PauseSprite : PlaySprite;

            if (_isPlaying) PlayVideo();
            else StartCoroutine(StopVideo());
        }

        void PlayVideo()
        {
            if (ThumbNailImage != null) ThumbNailImage.gameObject.SetActive(false);
            _videoPlayer.Play();
        }

        IEnumerator StopVideo()
        {
            _videoPlayer.Pause();
            yield return new WaitForEndOfFrame();
        }

        void EndMove()
        {
            _myRectTransform.SetParent(_myParent);
            _myRectTransform.SetSiblingIndex(_siblingIndex);
        }
        private void InitSlider()
        {
            ForwardingSlider.minValue = 0f;
            ForwardingSlider.maxValue = (float) _videoPlayer.clip.length;
            Debug.Log(_videoPlayer.clip.length);
        }
        private void HandleSliderChange(float f) { StartCoroutine(ChangeFrame(f)); }

        IEnumerator ChangeFrame(float f)
        {
            if (_sliderPressed)
            {
                _videoPlayer.time = f;
                yield return new WaitForSeconds(0.15f);
                _sliderPressed = false;
            }
        }



        private void SliderPressHandle(bool b)
        {
            if (b) _sliderPressed = true;
            else
            {
                StartCoroutine(ChangeFrame(ForwardingSlider.value));
            }
        }


        public void InitNewPosition()
        {
            if (_videoPlayer == null) _videoPlayer = GetComponent<VideoPlayer>();
            if (_rawImage == null) _rawImage = GetComponentInChildren<RawImage>();
            if (_myRectTransform == null) _myRectTransform = GetComponent<RectTransform>();
            if (_CanvasRect == null) _CanvasRect = UIMain.Instance.GetMainCanvas().GetComponent<RectTransform>();
            _myParent = _myRectTransform.parent;
            _startPosition = _myRectTransform.position;
            _centeredPosition = _CanvasRect.TransformPoint(Vector3.zero); // Vector2.zero;

            _targetPosition = _centeredPosition;
            _startScale = _myRectTransform.localScale;
            _targetScale = _startScale;
            _showScale = new Vector3(UIMain.Instance.GetCanvasScale().x / _myRectTransform.sizeDelta.x, UIMain.Instance.GetCanvasScale().y / _myRectTransform.sizeDelta.y);
        }

        void Update()
        {
            if (_isPlaying && !_sliderPressed)
                ForwardingSlider.value = (float) _videoPlayer.time;
        }
    }
}