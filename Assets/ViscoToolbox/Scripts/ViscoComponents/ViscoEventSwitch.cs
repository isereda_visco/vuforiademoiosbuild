﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Visco
{
    public class ViscoEventSwitch : MonoBehaviour
    {
        public List<FromViscoEventToUnityEvent> ViscoToUnitySwitches;

        void OnEnable() { StatesManager.SharedSpotAction += HandleSharedAction; }


        void OnDisable() { StatesManager.SharedSpotAction -= HandleSharedAction; }

        private void HandleSharedAction(EventId arg0)
        {
            foreach (FromViscoEventToUnityEvent element in ViscoToUnitySwitches)
            {
                if (element.InputViscoEvent.Value == arg0.Value)
                {
                    print(element.OutputUnityEvent.ToString());
                    element.OutputUnityEvent.Invoke();
                }
            }
        }

    }

    [Serializable]
    public class FromViscoEventToUnityEvent
    {
        public EventId InputViscoEvent;
        public UnityEvent OutputUnityEvent;
    }
}

