﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Visco
{
    [Serializable]
    public class ContextButtonsStructure
    {
        [Header("Required parameters")] [Tooltip("ButtonName will be text on the button")] public string ButtonName;
        [Tooltip("ButtonState - the state when button clicked")] public EventId ButtonEvent;
        public UnityEvent UnityEvents;
        [Header("Optional parameters")] public bool useSecondaryButtonState;
        public string SecondaryButtonName;
        public EventId SecondaryButtonEvent;
        public UnityEvent SecondaryUnityEvents;
        public Transform CustomButton;
        public bool UseCustomSizeAndPlaceForCustomButton;

    }

    public class ViscoButtonList : MonoBehaviour
    {
        [HideInInspector] public List<ContextButtonsStructure> ButtonsList = new List<ContextButtonsStructure>();
        [SerializeField] private Vector2 ButtonsSize;

        public Vector2 GetButtonsSize() { return ButtonsSize != Vector2.zero ? ButtonsSize : UIMain.GetInstance().DefaultContextMenuButtonsSize; }

        private List<ContextButtonsStructure> previousButtonList;

        public void ShowThisMenu()
        {
            previousButtonList = UIMain.Instance.GetPreviousContextMenuStructure();
            UIMain.Instance.HideContextMenu();
            UIMain.Instance.ShowContextMenu(ButtonsList, GetButtonsSize());
        }

        public void BackToPreviousMenu()
        {
            UIMain.Instance.HideContextMenu();
            UIMain.Instance.ShowContextMenu(previousButtonList, GetButtonsSize());
            previousButtonList = null;
        }
    }
}