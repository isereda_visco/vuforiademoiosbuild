﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Visco

{
    public class ViscoClickSensor : MonoBehaviour
    {

        [Header("Remember to add a collider to the mesh")]
        public bool ExecuteActionsAtStartup;

        [Header("Functions to call on click")]
        public UnityEvent[] OnClickActions;


        // Use this for initialization
        void Start()
        {
            if (ExecuteActionsAtStartup)
            {
                ExecuteEventList();
            }

            if (!gameObject.GetComponent<Collider>())
            {
                throw new System.Exception("GameObject '" + gameObject.name + "' has a ViscoClickSensor but no collider. Without collider it can not detect mouse clicks.");
            }

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnMouseUpAsButton()
        {
            Debug.Log("And there was much rejoice!");
            ExecuteEventList();
        }


        public void ExecuteEventList()
        {
            foreach (var eventDelegate in OnClickActions)
            {
                eventDelegate.Invoke();
            }
        }


    }
}