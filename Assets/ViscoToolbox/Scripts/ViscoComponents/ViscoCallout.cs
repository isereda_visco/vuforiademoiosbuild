﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
namespace Visco
{
    public class ViscoCallout : MonoBehaviour
    {
        [Header("Required parameters")]
        public GameObject TextCalloutManager;
        public bool Visible = false;

        [Header("Optional parameters")]

        public string Title;

        [Tooltip("Text to display in a small popup window once the Callout Point is in view")]
        [TextArea(3, 5)]
        public string CalloutText;

        [Tooltip("If the 'CalloutDeaultTextPanel' prefab contains an image it will be replaced with the image you assign here"  )]
        public Image image;

        
        [Header("Custom UI object")]
        public GameObject customDisplayObject;

        [Header("Runtime Behavior")]
        [Tooltip("0-180:   90 = Visible from side. 180 = Visible when pointing away from camera.")]
        public float MaxVisibleAngle = 90f;
        [Tooltip("Change in Alpha per frame. 1.0 = instant. 0.016 = ~1 sec at fps")]
        [Range (0.001f , 1.0f)]
        public float fadeInSpeed = 0.1f;

        [Tooltip("Change in Alpha per frame. 1.0 = instant. 0.016 = ~1 sec at fps")]
        [Range(0.001f, 1.0f)]
        public float fadeOutSpeed = 0.1f;


        [Header("Callout line")]
        public Material lineMat;

        [Tooltip("Color of the line between point of interest and text callout.")]
        public Color LineColor;

        [Header("Events (legacy system)")]
        public EventId[] ShowCalloutEvents;
        public EventId[] HideCalloutEvents;

        public float GizmosSizeMultiplier = 1.0f;


        [Tooltip("Hide the GameObject holding the TextCallout script at runtime. Usefull if TextCallout is placed on desig-time friendly cube etc. Replace with Gizmo")]
        public bool HidePointOfInterestObject = false;

        // Private vars -----------------------------------------------


        [HideInInspector]
        public bool outOfView;

        [HideInInspector]
        public Rect CalloutRect;
        private ViscoCalloutManager textCalloutManagerScript;
        private GameObject calloutCanvas;
        private GameObject calloutPanel;


        private float originalAlpha;
        private CanvasGroup canvasGroup;
        private CanvasGroup canvasGroupLine;
        private List<Vector4> lineCoords = new List<Vector4>();
        private GameObject goLineRenderer;
        private UILineRenderer UIlineRenderer;

        private Vector2 lineStart = new Vector2();
        private Vector2 lineEnd = new Vector2();
        private bool isFadingOut = false;


        // ------------------------------------------------  Code

        private void OnEnable() { StatesManager.SharedSpotAction += RespondToIncomingEvent; }

        private void OnDisable() { StatesManager.SharedSpotAction -= RespondToIncomingEvent; }

        private void RespondToIncomingEvent(EventId state)
        {
            if (ShowCalloutEvents != null && ShowCalloutEvents.Contains_EventID(state))
            {
                Visible = true;
            }

            if (HideCalloutEvents != null && HideCalloutEvents.Contains_EventID(state))
            {
                Visible = false;
            }
        }

        public void FadeInCallout() { Visible = true; }
        public void FadeOutCallout() { Visible = false; }

        public void InstantShowCallout() { canvasGroup.alpha = originalAlpha; Visible = true; }
        public void InstantHideCallout() { canvasGroup.alpha = 0; Visible = false; }

        public bool VisibleOrFadingOut
        {
            get
            {
                if (calloutPanel)
                    return calloutPanel.activeSelf;
                else
                    return false;
            }
        }

        public void SetCalloutPosition(Vector2 pos)
        {
            calloutPanel.transform.localPosition = pos;
            CalloutRect = calloutPanel.GetComponent<RectTransform>().rect;
        }

        public GameObject GetTextCallout2dObject() { return calloutPanel; }


        private void Awake()
        {
            TextCalloutManager.GetComponent<ViscoCalloutManager>().RegisterCallout(this);
        }


        void Start()
        {

            if (customDisplayObject == null)
            {
                calloutPanel = Instantiate(Resources.Load(Constants.CALLOUT_DEFAULT_TEXTPANEL)) as GameObject;
                calloutPanel.transform.SetParent(TextCalloutManager.transform);

                Text[] texts = calloutPanel.GetComponentsInChildren<Text>();

                // Set Title and Body
                if (texts.Length == 1 )
                {
                    texts[0].text = CalloutText;
                }
                else if (texts.Length == 2)
                {
                    texts[0].text = Title;
                    texts[1].text = CalloutText;
                }
                else
                {
                    throw new System.Exception("Warning: prefab CalloutDerfaultTextbox. Ask programmer about this.");
                }

                // Set optional image in the textbox
                if (image != null)
                { 
                    Image img = calloutPanel.GetComponentInChildren<Image>();
                    if (img)
                    {
                        img = image;
                    }
                }


                //calloutPanel.GetComponentInChildren<Text>().text = CalloutText;
                CalloutRect = calloutPanel.GetComponent<RectTransform>().rect;
                calloutPanel.name = "_CalloutPanel_" + name;
            }
            else
            {
                calloutPanel = customDisplayObject;
                calloutPanel.transform.SetParent(TextCalloutManager.transform);
            }

            canvasGroup = calloutPanel.GetComponent<CanvasGroup>();
            originalAlpha = canvasGroup.alpha;


            if (HidePointOfInterestObject)
            {
                gameObject.transform.localScale = new Vector3(0, 0, 0);
            }

            // Create line renderer
            goLineRenderer = new GameObject("_LineRenderer for callout " + name);

            Canvas rootCanvas = TextCalloutManager.GetComponent<Canvas>().rootCanvas;

            GameObject newCanvas = new GameObject("_Canvas for callout " + name);
            newCanvas.AddComponent<CanvasRenderer>();
            canvasGroupLine = newCanvas.AddComponent<CanvasGroup>();
            newCanvas.transform.parent = rootCanvas.transform;


            goLineRenderer.transform.SetParent(newCanvas.transform);

            UIlineRenderer = goLineRenderer.AddComponent<UILineRenderer>();
            UIlineRenderer.LineList = true;
            UIlineRenderer.color = LineColor;


            if (!Visible)
            {
                canvasGroup.alpha = 0;
            }

        }







        public bool IsFacingCamera() { return !outOfView; }

        

        public bool IsFadingOut { get { return isFadingOut; } set { isFadingOut = value; } }



        void Update()
        {

            outOfView = Vector3.Angle(Camera.main.transform.forward, transform.forward) < MaxVisibleAngle;

            if ((outOfView || !Visible) && canvasGroup.alpha > 0)
            {
                canvasGroup.alpha = Mathf.Max(canvasGroup.alpha - fadeOutSpeed, 0);
                if (canvasGroup.alpha == 0)
                {
                    calloutPanel.SetActive(false);
                    goLineRenderer.SetActive(false);
                }
            }

            if (!outOfView && Visible && canvasGroup.alpha < originalAlpha)
            {
                if (!calloutPanel.activeSelf)
                    calloutPanel.SetActive(true);
                if (!goLineRenderer.activeSelf)
                    goLineRenderer.SetActive(true);
                canvasGroup.alpha = Mathf.Min(canvasGroup.alpha + fadeInSpeed, originalAlpha);
            }

            canvasGroupLine.alpha = canvasGroup.alpha;

        }

        // First calculate "line attach" coordinates on the Callout panel and then draw them.
        // Draw a line from the Point Of Interest to the Callout panel        
        public void DrawLine(int Quadrant)
        {

            CalloutRect = RectTransformToScreenSpace(calloutPanel.GetComponent<RectTransform>());

            lineStart = Camera.main.WorldToScreenPoint(gameObject.transform.position);


            Vector2 lineRectIntersect = new Vector2(0, 0);
            bool properIntersect = LineRectIntersection(lineStart, CalloutRect.center, CalloutRect, ref lineRectIntersect);


            if (properIntersect)
                lineEnd = lineRectIntersect;
            else
                lineEnd = lineStart;

            Vector2[] ps = {lineStart, lineEnd};
            UIlineRenderer.Points = ps; // This assignment trigers update of line
        }

        public Rect RectTransformToScreenSpace(RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            return new Rect((Vector2) transform.position - (size * 0.5f), size);
        }



        // Took this one from stackoverflow.
        private static bool LineRectIntersection(Vector2 lineStartPoint, Vector2 lineEndPoint, Rect rectangle, ref Vector2 result)
        {
            Vector2 minXLinePoint = lineStartPoint.x <= lineEndPoint.x ? lineStartPoint : lineEndPoint;
            Vector2 maxXLinePoint = lineStartPoint.x <= lineEndPoint.x ? lineEndPoint : lineStartPoint;
            Vector2 minYLinePoint = lineStartPoint.y <= lineEndPoint.y ? lineStartPoint : lineEndPoint;
            Vector2 maxYLinePoint = lineStartPoint.y <= lineEndPoint.y ? lineEndPoint : lineStartPoint;

            double rectMaxX = rectangle.xMax;
            double rectMinX = rectangle.xMin;
            double rectMaxY = rectangle.yMax;
            double rectMinY = rectangle.yMin;

            if (minXLinePoint.x <= rectMaxX && rectMaxX <= maxXLinePoint.x)
            {
                double m = (maxXLinePoint.y - minXLinePoint.y) / (maxXLinePoint.x - minXLinePoint.x);

                double intersectionY = ((rectMaxX - minXLinePoint.x) * m) + minXLinePoint.y;

                if (minYLinePoint.y <= intersectionY && intersectionY <= maxYLinePoint.y
                    && rectMinY <= intersectionY && intersectionY <= rectMaxY)
                {
                    result = new Vector2((float) rectMaxX, (float) intersectionY);

                    return true;
                }
            }

            if (minXLinePoint.x <= rectMinX && rectMinX <= maxXLinePoint.x)
            {
                double m = (maxXLinePoint.y - minXLinePoint.y) / (maxXLinePoint.x - minXLinePoint.x);

                double intersectionY = ((rectMinX - minXLinePoint.x) * m) + minXLinePoint.y;

                if (minYLinePoint.y <= intersectionY && intersectionY <= maxYLinePoint.y
                    && rectMinY <= intersectionY && intersectionY <= rectMaxY)
                {
                    result = new Vector2((float) rectMinX, (float) intersectionY);

                    return true;
                }
            }

            if (minYLinePoint.y <= rectMaxY && rectMaxY <= maxYLinePoint.y)
            {
                double rm = (maxYLinePoint.x - minYLinePoint.x) / (maxYLinePoint.y - minYLinePoint.y);

                double intersectionX = ((rectMaxY - minYLinePoint.y) * rm) + minYLinePoint.x;

                if (minXLinePoint.x <= intersectionX && intersectionX <= maxXLinePoint.x
                    && rectMinX <= intersectionX && intersectionX <= rectMaxX)
                {
                    result = new Vector2((float) intersectionX, (float) rectMaxY);

                    return true;
                }
            }

            if (minYLinePoint.y <= rectMinY && rectMinY <= maxYLinePoint.y)
            {
                double rm = (maxYLinePoint.x - minYLinePoint.x) / (maxYLinePoint.y - minYLinePoint.y);

                double intersectionX = ((rectMinY - minYLinePoint.y) * rm) + minYLinePoint.x;

                if (minXLinePoint.x <= intersectionX && intersectionX <= maxXLinePoint.x
                    && rectMinX <= intersectionX && intersectionX <= rectMaxX)
                {
                    result = new Vector2((float) intersectionX, (float) rectMinY);

                    return true;
                }
            }

            return false;
        }



#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            float size = GizmosSizeMultiplier;
            if (TextCalloutManager)
            {
                textCalloutManagerScript = TextCalloutManager.GetComponent<ViscoCalloutManager>();
                size = textCalloutManagerScript.GizmoScale * GizmosSizeMultiplier;
            }
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position, size);
            Gizmos.color = new Color(1f, 1f, 0.5f);
            Vector3 scaledForward = Vector3.forward * size * 3;
            Gizmos.DrawRay(transform.position, transform.rotation * scaledForward);
        }

        private void OnDrawGizmos()
        {
            float size = GizmosSizeMultiplier;
            if (TextCalloutManager)
            {
                textCalloutManagerScript = TextCalloutManager.GetComponent<ViscoCalloutManager>();
                size = textCalloutManagerScript.GizmoScale * GizmosSizeMultiplier;
            }

            if (textCalloutManagerScript)
            {
                if (textCalloutManagerScript.ShowAllGizmos)
                {
                    Gizmos.color = LineColor;
                    Gizmos.DrawSphere(transform.position, size);

                    Vector3 scaledForward = Vector3.forward * size * 3;
                    Gizmos.DrawRay(transform.position, transform.rotation * scaledForward);
                }
            }
        }

#endif


    }

}
