﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Visco
{
//#if UNITY_EDITOR
//    using UnityEditor;
 //   public class HideObjectManager : MonoBehaviourEditorUI
//#else 

public class HideObjectManager : MonoBehaviour
//#endif
    {
        private List<GameObject> childList = new List<GameObject>();
        private MeshRenderer _myMeshRenderer;
        private Collider _myCollider;
        private Image _myImage;
        private RawImage _mRawImage;

        public EventId[] HideEvents;
        public EventId[] ShowEvents;

        private void Awake()
        {
            foreach (Transform tr in transform)
            {
                childList.Add(tr.gameObject);
            }
            _myMeshRenderer = GetComponent<MeshRenderer>();
            _myCollider = GetComponent<Collider>();
            _myImage = GetComponent<Image>();
            _mRawImage = GetComponent<RawImage>();
        }

        private void OnEnable() { StatesManager.SharedSpotAction += HotSpotClickedAction; }

        private void OnDisable() { StatesManager.SharedSpotAction -= HotSpotClickedAction; }

        private void HotSpotClickedAction(EventId @event)
        {
            if (HideEvents != null && HideEvents.Contains_EventID(@event)) HideShow(false);
            if (ShowEvents != null && ShowEvents.Contains_EventID(@event)) HideShow(true);
        }

        void HideShow(bool show)
        {
            if (_myCollider != null) _myCollider.enabled = show;
            if (_myMeshRenderer != null) _myMeshRenderer.enabled = show;
            if (_myImage != null) _myImage.enabled = show;
            if (_mRawImage != null) _mRawImage.enabled = show;
            foreach (GameObject go in childList) go.SetActive(show);
        }

        public void Hide()
        {
            HideShow(false);
        }

        public void Hide1(int n)
        {
            HideShow(false);
        }

        public void Show() { HideShow(true); }

#if UNITY_EDITOR

        void HideInEditor()
        {
            _myMeshRenderer = GetComponent<MeshRenderer>();
            _myCollider = GetComponent<Collider>();
            _myImage = GetComponent<Image>();
            _mRawImage = GetComponent<RawImage>();
            ;

            if (_myCollider != null) _myCollider.enabled = false;
            if (_myMeshRenderer != null) _myMeshRenderer.enabled = false;
            if (_myImage != null) _myImage.enabled = false;
            if (_mRawImage != null) _mRawImage.enabled = false;

            foreach (Transform tr in transform) tr.gameObject.SetActive(false);
        }

        //public override bool OnInspectorGUI(Editor editor)
        //{
        //    editor.DrawDefaultInspector();
        //    if (GUILayout.Button("Hide object"))
        //    {
        //        HideInEditor();
        //    }
        //    editor.serializedObject.ApplyModifiedProperties();
        //    return false;
        //}
#endif


    }
}