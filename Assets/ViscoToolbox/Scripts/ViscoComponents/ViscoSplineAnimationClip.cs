﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Visco
{

    public class ViscoSplineAnimationClip : MonoBehaviour, IViscoAnimatable
    {

        public new MegaShapeCircle animation;


        [Header("Use these to auto-calculate start/stop time")]
        public bool useFrameNumbers;
        public int clipStartFrame;
        public int clipEndFrame;
        public int globalAnimationFrameCount;
        public float globalAnimationLengthSec;


        [Header("Or manually assign...")]
        public float startTime;
        public float stopTime;

        [Header("Playback speeds")]
        public float defaultForwardSpeed = 1;
        public float defaultBackwardSpeed = -1;
        public float fastForwardSpeed = 5;
        public float fastRewindSpeed = -5;
        public float customSpeed = 1;
        public WrapMode wrapMode;

        [Header("Playback conditions")]
        public bool WaitForCameraToArrive;
        public float DelayBeforePlaying;



        public bool WaitForCamera
        {
            get { return WaitForCameraToArrive; }
            set { WaitForCameraToArrive = value; }
        }

        public float DelayBeforePlay
        {
            get { return DelayBeforePlaying; }
            set { DelayBeforePlaying = value; }
        }

        public bool IsPlaying()
        {
            //Debug.Log("IsPlayingTest: anim.time = " + animation.time + " and speed=" + animation.speed + " and start/stop = " + startTime.ToString() + "," + stopTime.ToString());
            if (animation.speed != 0 && animation.time > startTime && animation.time < stopTime)
                return true;
            else
                return false;

        }

        public void ExecuteAllForwardFrameEvents()
        {
            // N/A for splines.
        }
        public void ExecuteAllBackwardsFrameEvents()
        {
            // N/A for splines.
        }


        public void PlayAnimationClip()
        {

            Debug.Log("Playing spline clip " + animation.name + " \n");


            if (!IsPlaying())
                animation.time = startTime;

            animation.speed = defaultForwardSpeed;
            animation.animate = true;
        }

        /// <summary>
        /// SetCustomAnimationSpeed first needed, if not, normal speed will be used
        /// </summary>
        /// <param name="animation"></param>
        public void PlayAnimationClipWithCustomSpeed()
        {
            animation.time = 0;
            animation.speed = customSpeed;
            animation.animate = true;
            ResetCustomSpeed();
        }

        public void PlayAnimationClipBackwards()
        {

            Debug.Log("Playing clip " + animation.name + " backwards \n");

            if (!IsPlaying())
            { 
                animation.time = stopTime;
                //Debug.Log("Reverse spline animation WITH reset position to end\n");
            }
            else
            {
               // Debug.Log("Reverse spline animation WITHOUT reset position!\n");
            }

            animation.speed = defaultBackwardSpeed;
            animation.animate = true;
        }

        void Start()
        {
            if (useFrameNumbers == true)
            {
                startTime = (float)clipStartFrame / (float)globalAnimationFrameCount * globalAnimationLengthSec;
                stopTime = (float)clipEndFrame / (float)globalAnimationFrameCount * globalAnimationLengthSec;
                Debug.Log( gameObject.name +  " - Spline calculated  start/stop = " + startTime.ToString() + "," + stopTime.ToString());
            }
        }

        void Update()
        {
            // Freeze animation when i gets outside clip limits.
            if (animation.time > stopTime && animation.time < startTime)
                animation.speed = 0;
        }

        public void PlayAnimationClipBackwardsWithCustomSpeed()
        {
            Debug.Log("Fast rewind " + animation.name + " backward  setting start pos=" + animation.time.ToString()  + "   \n");
            animation.speed = Mathf.Min( customSpeed, -customSpeed);
            animation.time = stopTime; 
            animation.animate = true;


        }

        public void SetAnimationClipToBegin()
        {
            animation.speed = 0.0f;
            animation.time = startTime;
            animation.animate = true;
        }

        public void SetAnimationClipToEnd()
        {

            animation.time = stopTime;
            animation.speed = 0.0f;
            animation.animate = true;
        }


        public float SetTime
        {
            set  { animation.time = value;
                    Debug.Log("Visco Warning: Setting time on Spline animation. Undetermined if time is normalized or not. Potential bug");
            }
            get { return animation.time; }
        }


        public void SetCustomAnimationSpeed(float speed) { customSpeed = speed; }

        void ResetCustomSpeed() { customSpeed = 1; }

        public void Stop()
        {
            if (animation)
                animation.speed = 0;
        }

   
        public GameObject GameObject
        {
            get { return gameObject; }
        }

        public Animation Animation
        {
            get { return null; }
            set { throw new System.Exception("ERROR: You are trying to assign Animation to a SplineAnimation"); }
        }

        public AnimationClip AnimationClip
        {
            get { return null; }
            set { throw new System.Exception("ERROR: You are trying to assign AnimationClip to a SplineAnimation"); }
        }





        public WrapMode WrapMode
        {
            get
            {
                return wrapMode;
            }

            set
            {
                wrapMode = value;
            }
        }


    }

}