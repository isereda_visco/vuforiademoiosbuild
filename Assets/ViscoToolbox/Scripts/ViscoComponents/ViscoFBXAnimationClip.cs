﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


namespace Visco
{

    public interface IViscoAnimatable
    {
        GameObject GameObject { get; }
        Animation Animation { get; set; }
        AnimationClip AnimationClip { get; set; }

        WrapMode WrapMode { get; set; }

        bool IsPlaying();

        bool WaitForCamera { get; set; }
        float DelayBeforePlay { get; set; }
        void PlayAnimationClip();
        void PlayAnimationClipWithCustomSpeed();
        void PlayAnimationClipBackwards();
        void PlayAnimationClipBackwardsWithCustomSpeed();
        void SetAnimationClipToBegin();
        void SetAnimationClipToEnd();
        void SetCustomAnimationSpeed(float speed);
        float SetTime { get; set; }
        void Stop();

        void ExecuteAllForwardFrameEvents();
        void ExecuteAllBackwardsFrameEvents();

    }


    [Serializable]
    public class AnimationEventDelegateWrapper
    {
        public int frame;
        public EventDelegate ForwardAction;
        public EventDelegate BackwardsAction;
    }



    public class ViscoFBXAnimationClip : MonoBehaviour, IViscoAnimatable
    {
        [Header("Animation and clip")]
        public new Animation animation;
        public AnimationClip clip;

        [Header("The clip's global 3DS Max frame numbers")]
        public bool useFrameNumbers;
        public int StartFrame;
        public int EndFrame;

        [Header("Playback properties")]
        public float defaultForwardSpeed = 1;
        public float defaultBackwardSpeed = -1;
        public float fastForwardSpeed = 5;
        public float fastRewindSpeed = -5;
        public WrapMode wrapMode;

        [Header("Playback conditions")]
        public bool WaitForCameraToArrive;
        public float DelayBeforePlaying;


        public float customSpeed;

        [Header("Start/End frame above MUST be filled in for these to work.")]
        public AnimationEventDelegateWrapper[] frameActions;
        private Delegate callbackFunction;  // This is the callback whenever we have finished playing a clip
        private AnimationState animState;

        void Start()
        {
            
            if (animation == null)
            {
                throw new Exception("GameObject '" + gameObject.name + "' has Animation = null. You must correct this error for proper function");
            }
            if (clip == null && useFrameNumbers == false)
            {
                throw new Exception("GameObject '" + gameObject.name + "' must have either a clip assigned or manually specified start/end frames.");
            }

            if (frameActions.Length > 20)
            {
                throw new Exception("WARNING: More than 20 animationn-frame events on a single clip. This bubble sort needs replacement!");
            }

            // Bubble sort frame-actions. It is unlikely more than 10 items, ever
            for (int a = 0; a < frameActions.Length - 2; a++)
            {
                for (int b = a + 1; b < frameActions.Length - 1; b++)
                {
                    if (frameActions[a].frame > frameActions[b].frame)
                    {
                        AnimationEventDelegateWrapper tmp = frameActions[a];
                        frameActions[a] = frameActions[b];
                        frameActions[b] = tmp;
                    }
                }
            }


            if (useFrameNumbers)
            {
                Debug.Log("Creating clip from frame number range");
                string clipName = "_" + name + "autoGen";

                if (animation[clipName] != null)
                {
                    throw new Exception("Animation GameObjects must have unique names. Currently there are 2 or more animation GO's with the name " + name);
                }
                AnimationClip newClip = new AnimationClip();
                newClip.legacy = true;
                animation.AddClip(newClip, clipName, StartFrame, EndFrame);
                clip = animation[clipName].clip;
            }

            animState = animation[clip.name];

            
            // Set up frame-specific event callbacks.
            if (frameActions.Length > 0 )
            {
                // Add a helper script on Animation object. Helper script recieves animation events and sends them foward to this script.
                 if (animation.gameObject.AddComponent<ViscoAnimationHelper>() == null)
                 {
                    animation.gameObject.AddComponent<ViscoAnimationHelper>();
                 }

                foreach (AnimationEventDelegateWrapper thing in frameActions)
                {
                    if (thing.frame < 0 || thing.frame > EndFrame)
                    {
                        throw new Exception("ERROR: GameObject '" + gameObject.name + "' has a frame-event outside the range frame range of it's animation clip");
                    }

                    float clipFrameCount = EndFrame - StartFrame;
                    float eventFraction = (thing.frame - StartFrame) / clipFrameCount;
                    float lengthAdjustedEventFraction = eventFraction * clip.length;

                    AnimationEvent animationEvent = new AnimationEvent
                    {
                        functionName = "OnForwardPlayEvent",
                        stringParameter = gameObject.name,
                        intParameter = Array.IndexOf(frameActions, thing),
                        objectReferenceParameter = gameObject,
                        time = lengthAdjustedEventFraction
                    };

                    //string dbg = "-------- Frame event calculation ----\n";
                    //dbg += "AnimationGameObject: " + gameObject.name + ": CreateFrameEvent at frame=" + thing.frame + ": Calculated event time within clip: " + animationEvent.time + "sec.\n";
                    //dbg += "clip.length: " + clip.length + "\n";
                    //Debug.Log(dbg);

                    clip.AddEvent(animationEvent);
                }
            }
        }

        // Execute incoming frame-events (routed from animationClip -> animation -> here)
        public void AnimationEventReceiverFinalDestination(AnimationEvent animationEvent)
        {
            AnimationEventDelegateWrapper frameAction = frameActions[animationEvent.intParameter];

            if (animState.speed > 0)
            {

                if (frameAction.ForwardAction.isValid)
                {
                    float ReverseCalculatedFrameNo = (float)StartFrame + (animState.time / clip.length) * ((float)(EndFrame - StartFrame)); 
                    Debug.Log("Animation [" + clip.name + "] at GameObject [" + gameObject.name     + "]: Execute forward frameAction " + frameAction.ForwardAction.methodName + " at clip time=" + animState.time + " (frame = ~"   + ReverseCalculatedFrameNo + ")\n");
                    frameAction.ForwardAction.Execute();

                }
            }
            else if (animState.speed < 0)
            {
            
                if (frameActions[animationEvent.intParameter].BackwardsAction.isValid)
                {
                    float ReverseCalculatedFrameNo = (float)StartFrame + (animState.time / clip.length) * ((float)(EndFrame - StartFrame));
                    Debug.Log("Animation [" + clip.name + "] at GameObject [" + gameObject.name + "]: Execute backwards frameAction " + frameAction.BackwardsAction.methodName + " at clip time=" + animState.time + " (frame = ~" + ReverseCalculatedFrameNo + ")\n");

                    frameActions[animationEvent.intParameter].BackwardsAction.Execute();
                }
                    
            }
        }

        public void ExecuteAllForwardFrameEvents()
        {
            foreach (AnimationEventDelegateWrapper a in frameActions)
            {
                a.ForwardAction.Execute();
            }
        }

        public void ExecuteAllBackwardsFrameEvents()
        {
            for (int i = frameActions.Length- 1; i >= 0;  i--)
            {
                frameActions[i].BackwardsAction.Execute();
            }
        }




        public bool IsPlaying()
        {
            if (!animation.IsPlaying(clip.name))
            {
                return false;
            }

            if (animState.speed != 0)
                return true;
            else
                return false;
        }


        public bool WaitForCamera
        {
            get { return WaitForCameraToArrive; }
            set { WaitForCameraToArrive = value; }
        }

        public float DelayBeforePlay
        {
            get { return DelayBeforePlaying; }
            set { DelayBeforePlaying = value; }
        }




        public void PlayAnimationClip()
        {
            if (!animation.IsPlaying(clip.name))
            {
                animState.time = 0;
            }

            animState.speed = defaultForwardSpeed;
            animation.Play(clip.name);
            clip.wrapMode = wrapMode;
            animation.wrapMode = wrapMode;

            Debug.Log("Playing clip " + clip.name + "   (Speed: " + animState.speed + ")   WrapMode: " + wrapMode.ToString() + " \n");
        }


        public void PlayAnimationClipWithCustomSpeed()
        {
            animState.time = 0;
            animState.speed = customSpeed;
            animation.Play(animation.name);
            ResetCustomSpeed();
        }

        public void PlayAnimationClipBackwards()
        {

            Debug.Log("Playing clip " + clip.name + " backwards  (Speed: " + animState.speed + ")   WrapMode: " + wrapMode.ToString() + " \n");

            if (defaultBackwardSpeed > 0)
                defaultBackwardSpeed = -defaultBackwardSpeed;


            if (!animation.IsPlaying(clip.name))
            {
                animState.normalizedTime = 1f;
            }
            animState.speed = defaultBackwardSpeed;
            animation.Play(clip.name);

        }







        string preString;

        // Freeze animation clips when they reach their boundary. 
        //"Wrap-around" time for clips that play in WrapMode.loop
        void Update()
        {
            if (animation.IsPlaying(clip.name))
            {
                //string msg = "Playing " + clip.name + " @ " + Anim[clip.name].time + "   normalizedTime = " + Anim[clip.name].normalizedTime + "\n";
                //if (msg != preString)
                //{

                //    Debug.Log(msg);
                //    preString = msg;
                //}

                if (wrapMode == WrapMode.ClampForever)  // Freeze Animation.time when we reach the end of clip *
                {
                    if (animState.normalizedTime >= 1f && animState.speed > 0f)
                    {
                        animState.speed = 0;
                    }
                    else if (animState.normalizedTime <= 0f && animState.speed < 0f)
                    {
                        animState.speed = 0;
                    }
                } else if (wrapMode == WrapMode.Loop)   // Wrap around time back to start of clip when we reach end, for loop-clips only
                {
                    // Forward
                    if (animState.speed > 0 )
                    {
                        if (animState.normalizedTime >= 1.0f)
                            animState.normalizedTime = animState.normalizedTime - 1.0f;
                    }

                    // Backwards
                    if (animState.speed < 0)
                    {
                        if (animState.normalizedTime <= 1.0f)
                            animState.normalizedTime = animState.normalizedTime + 1.0f;
                    }



                }



            }
        }

        public void PlayAnimationClipBackwardsWithCustomSpeed()
        {
            Debug.Log("Fast rewind " + clip.name + " backward  setting start pos=" + clip.length + "   \n");
            //animation.Stop();
            animState.speed = fastRewindSpeed;
            animState.time = 1f; // clip.length;
            animation.Play(clip.name);
                       

        }

        public void SetAnimationClipToBegin()
        {
            Debug.Log("Rewind animation clip " + clip.name + " to beginning\n");

            if (animation[clip.name] == null)
            {
                Debug.Log("smth is weird! " + animation.GetClipCount() + " = clipcount");
                return;
            }
            animState.speed = 0.0f;
            animState.time = 0;
            animation.Play(clip.name);
        }

        public void SetAnimationClipToEnd()
        {

            animState.normalizedTime = 1f;
            animState.speed = 0.0f;
            animation.Play(clip.name);
        }

        public void SetCustomAnimationSpeed(float speed) { customSpeed = speed; }

        void ResetCustomSpeed() { customSpeed = 1; }

        public void Stop()
        {
            //if (Animation) Animation.Stop();
        }


        public float SetTime
        {
            get { return animState.time; }
            set { animState.time = value; }
        }

        public GameObject GameObject
        {
            get { return gameObject; }
        }

        public Animation Animation
        {
            get  {    return animation;  }
            set  {    animation = value; }
        }

        public AnimationClip AnimationClip
        {
            get     { return clip;       }
            set     { clip = value; }
        }

        public WrapMode WrapMode
        {
            get  {   return wrapMode;    }
            set  {   wrapMode = value;   }
        }


    }





}