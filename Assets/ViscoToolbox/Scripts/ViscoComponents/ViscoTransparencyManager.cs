﻿using System.Collections.Generic;
using UnityEngine;
namespace Visco
{
    [System.Serializable]
public class TransparencyTarget
{
    // This object encapsulates data and code for 'lerping' alpha values from say 1 to 0.
    // The object is shared between a TransparencyScript and one ore many TransparencyHelpers (they are assigned to meshes)
    public float startAlpha;
    public float arrivalAlpha;   // Target alpha value
    
    public float arrivalTime;   // 
    public float startTime;

    public float GetCurrentAlpha()
    {
        float totalTime = arrivalTime - startTime;
        float currentProgress = Mathf.Min((Time.fixedTime - startTime) / totalTime, 1.0f);
        currentProgress = Mathf.Max(0, currentProgress);

        float result = startAlpha + currentProgress * (arrivalAlpha - startAlpha);

        if (Time.fixedTime > arrivalTime)
        {
            startAlpha = arrivalAlpha;
            result = arrivalAlpha;
        }
        return result;
    }

    public void SetTargetAlpha(float alpha, float duration)
    {
        startAlpha = GetCurrentAlpha();
        arrivalAlpha = alpha;
        startTime = Time.fixedTime;
        arrivalTime = startTime + duration;
    }
}



    public class ViscoTransparencyManager : MonoBehaviour
    {

        [Tooltip("Your comments, so other designers can understand what this Transparency manager is supposed to do")] [TextArea(3, 5)]
        public string Comments;

        
        [Tooltip("If asked to toggle transparency, this is the alpha that will be faded too.")] [Range(0f, 1f)]
        public float ToggleTargetAlpha = 0.5f;


        private bool isToggled = false;

        [Tooltip("Seconds it shall take to fade to a new alpha value")] [Range(0f, 60f)]
        public float duration = 1.5f;

        [Tooltip("Gameobjects (+ recursive children) to apply transparency to")]
        public List<GameObject> FadeParts;


        public EventId[] SetTransparencyStates;
        public EventId[] SetOpaqueStates;
        public EventId[] ToggleTransparencyStates;

        private void OnEnable() { StatesManager.SharedSpotAction += RespondToIncomingEvent; }

        private void OnDisable() { StatesManager.SharedSpotAction -= RespondToIncomingEvent; }

        private void RespondToIncomingEvent(EventId state)
        {

            if (SetOpaqueStates != null && SetOpaqueStates.Contains_EventID(state))
            {
                FadeToOpaqueIfTransparrent();
            }

            if (SetTransparencyStates != null && SetTransparencyStates.Contains_EventID(state))
            {
                FadeToTransparrentIfOpaque();
            }

            if (SetTransparencyStates != null && ToggleTransparencyStates.Contains_EventID(state))
            {
                ToggleTransparency();
            }

        }





        private List<GameObject> nodeList = new List<GameObject>();

        // Each mesh that we will change alpha on will have a pointer to this AlphaEgg.
        // Any change to it's values will make the meshes update the look of their materials.
        private TransparencyTarget AlphaEgg;


        // Use this for initialization
        void Start()
        {
            AlphaEgg = new TransparencyTarget
            {
                startAlpha = 1.0f,
                arrivalAlpha = 1.0f,
                startTime = Time.fixedTime,
                arrivalTime = Time.fixedTime
            };

            // Make a list of ALL childnodes that have a MeshRenderer on them
            foreach (GameObject parentNode in FadeParts)
            {
                nodeList.AddRange(GetAllMeshRenderChildrenRecursive(parentNode));

                if (parentNode.GetComponent<MeshRenderer>())
                {
                    nodeList.Add(parentNode);
                }

            }


            // Decorate each childnode with a transparency helper if needed, 
            // and assign ask it to subscribe to our AlphaEgg
            foreach (GameObject g in nodeList)
            {
                if (!g.GetComponent<TransparencyHelper>())
                {
                    g.AddComponent<TransparencyHelper>();
                }
                TransparencyHelper th = g.GetComponent<TransparencyHelper>();
                th.RegisterTransparencyManipulator(AlphaEgg);
            }
        }


        // Recursivly get all children that has as MeshRenderer.
        private List<GameObject> GetAllMeshRenderChildrenRecursive(GameObject parentNode)
        {
            List<GameObject> result = new List<GameObject>();

            if (parentNode == null)
            {
                Debug.Log("WARNING: GameObject '" + gameObject.name + "' has a TransparencyManager script attached that has null set as FadePart\n");
                return result;
            }

            foreach (Transform childTransform in parentNode.transform)
            {
                GameObject go = childTransform.gameObject;

                if (go.GetComponent<MeshRenderer>())
                    result.Add(go);

                result.AddRange(GetAllMeshRenderChildrenRecursive(go));
            }
            return result;
        }




        // Update is called once per frame
        void Update() { }



        /// <summary>
        /// Fade the selected objects to opaque IF they are transparrent,
        /// otherwise leave as is
        /// </summary>
        public void FadeToOpaqueIfTransparrent()
        {
            Debug.Log("GameObject " + gameObject.name + " using TransparencyManager.FadeToOpaqueIfTransparrent()\n");
            AlphaEgg.SetTargetAlpha(1, duration);
        }


        /// <summary>
        /// Fade the selected objects to transparrent only IF they are not transparent
        /// </summary>
        public void FadeToTransparrentIfOpaque()
        {
            Debug.Log("GameObject " + gameObject.name + " using TransparencyManager.FadeToTransparrentIfOpaque()\n");
            AlphaEgg.SetTargetAlpha(ToggleTargetAlpha, duration);
        }

        /// <summary>
        /// Fade to selected alpha regardless of current value
        /// </summary>
        public void SetTransparency(float alpha)
        {
            AlphaEgg.SetTargetAlpha(alpha, duration);
        }


        /// <summary>
        /// Toggles current transparency state. If opaque -> make transparent, and the other way around. Uses property ToggleTargetAlpha
        /// </summary>

        public void ToggleTransparency()
        {
            if (!isToggled)
                AlphaEgg.SetTargetAlpha(ToggleTargetAlpha, duration);
            else
                AlphaEgg.SetTargetAlpha(1, duration);

            isToggled = !isToggled;
        }

        /// <summary>
        /// Instantly fade to original Alpha
        /// </summary>
        public void InstantShow()
        {
            AlphaEgg.SetTargetAlpha(1, 0);
        }


        /// <summary>
        /// Instantly fade to invisible
        /// </summary>
        public void InstantHide()
        {
            AlphaEgg.SetTargetAlpha(0, 0);
        }





    }
}
