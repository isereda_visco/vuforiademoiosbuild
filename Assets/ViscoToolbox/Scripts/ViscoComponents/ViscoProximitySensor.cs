﻿using UnityEngine;

namespace Visco
{

    public class ViscoProximitySensor : MonoBehaviour
    {

        [Header("Standard parameters")] public float radius = 1;

        public bool triggerOnStart = true;
        public bool runTriggersEveryFrame = false;

        [Header("Object to track - Camera if not assigned")] public GameObject proximityObject;

        public float currentDistance;
        public UnityEventWrapper EnterWrappedEvents;
        //public UnityEvent EnterEvents;
        public UnityEventWrapper ExitWrappedEvents;
        // public UnityEvent ExitEvents;

        [Header("Tell dev if you need these. N/A")] public float EnterEventDelay;
        public float ExitEventDelay;


        private Transform trackedObject;
        private bool isInRange;

        public GameObject ProximityObject
        {
            get { return proximityObject; }
            set
            {
                proximityObject = value;
                if (value != null)
                    trackedObject = value.transform;
                else
                {
                    trackedObject = Camera.main.transform;
                }
            }
        }

        public bool TargetInRange() { return Vector3.Distance(transform.position, trackedObject.position) < radius; }




        // Use this for initialization
        void Start()
        {
            if (!trackedObject)
                ProximityObject = null;
        }

        // Update is called once per frame
        void Update()
        {
            if (!trackedObject) return;

            currentDistance = Vector3.Distance(transform.position, trackedObject.position);
            if (currentDistance < radius)
            { // In range
                if (runTriggersEveryFrame)
                {
                    // EnterEvents.Invoke();
                    EnterWrappedEvents.InvokeEvents();
                }
                else
                {
                    if (!isInRange)
                    {
                        isInRange = true;
                        //  EnterEvents.Invoke();
                        EnterWrappedEvents.InvokeEvents();
                        Debug.Log("EnterTriggers activated\n");
                    }
                }
            }
            else // Out of range
            {
                if (runTriggersEveryFrame)
                {
                    // ExitEvents.Invoke();
                    ExitWrappedEvents.InvokeEvents();
                }
                else
                {
                    if (isInRange)
                    {
                        isInRange = false;
                        //   ExitEvents.Invoke();
                        ExitWrappedEvents.InvokeEvents();
                        Debug.Log("ExitTriggers activated\n");
                    }
                }
            }
        }



#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color32(255, 238, 20, 40);
            Gizmos.DrawSphere(transform.position, radius);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color32(128, 128, 128, 40);
            Gizmos.DrawSphere(transform.position, radius / 30f);
            Gizmos.color = new Color(1f, 1f, 0.5f);
        }

#endif



    }
}

