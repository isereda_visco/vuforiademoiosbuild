﻿namespace Visco
{


    using System.Collections.Generic;
    using UnityEngine;
#if UNITY_EDITOR
    using UnityEditor;

    /// <summary>
    /// uses default textbox to show description or lest to define list of custom text boxes
    /// </summary>
    public class ViscoDescriptionManager : MonoBehaviourEditorUI
#else


public class ViscoDescriptionManager : MonoBehaviour
#endif
    {
        [Tooltip("If custom description panel not defined, default panel will be used")] [SerializeField] private List<GameObject> CustomTextBoxList;

        public List<GameObject> GetCustomTextBoxsList() { return CustomTextBoxList; }

        private List<GameObject> previousTextBoxList;

        public void BackToPreviousDescriptions()
        {
            UIMain.Instance.ShowTextBoxes(previousTextBoxList);
            previousTextBoxList = null;
        }

        public void ShowThisDescription()
        {
            previousTextBoxList = UIMain.Instance.GetTexBoxesList();
            UIMain.Instance.ShowTextBoxes(CustomTextBoxList);
        }

#if UNITY_EDITOR


        public override bool OnInspectorGUI(Editor editor)
        {
            editor.DrawDefaultInspector();

            if (GUILayout.Button("Add custom description text box"))
            {
                GameObject go = Resources.Load(Constants.TEXT_BOX_PREFAB_PATH) as GameObject;
                GameObject g = PrefabUtility.InstantiatePrefab(go) as GameObject;
                if (CustomTextBoxList == null) CustomTextBoxList = new List<GameObject>();
                g.name = gameObject.name + "_CustomTextBox" + CustomTextBoxList.Count;
                Canvas Canvas = FindObjectOfType<Canvas>();
                if (Canvas != null && Canvas.GetComponent<UIMain>() != null) g.transform.SetParent(Canvas.transform);
                else Debug.Log("Cant find Canvas or UIMain");

                SerializedProperty сustomTextBoxList = editor.serializedObject.FindProperty("CustomTextBoxList");
                сustomTextBoxList.InsertArrayElementAtIndex(сustomTextBoxList.arraySize);
                сustomTextBoxList.GetArrayElementAtIndex(сustomTextBoxList.arraySize - 1).objectReferenceValue = g;
                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(g.scene);
            }
            editor.serializedObject.ApplyModifiedProperties();
            return false;
        }
#endif
    }
}