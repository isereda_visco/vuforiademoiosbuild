﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class ViscoWorldToScreenTool : MonoBehaviour
    {

        public GameObject ScreenspaceObject;
        public Vector3 screenSpacePos;
        public bool autoUpdatePosition = true;

        public Vector2 fixedOffset;

        // Use this for initialization
        void Start()
        {

        }

        private void Update()
        {
            // Check that the position is in front of the camera, not behind.
            Vector3 p = Camera.main.WorldToViewportPoint(transform.position);

            if (0 <= p.x && p.x <= 1.0 && 0 <= p.y && p.y <= 1.0f && p.z > 0)
            {
                screenSpacePos = Camera.main.WorldToScreenPoint(transform.position);
            }
            else
            {
                // outside cameras field of view
                screenSpacePos.x = 9999;
                screenSpacePos.y = 9999;
                screenSpacePos.z = 9999;

            }


            if (autoUpdatePosition)
            {
                ScreenspaceObject.transform.position = screenSpacePos + new Vector3(fixedOffset.x, fixedOffset.y, 0);
            }


        }
    }
}