﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Visco
{
    public class ViscoSideMenuManager : MonoBehaviour
    {
        public List<ImageStruct> Images;

        public List<VideoStruct> Videos;

        public List<ImageStruct> GetImagesList()
        {
            for (byte i = 0; i < Images.Count; i++)
                if (Images[i].MySprite == null) throw new Exception(string.Format("{0}, have empty 'My Sprite' field in ViscoSideMenuManager => Images => Element {1}", name, i));

            return Images != null && Images.Count != 0 ? Images : null;
        }

        public List<VideoStruct> GetVideosList()
        {
            for (byte i = 0; i < Videos.Count; i++)
                if (Videos[i].VideoClip == null) throw new Exception(string.Format("{0}, have empty 'Video Clip' field in ViscoSideMenuManager => Videos => Element {1}", name, i));

            return Videos != null && Videos.Count != 0 ? Videos : null;
        }

        public void Show() { UIMain.Instance.ShowSideMenu(GetVideosList(), GetImagesList()); }
        public void Hide() { UIMain.Instance.ShowStateSideMenu(); }
    }
}