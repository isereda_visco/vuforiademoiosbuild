﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Visco
{
    /// <summary>
    /// It is used by animation clips to pass on animation-frame bound events to the gameObjects that manages animation playback - the child objects of states
    /// It is added automatically at program start if not present.
    /// </summary>
    public class ViscoAnimationHelper : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Incoming event here, passed on to ViscoFBXAnimationClip
        /// </summary>
        /// <param name="animationEvent"></param>
        public void OnForwardPlayEvent(UnityEngine.AnimationEvent animationEvent)
        {
            GameObject tmp = (GameObject)animationEvent.objectReferenceParameter;
            ViscoFBXAnimationClip test = ((GameObject)(animationEvent.objectReferenceParameter)).GetComponent<ViscoFBXAnimationClip>();
            test.AnimationEventReceiverFinalDestination(animationEvent);

            //Debug.Log(Time.time + " - eureka OnForwardPlayEvent! [" + animationEvent.stringParameter + "] \n");
        }



    }
}