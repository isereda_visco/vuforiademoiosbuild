﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visco
{

    public class ViscoShaderReplacer : MonoBehaviour
    {
        [Header("Root target mesh")]
        public GameObject meshRootObject;

        [Header("Replacement shader")]
        public string shaderName = "Transparent/Diffuse";

        public bool runOnProgramStart = false;

        private Shader shader;
        private List<GameObject> nodeList;

        public void Start()
        {
            shader = Shader.Find(shaderName);

            nodeList.AddRange(GetAllChildnodesRecursive(meshRootObject));

            if (runOnProgramStart)
                ChangeShader();

        }


        public void ChangeShader()
        {
            foreach (GameObject go in nodeList)
            {
                foreach (Renderer r in go.GetComponents<Renderer>())
                {
                    for (int i = 0; i < r.materials.Length; i++)
                    {
                        r.materials[i].shader = shader;
                    }

                }
            }
        }



        private List<GameObject> GetAllChildnodesRecursive(GameObject parentNode)
        {
            List<GameObject> result = new List<GameObject>();
            foreach (Transform childTransform in parentNode.transform)
            {
                GameObject go = childTransform.gameObject;
                if (go.GetComponent<MeshRenderer>())
                {
                    result.Add(go);
                }
                result.AddRange(GetAllChildnodesRecursive(go));
            }

            return result;
        }
    }
}