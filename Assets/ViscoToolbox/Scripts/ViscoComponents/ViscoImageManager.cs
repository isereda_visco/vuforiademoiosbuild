﻿using System;
using UnityEngine;

namespace Visco
{

    [Serializable]
    public class ImageStruct
    {
        public Sprite MySprite;
        public bool ShowName;
        public string ImageName;

        public ImageStruct(ImageStruct vs)
        {
            MySprite = vs.MySprite;
            ShowName = vs.ShowName;
            ImageName = vs.ImageName;
        }
    }

    public class ViscoImageManager : MonoBehaviour
    {
        [Header("The image will be shown in default image button")] [SerializeField] private ImageStruct MyImage;

        /// <summary>
        /// retuns null if MySprite field is empty
        /// </summary>
        /// <returns></returns>
        public ImageStruct GetImage()
        {
            return MyImage.MySprite != null ? MyImage : null;
        }
        public void Show() { UIMain.Instance.ShowImage(MyImage); }

        public void Hide() { UIMain.Instance.ShowStateImage(); }
    }
}