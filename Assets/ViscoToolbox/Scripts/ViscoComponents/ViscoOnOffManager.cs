﻿using System.Collections.Generic;
using UnityEngine;
namespace Visco
{
    [System.Serializable]
    
    public class ViscoOnOffManager : MonoBehaviour
    {

        [Tooltip("Your comments, so other designers can understand what this OnOff manager is supposed to do")] [TextArea(3, 5)]
        public string Comments;
        


        [Tooltip("Gameobjects to activate/deactivate")]
        public List<GameObject> targetObjects;

        public string debugLastCommand;

        // Use this for initialization
        void Start()
        {

        }

        /// <summary>
        /// Toggles each object's active status individually.
        /// </summary>
        public void ToggleAll()
        {
            foreach (GameObject go in targetObjects)
            {
                go.SetActive(!go.activeSelf);
            }
            debugLastCommand = "All " + targetObjects.Count + " gameobjects toggled";
        }


        /// <summary>
        /// Calls SetActive(true) on each gameobject in Target Objects list
        /// </summary>
        public void ActivateAll()
        {
            foreach (GameObject go in targetObjects)
            {
                go.SetActive(true);
            }
            debugLastCommand = "All " + targetObjects.Count + " gameobjects activated";
        }


        /// <summary>
        /// Calls SetActive(false) on each gameobject in Target Objects list
        /// </summary>

        public void DeActivateAll()
        {
            foreach (GameObject go in targetObjects)
            {
                go.SetActive(false);
            }
            debugLastCommand = "All " + targetObjects.Count + " gameobjects de-activated";
        }

        /// <summary>
        /// Calls SetActive() on each gameobject in Target Objects list using supplied bool parameter.
        /// </summary>
        public void SetActiveAll(bool state)
        {
            foreach (GameObject go in targetObjects)
            {
                if (go)
                    go.SetActive(state);
                else
                {
                    Debug.Log("Warning: GameObject [" + gameObject.name + "] has incomplely configured OnOffManager\n");
                }
            }
            debugLastCommand = "All " + targetObjects.Count + " gameobjects: SetActive(" + state + ")";
        }

    }
}
