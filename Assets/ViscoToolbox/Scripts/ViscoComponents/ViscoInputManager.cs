﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Visco
{
    public class ViscoInputManager : MonoBehaviour
    {
        public List<ButtonsEvents> ButtonsEventsList;

        void Update()
        {
            if (Input.anyKeyDown)
            {
                for (int i = 0; i < ButtonsEventsList.Count; i++)
                {
                    if (Input.GetKeyDown(ButtonsEventsList[i].keyCode))
                    {
                        ButtonsEventsList[i].keyUnityEvent.Invoke();
                        Main.Instance.GetStatesManager().DoSharedAction((ButtonsEventsList[i].keyViscoEvent));
                        //print(ButtonsEventsList[i].keyCode);
                        break;
                    }
                }
            }
        }
    }

    [Serializable]
    public class ButtonsEvents
    {
        public KeyCode keyCode;
        public EventId keyViscoEvent;
        public UnityEvent keyUnityEvent;

    }
}