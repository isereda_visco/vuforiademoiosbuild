﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visco
{

    // This extension is added to meshes to handle changing their transparency.
    // Multiple callers can ask for changes in transparency, and this script will calculate a resulting transparency
    // and apply to the object.
    //  TODO:  Perhaps extend to scan for mutiple materials?

    public class TransparencyHelper : MonoBehaviour
    {

        // These two Public only for easier debugging - never to be touched by outside clases.
        [Header("Do NOT edit these. This script is automatically attached to Meshes that ViscoTransparencyManager manipulates.")]
        [Tooltip("Shown for easier debugging. Do not touch")] public List<TransparencyTarget> tTargets = new List<TransparencyTarget>();
        [Tooltip("Shown for easier debugging. Do not touch")] private Renderer[] renderers;
        private Material[][] originalMaterials; // index by [rendererIdx][materialIds]
        private float[][] originalAlphas; // Stricly speaking we dont need this jagged float array anymore as we have the originalMaterials

        public float lastAlphaValue = Mathf.PI; // public for debugging only


        // Use this for initialization
        void Start()
        {

            renderers = GetComponents<Renderer>();

            // Store a copy of all materials that could possibly be on this mesh
            originalAlphas = new float[renderers.Length][];
            originalMaterials = new Material[renderers.Length][];
            for (int rCnt = 0; rCnt < renderers.Length; rCnt++)
            {
                Renderer r = renderers[rCnt];
                originalAlphas[rCnt] = new float[r.materials.Length];
                originalMaterials[rCnt] = new Material[r.materials.Length];
                for (int mCnt = 0; mCnt < r.materials.Length; mCnt++)
                {
                    Material m = r.materials[mCnt];
                    if (m.shader.name == "Standard")
                        originalAlphas[rCnt][mCnt] = m.color.a;
                    else if (m.shader.name == "OyvindsShader")
                    {
                        originalAlphas[rCnt][mCnt] = m.GetFloat("_Opacity");
                    }

                    originalMaterials[rCnt][mCnt] = new Material(m);
                }
            }
        }


        public void RegisterTransparencyManipulator(TransparencyTarget t)
        {
            //Debug.Log("Registering transparncytarget to " + gameObject.name + "\n");
            tTargets.Add(t);
        }


        // Update is called once per frame
        void Update()
        {

            if (tTargets.Count == 0)
                return;


            float alphaSum = 1;
            foreach (TransparencyTarget t in tTargets)
            {
                alphaSum = alphaSum * t.GetCurrentAlpha();
            }

            if (alphaSum == lastAlphaValue) // If nothing changed from previous frame then just return
                return;

            SetAlphaForStdShader(alphaSum);
            lastAlphaValue = alphaSum;

        }

        

        private void SetAlphaForStdShader(float alphaSum)
        {

            for (int rCnt = 0; rCnt < renderers.Length; rCnt++)
            {
                Renderer r = renderers[rCnt];
                if (alphaSum == 1.0f)
                {
                    if (!r.enabled)
                        r.enabled = true;

                    for (int mCnt = 0; mCnt < r.materials.Length; mCnt++)
                    {
                        r.materials[mCnt] = originalMaterials[rCnt][mCnt];

                        Material m = r.materials[mCnt];
                        if (m.shader.name == "Standard")
                        {
                            StandardShaderUtils.ChangeRenderMode(m, StandardShaderUtils.BlendMode.Opaque);
                        }
                    }
                }
                else if (alphaSum == 0)
                {
                    r.enabled = false;
                }
                else if (alphaSum > 0 && alphaSum < 1.0f)
                {
                    if (!r.enabled)
                        r.enabled = true;

                    for (int mCnt = 0; mCnt < r.materials.Length; mCnt++)
                    {
                        Material m = r.materials[mCnt];
                        
                        if (m.shader.name == "OyvindsShader")
                        {
                            float resultAlpha = originalAlphas[rCnt][mCnt] * alphaSum;
                            m.SetFloat("_Opacity", alphaSum);
                        }
                        else 
                        {
                           if (lastAlphaValue == 0 || lastAlphaValue == 1.0)
                            {
                                // Only setup shader mode if alpha changed in to "fade-range
                                StandardShaderUtils.ChangeRenderMode(m, StandardShaderUtils.BlendMode.Fade);
                            }

                            float resultAlpha = originalMaterials[rCnt][mCnt].color.a * alphaSum;

                            Color materialColor = m.color;
                            m.SetColor("_Color", new Color(materialColor.r, materialColor.g, materialColor.b, resultAlpha));
                        }
                        
                    }
                }
            }


        }


















        public static class StandardShaderUtils
        {
            public enum BlendMode
            {
                Opaque,
                Cutout,
                Fade,
                Transparent
            }

            public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
            {
                switch (blendMode)
                {
                    case BlendMode.Opaque:
                        standardShaderMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
                        standardShaderMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
                        standardShaderMaterial.SetInt("_ZWrite", 1);
                        standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                        standardShaderMaterial.renderQueue = -1;
                        break;
                    case BlendMode.Cutout:
                        standardShaderMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
                        standardShaderMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.Zero);
                        standardShaderMaterial.SetInt("_ZWrite", 1);
                        standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                        standardShaderMaterial.renderQueue = 2450;
                        break;
                    case BlendMode.Fade:
                        standardShaderMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
                        standardShaderMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        standardShaderMaterial.SetInt("_ZWrite", 0);
                        standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                        standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                        standardShaderMaterial.renderQueue = 3000;
                        break;
                    case BlendMode.Transparent:
                        standardShaderMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.One);
                        standardShaderMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        standardShaderMaterial.SetInt("_ZWrite", 0);
                        standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                        standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                        standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                        standardShaderMaterial.renderQueue = 3000;
                        break;
                }

            }
        }

    }
}