﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Visco
{
    public class SliderHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public static UnityAction<bool> PressedAction;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (PressedAction != null) PressedAction(true);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (PressedAction != null) PressedAction(false);
        }
    }
}