﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Visco
{
    /// <summary>
    /// Provides all ui clicks, avoiding camera move during click
    /// </summary>
    public class BaseClickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        protected Button _button;

        protected void Awake() { _button = GetComponent<Button>(); }

        protected void OnEnable() { _button.onClick.AddListener(Click); }

        protected void OnDisable() { _button.onClick.RemoveAllListeners(); }

        public virtual void Click() { }

        public void OnPointerDown(PointerEventData eventData) { Main.Instance.PointerDonwUI(); }
        public void OnPointerUp(PointerEventData eventData) { Main.Instance.PointerUpUI(); }
    }
}