﻿using System.Collections.Generic;
using UnityEngine;

namespace Visco
{

    public class ContextMenuManager : MonoBehaviour
    {
        public Transform Menu;

        struct ButtonStructure
        {
            public Transform ButtonTransform;
            public ContextButtonManager ButtonManager;
        }

        private List<ButtonStructure> _buttonsList;
        private List<ButtonStructure> _definedButtonsList;
        private List<ContextButtonsStructure> _curreButtonsStructure;

        private GameObject _buttonSinglePrefab;
        private GameObject _buttonLeftPrefab;
        private GameObject _buttonRigthPrefab;
        private GameObject _buttonMiddlePrefab;
        private RectTransform _menuRectTransform;
        private Transform _transform;
        private GameObject _CurrentPrefab;

        private void Awake() { Init(); }

        public void Init()
        {
            _buttonLeftPrefab = (GameObject) Instantiate(Resources.Load(Constants.CONTEXT_BUTTON_LEFT_PREFAB_PATH));
            _buttonMiddlePrefab = (GameObject) Instantiate(Resources.Load(Constants.CONTEXT_BUTTON_MIDDLE_PREFAB_PATH));
            _buttonRigthPrefab = (GameObject) Instantiate(Resources.Load(Constants.CONTEXT_BUTTON_RIGHT_PREFAB_PATH));
            _buttonSinglePrefab = (GameObject) Instantiate(Resources.Load(Constants.CONTEXT_BUTTON_SINGLE_PREFAB_PATH));

            _buttonsList = new List<ButtonStructure>();
            _definedButtonsList = new List<ButtonStructure>();
            _menuRectTransform = Menu.GetComponent<RectTransform>();
            _transform = transform;
            _CurrentPrefab = new GameObject("____curPref");
        }

        public List<ContextButtonsStructure> GetButtonsStructuresList()
        {
            return _curreButtonsStructure;
        }

        public void ShowContextMenu(List<ContextButtonsStructure> list, Vector2 buttonSize)
        {
            _curreButtonsStructure = list;
            if (list == null || list.Count == 0) return;
            _buttonsList = new List<ButtonStructure>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].CustomButton != null) //custom buttons
                {
                    //Adjust optional button
                    _definedButtonsList.Add(new ButtonStructure()
                    {
                        ButtonTransform = list[i].CustomButton,
                        ButtonManager = list[i].CustomButton.GetComponent<ContextButtonManager>()
                    });
                    list[i].CustomButton.GetComponent<ContextButtonManager>().InitButton(list[i]);

                    if (!list[i].UseCustomSizeAndPlaceForCustomButton)
                    {
                        list[i].CustomButton.GetComponent<RectTransform>().sizeDelta = _buttonLeftPrefab.GetComponent<RectTransform>().sizeDelta;
                        list[i].CustomButton.SetParent(Menu);
                        Vector2 size = _menuRectTransform.sizeDelta;
                        size.x += list[i].CustomButton.GetComponent<RectTransform>().sizeDelta.x;
                        _menuRectTransform.sizeDelta = size;
                    }
                }
                else
                {
                    if (_buttonsList.Count < i + 1)
                    {
                        switch (list.Count)
                        {
                            case 1:
                                _CurrentPrefab = _buttonSinglePrefab;
                                break;
                            case 2:
                                _CurrentPrefab = i == 0 ? _buttonLeftPrefab : _buttonRigthPrefab;
                                break;
                            default:
                                if (i == 0) _CurrentPrefab = _buttonLeftPrefab;
                                else if (i == list.Count - 1) _CurrentPrefab = _buttonRigthPrefab;
                                else _CurrentPrefab = _buttonMiddlePrefab;
                                break;
                        }

                        Transform Button = Instantiate(_CurrentPrefab, Menu).transform;
                        Button.GetComponent<RectTransform>().sizeDelta = buttonSize;
                        _buttonsList.Add(new ButtonStructure()
                        {
                            ButtonTransform = Button,
                            ButtonManager = Button.GetComponent<ContextButtonManager>()
                        });
                    }

                    _buttonsList[i].ButtonTransform.SetParent(Menu);
                    _buttonsList[i].ButtonManager.InitButton(list[i]);
                    Vector2 size = _menuRectTransform.sizeDelta;
                    size.x += _buttonsList[i].ButtonTransform.GetComponent<RectTransform>().sizeDelta.x;
                    _menuRectTransform.sizeDelta = size;

                    _buttonsList[i].ButtonTransform.gameObject.SetActive(true);
                }
            }
            foreach (var VARIABLE in _definedButtonsList)
            {
                VARIABLE.ButtonTransform.SetAsLastSibling();
                VARIABLE.ButtonTransform.gameObject.SetActive(true);
            }
            _transform.SetAsLastSibling();
        }

        public void HideContextMenu()
        {
            _curreButtonsStructure = null;
            foreach (ButtonStructure contentButtonsStructure in _buttonsList)
            {
                contentButtonsStructure.ButtonTransform.SetParent(transform);
                contentButtonsStructure.ButtonTransform.gameObject.SetActive(false);
            }
            Vector2 size = Menu.GetComponent<RectTransform>().sizeDelta;
            size = new Vector2(0, size.y);
            Menu.GetComponent<RectTransform>().sizeDelta = size;

            foreach (var VARIABLE in _definedButtonsList)
            {
                VARIABLE.ButtonTransform.gameObject.SetActive(false);
            }
            _definedButtonsList.Clear();
        }
    }
}
