﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class ImageButtonManager : BaseClickButton
    {
        private Text MyText;
        public bool ShowName;
        public string Name;

        private Vector2 _startPosition, _centeredPosition, _targetPosition;
        private Vector2 _originalPivot, _originalAnchorMax, _originalAnchorMin;
        private Vector3 _startScale, _showScale, _targetScale;

        private RectTransform _myRectTransform;
        private RectTransform _CanvasRect;
        [SerializeField] private Transform _myParent;
        private bool _isPlaying;
        public float Speed;
        public float ScaleSpeed;
        public float ScaleDuration;
        private int _siblingIndex;


        private new void Awake()
        {
            base.Awake();
            Init();
        }

        void Init()
        {
            InitNewPosition();

            Speed = Speed != 0 ? Speed : 3f;
            ScaleSpeed = ScaleSpeed != 0 ? ScaleSpeed : 3f;
            ScaleDuration = ScaleDuration != 0 ? ScaleDuration : 0.5f;
        }

        public void InitButton(ImageStruct imageStruct, int siblingIndex)
        {
            if (MyText == null) MyText = GetComponentInChildren<Text>();
            GetComponent<Image>().sprite = imageStruct.MySprite;
            Name = imageStruct.ImageName;
            ShowName = imageStruct.ShowName;
            _siblingIndex = siblingIndex;
            if (ShowName)
            {
                MyText = GetComponentInChildren<Text>();
                MyText.text = Name;
            }
        }

        public void ChangeBaseParent(Transform newParent) { _myParent = newParent; }

        public override void Click()
        {
            _isPlaying = !_isPlaying;
            _targetPosition = _isPlaying ? _centeredPosition : _startPosition;
            _targetScale = _isPlaying ? _showScale : _startScale;

            if (_isPlaying) // move to center
            {
                _myRectTransform.SetParent(_CanvasRect);
                _startPosition = _myRectTransform.position;
                _myRectTransform.SetAsLastSibling();
                if (ShowName && MyText != null) MyText.gameObject.SetActive(false);
                _myRectTransform.DOMove(_targetPosition, ScaleDuration);
                _myRectTransform.DOScale(_targetScale, ScaleDuration);
            }
            else // move to start pos
            {
                if (ShowName && Name != null)
                {
                    MyText.gameObject.SetActive(true);
                }
                _myRectTransform.DOMove(_targetPosition, ScaleDuration).OnComplete(EndMove);
                _myRectTransform.DOScale(_targetScale, ScaleDuration);
            }
        }

        void EndMove()
        {
            _myRectTransform.SetParent(_myParent);
            // _myRectTransform.SetSiblingIndex(_siblingIndex);
            Invoke("SetSibling", 0.5f);
        }
        void SetSibling() { _myRectTransform.SetSiblingIndex(_siblingIndex); }

        public void InitNewPosition()
        {
            if (_myRectTransform == null) _myRectTransform = GetComponent<RectTransform>();
            if (_CanvasRect == null) _CanvasRect = UIMain.Instance.GetMainCanvas().GetComponent<RectTransform>();
            _myParent = _myRectTransform.parent;
            _startPosition = _myRectTransform.anchoredPosition;
            _centeredPosition = _CanvasRect.TransformPoint(Vector3.zero); // Vector2.zero;
            _originalPivot = _myRectTransform.pivot;
            _originalAnchorMin = _myRectTransform.anchorMin;
            _originalAnchorMax = _myRectTransform.anchorMax;

            _targetPosition = _centeredPosition;
            _startScale = _myRectTransform.localScale;
            _targetScale = _startScale;
            _showScale = new Vector3(UIMain.Instance.GetCanvasScale().x / _myRectTransform.sizeDelta.x, UIMain.Instance.GetCanvasScale().y / _myRectTransform.sizeDelta.y);
        }
    }
}