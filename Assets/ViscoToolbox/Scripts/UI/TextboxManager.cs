﻿using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;

namespace Visco
{
    public class TextboxManager : MonoBehaviourEditorUI

#else
namespace Visco
{
public class TextboxManager : MonoBehaviour
#endif
    {
        public Button CloseButton;
        [SerializeField] private Text _text;

        private void OnEnable()
        {
            if (CloseButton)
            {
                CloseButton.onClick.AddListener(Close);
            }
            transform.SetAsLastSibling();
        }

        private void OnDisable()
        {
            if (CloseButton)
            { 
                CloseButton.onClick.RemoveAllListeners();
            }
        }


        /// <summary>
        /// Set the text of this Visco TextboxMananger
        /// </summary>
        /// <param name="txt"></param>
        public void SetText(string txt)
        {
            if (!_text) _text = GetComponentInChildren<Text>();
            _text.text = txt;
        }


        public void Close()
        {
               gameObject.SetActive(false);
        }

#if UNITY_EDITOR

        public void OffElement()
        {
            gameObject.SetActive(false);
        }

        public override bool OnInspectorGUI(Editor editor)
        {
            editor.DrawDefaultInspector();

            if (GUILayout.Button("Off element"))
            {
                OffElement();
            }
            editor.serializedObject.ApplyModifiedProperties();
            return false;
        }
#endif
    }
}