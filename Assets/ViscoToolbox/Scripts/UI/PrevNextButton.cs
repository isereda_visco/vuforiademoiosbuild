﻿namespace Visco
{
    public class PrevNextButton : BaseClickButton
    {
        public bool Previous;
        public bool Loop;

        public override void Click()
        {
            if (Loop)
            {
                if (Previous) Main.Instance.GetStatesManager().GoToPreviousStateLoopMode();
                else Main.Instance.GetStatesManager().GoToNextStateLoopMode();
            }
            else
            {
                if (Previous) Main.Instance.GetStatesManager().GoToPreviousState();
                else Main.Instance.GetStatesManager().GoToNextState();
            }

        }
    }
}