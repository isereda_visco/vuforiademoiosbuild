﻿using UnityEngine.Events;

namespace Visco
{
    public class BackButton : BaseClickButton
    {
        public static event UnityAction ButtonClickAction;

        public override void Click()
        {
            if (ButtonClickAction != null) ButtonClickAction();
        }
    }

}