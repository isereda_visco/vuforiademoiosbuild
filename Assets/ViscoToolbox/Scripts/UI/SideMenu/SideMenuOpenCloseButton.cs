﻿using UnityEngine.Events;

namespace Visco
{
    public class SideMenuOpenCloseButton : BaseClickButton
    {
        public static UnityAction ClickAction;

        public override void Click()
        {
            if (ClickAction != null) ClickAction();
        }
    }
}