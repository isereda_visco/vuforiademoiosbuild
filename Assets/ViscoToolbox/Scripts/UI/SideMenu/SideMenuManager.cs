﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class SideMenuManager : MonoBehaviour
    {
        public Transform MenuGrid;
        private GameObject _videoButtonPrefab;
        private GameObject _imageButtonPrefab;
        private List<GameObject> _videoButtons;
        private List<GameObject> _imageButtons;
        private SideMenuOpenClose _sideMenuOpenClose;


        private void Awake()
        {
            _sideMenuOpenClose = GetComponent<SideMenuOpenClose>();
            _videoButtonPrefab = Resources.Load(Constants.VIDEO_BUTTON_PREFAB_PATH) as GameObject;
            _imageButtonPrefab = Resources.Load(Constants.IMAGE_BUTTON_PREFAB_PATH) as GameObject;
            _videoButtons = new List<GameObject>();
            _imageButtons = new List<GameObject>();
        }

        public void OffMenu() { _sideMenuOpenClose.OffMEnu(); }

        public IEnumerator OffVerticalGroup()
        {
            yield return new WaitForSeconds(0.3f);
            MenuGrid.GetComponent<VerticalLayoutGroup>().enabled = false;

        }

        public IEnumerator OnVerticalGroup()
        {
            MenuGrid.GetComponent<VerticalLayoutGroup>().enabled = true;
            yield return new WaitForFixedUpdate();
        }

        public void InitVideoButtons(List<VideoStruct> videoList)
        {
            foreach (var VARIABLE in _videoButtons)
            {
                Destroy(VARIABLE.gameObject);
            }

            _videoButtons = new List<GameObject>();

            for (int i = 0; i < videoList.Count; i++)
            {
                GameObject go = Instantiate(_videoButtonPrefab, MenuGrid);

                go.name = "videoButton_" + i;
                _videoButtons.Add(go);
                AddToMenu(go);
                go.GetComponent<VideoButtonManager>().InitButton(new VideoStruct(videoList[i]), go.transform.GetSiblingIndex());
            }
        }

        public void InitImageButtons(List<ImageStruct> imagesList)
        {
            foreach (var VARIABLE in _imageButtons)
            {
                Destroy(VARIABLE.gameObject);
            }

            _imageButtons = new List<GameObject>();

            for (int i = 0; i < imagesList.Count; i++)
            {
                GameObject go = Instantiate(_imageButtonPrefab, MenuGrid);

                go.name = "imageButton_" + i;
                _imageButtons.Add(go);
                AddToMenu(go);
                go.GetComponent<ImageButtonManager>().InitButton(new ImageStruct(imagesList[i]), go.transform.GetSiblingIndex());
            }
        }

        public void AddToMenu(GameObject g)
        {
            IChangePlace iChP = g.GetComponent<IChangePlace>();
            if (iChP != null) iChP.InitNewPosition();
        }

        public void AddVideoButton() // for test
        {
            GameObject go = Instantiate(_videoButtonPrefab);
            AddToMenu(go);
        }

        public void AddImageButton() // for test
        {
            GameObject go = Instantiate(_imageButtonPrefab);
            AddToMenu(go);
        }
    }
}