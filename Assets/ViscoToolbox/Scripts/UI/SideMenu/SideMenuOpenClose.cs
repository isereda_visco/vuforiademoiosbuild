﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class SideMenuOpenClose : MonoBehaviour
    {
        private RectTransform _myRectTransform;
        public Text ButtonText;
        private Vector2 _OpenPosition, _closePosition, _targetPosition;
        private bool _isOpen;
        private bool _opening;
        [Tooltip("Opening speed")] public float Speed;

        private void Awake() { Init(); }

        private void Init()
        {
            _myRectTransform = GetComponent<RectTransform>();
            _OpenPosition = _myRectTransform.anchoredPosition;
            _OpenPosition.x = 0;
            _closePosition = new Vector3(_myRectTransform.sizeDelta.x, _OpenPosition.y);
            _myRectTransform.anchoredPosition = _closePosition;
            _isOpen = true;
            OpenClose();
            if (ButtonText == null) throw new Exception(name + "There no ButtonText setted on Inspector");
            Speed = Speed != 0 ? Speed : 5f;
        }

        private void OnEnable() { SideMenuOpenCloseButton.ClickAction += OpenClose; }


        private void OnDisable() { SideMenuOpenCloseButton.ClickAction -= OpenClose; }

        public void OffMEnu()
        {
            _opening = false;
            if (_isOpen)
            {
                _myRectTransform.anchoredPosition = _closePosition;
                _isOpen = false;
            }
            ButtonText.text = _isOpen ? ">>" : "<<";
            gameObject.SetActive(false);
        }

        private void OpenClose()
        {
            _isOpen = !_isOpen;
            _targetPosition = _isOpen ? _OpenPosition : _closePosition;
            _opening = true;
            ButtonText.text = _isOpen ? ">>" : "<<";
            if (_isOpen) _myRectTransform.SetAsLastSibling();
        }


        private void Update()
        {
            if (_opening)
            {
                _myRectTransform.anchoredPosition = Vector2.Lerp(_myRectTransform.anchoredPosition, _targetPosition, Time.deltaTime * Speed);
                if (Vector2.Distance(_myRectTransform.anchoredPosition, _targetPosition) < 0.1f)
                {
                    _opening = false;
                    _myRectTransform.anchoredPosition = _targetPosition;
                }
            }
        }

    }
}