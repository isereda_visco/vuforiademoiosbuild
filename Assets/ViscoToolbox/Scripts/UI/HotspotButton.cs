﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Visco
{
    public class HotspotButton : MonoBehaviour
    {
        public HotspotButtonPulseMode pulseMode;
        public float pulseMinSize = 3f;
        public float pulseMaxSize = 4.5f;
        public float pulseSpeed = 2.0f;

        private UnityAction ClickAction;
        private Transform LookAtPos;

        private float originalScale;

        private float pulseStartTime;
        private bool _isVisible;
        protected Button _button;

        protected void Awake()
        {
            _button = GetComponent<Button>();
            originalScale = transform.localScale.x;
        }

        protected void OnEnable()
        {
            _button.onClick.AddListener(Click);
        }

        protected void OnDisable()
        {
            _button.onClick.RemoveAllListeners();
        }

        private void Click()
        {
            if (ClickAction != null) ClickAction();
        }

        public void InitButton(UnityAction action, GameObject lookAt)
        {
            pulseStartTime = Time.time;
            ClickAction = action;
            LookAtPos = lookAt.transform;
        }
                

        public bool Visible
        {
            get { return _isVisible; }
            set { 
                _isVisible = value;
                GetComponent<Image>().enabled = value;
                if (value)
                    transform.localScale = new Vector3(pulseMinSize, pulseMinSize, pulseMinSize);
                else
                    transform.localScale = new Vector3(0, 0, 0);
            }
        }


        // Update position of 2D button.
        private void Update()
        {
            if (_isVisible)
            {
                // Check that the position is in front of the camera, not behind.
                Vector3 p = Camera.main.WorldToViewportPoint(LookAtPos.position);

                if (0 <= p.x && p.x <= 1.0 && 0 <= p.y && p.y <= 1.0f && p.z > 0)
                {
                    // In cameras field of view

                    transform.position = Camera.main.WorldToScreenPoint(LookAtPos.position);

                    if (pulseMode == HotspotButtonPulseMode.PulseAndFade)
                    {
                        float range = pulseMaxSize - pulseMinSize;
                        float size = pulseMinSize +  (Mathf.Sin( pulseSpeed * (Time.time - pulseStartTime)) + 1.0f)/ 2.0f * range;
                        transform.localScale = new Vector3(size, size, size);
                    }
                }
                else
                {
                    // outside cameras field of view
                    transform.localScale = new Vector3(0, 0, 0);
                }

            }


        }

    }
}
