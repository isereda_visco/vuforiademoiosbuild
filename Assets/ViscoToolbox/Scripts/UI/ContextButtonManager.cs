﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Visco
{
    public class ContextButtonManager : BaseClickButton
    {
        private string ButtonName;
        private EventId ButtonEvent;
        private Text _text;
        [SerializeField] private bool useTwoStates;
        private string SecondaryButtonName;
        private EventId SecondaryButtonEvent;
        private bool firsStateinUse;
        private UnityEvent UnityEvents;
        private UnityEvent SecondaryUnityEvents;
        [SerializeField] private Sprite _normalSprite;
        [SerializeField] private Sprite _activeSprite;
        [SerializeField] private Sprite _howerSprite;
        [SerializeField]


        private void Start()
        {
            if (_text == null) _text = GetComponentInChildren<Text>();
            if (useTwoStates)
            {
                _howerSprite = _button.spriteState.highlightedSprite;
                _activeSprite = _button.spriteState.pressedSprite;
                _normalSprite = new Sprite();
                _normalSprite = GetComponent<Image>().sprite;
            }
        }

        public void InitButton(ContextButtonsStructure butStruct)
        {
            if (_text == null) _text = GetComponentInChildren<Text>();
            _text.text = butStruct.ButtonName;
            ButtonEvent = butStruct.ButtonEvent;
            ButtonName = butStruct.ButtonName;
            useTwoStates = butStruct.useSecondaryButtonState;
            SecondaryButtonEvent = butStruct.SecondaryButtonEvent;
            SecondaryButtonName = butStruct.SecondaryButtonName;
            firsStateinUse = true;
            UnityEvents = butStruct.UnityEvents;
            SecondaryUnityEvents = butStruct.SecondaryUnityEvents;
        }
        public override void Click()
        {
            if (useTwoStates)
            {
                Main.Instance.GetStatesManager().ReceiveClickFromContextButton(firsStateinUse ? ButtonEvent : SecondaryButtonEvent);
                if (firsStateinUse)
                {
                    if (UnityEvents != null) UnityEvents.Invoke();

                }
                else
                {
                    if (SecondaryUnityEvents != null) SecondaryUnityEvents.Invoke();
                }
                ChangeSprites();
                firsStateinUse = !firsStateinUse;
                SetButtonText();
            }
            else
            {
                Main.Instance.GetStatesManager().ReceiveClickFromContextButton(ButtonEvent);
                if (UnityEvents != null) UnityEvents.Invoke();
            }
        }

        void SetButtonText() { _text.text = useTwoStates ? (firsStateinUse ? ButtonName : SecondaryButtonName) : ButtonName; }

        void ChangeSprites()
        {
            if (firsStateinUse)
            {
                GetComponent<Image>().sprite = _normalSprite;
            }
            else
            {
                GetComponent<Image>().sprite = _howerSprite;
            }
            _button.enabled = false;
            _button.enabled = true;
        }
    }
}