﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class UIMain : MonoBehaviour
    {
        public static UIMain Instance;
        private Canvas _mainCanvas;

        [Header("Optional parameters")] [Tooltip("default size - Vector2(100f,60f)")] public Vector2 DefaultContextMenuButtonsSize = new Vector2(100f, 60f);
        [Tooltip("Created automatically if needed")] [SerializeField] private GameObject BackButon;
        [Tooltip("Created automatically, in top left corner, or assign  to have own default text box")] [SerializeField] private GameObject DefaultTextBox;
        [SerializeField] private GameObject DefaultVideoButton;
        [SerializeField] private GameObject DefaultVideoPlayer;
        [SerializeField] private GameObject DefaultImageButton;
        [SerializeField] private GameObject ContextMenu;
        [SerializeField] private GameObject SideMenu;

        [SerializeField] private bool showPrevNextButtons;
        [SerializeField] private GameObject PreviousStateButton;
        [SerializeField] private GameObject NextStateButton;
        private CommonTools _currentcommonTools;
        private ContextMenuManager _contextMenuManager;
        private VideoButtonManager _defaultVideoButtonManager;
        private VideoPlayerManager _defaultVideoPlayerManager;
        private ImageButtonManager _defaultImageButtonManager;
        private SideMenuManager _sideMenuManager;
        private List<GameObject> _textBoxesList; // default text boxes list

        public static UIMain GetInstance() { return Instance; }

        private void Awake()
        {
            Debug.Log("UI Main Awake\n");
            Instance = this;
            if (_textBoxesList == null) _textBoxesList = new List<GameObject>();
            _mainCanvas = GetComponent<Canvas>();
            if (!_mainCanvas) throw new Exception("there no Canvas component attached to : " + name);

            if (showPrevNextButtons)
            {
                if (PreviousStateButton == null)
                {
                    GameObject obj = Resources.Load(Constants.PREVIOUS_STATE_BUTTON_PREFAB_PATH) as GameObject;
                
                    PreviousStateButton = Instantiate(obj, _mainCanvas.transform);
                    //PreviousStateButton = PrefabUtility.InstantiatePrefab(obj) as GameObject;
                    //PreviousStateButton.transform.SetParent(_mainCanvas.transform);
                    PreviousStateButton.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                }
                if (NextStateButton == null)
                {
                    var obj = Resources.Load(Constants.NEXT_STATE_BUTTON_PREFAB_PATH) as GameObject;
                    NextStateButton = Instantiate(obj, _mainCanvas.transform);
                    // NextStateButton = PrefabUtility.InstantiatePrefab(obj) as GameObject;
                    // NextStateButton.transform.SetParent(_mainCanvas.transform);
                    NextStateButton.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
                }
                PreviousStateButton.SetActive(true);
                NextStateButton.SetActive(true);
            }
            else
            {
                if (PreviousStateButton != null) PreviousStateButton.SetActive(false);
                if (NextStateButton != null) NextStateButton.SetActive(false);
            }
        }
        public void ApplyCommonTools(CommonTools commonTools)
        {
            _currentcommonTools = commonTools;
            ShowContextMenu(commonTools);
            ShowTextBoxes(commonTools);
            ShowVideo(commonTools);
            ShowImage(commonTools);
            ShowSideMenu(commonTools);
        }
        /// <summary>
        /// activates /unactivates BackButton
        /// </summary>
        public void ActivateBackButton(bool activate)
        {
            GetBackButton().SetActive(activate);
        }

        /// <summary>
        /// Activates the default description text box
        /// </summary>
        public void ShowDescription(bool active, string description = "")
        {
            GetDefaultTextBox().SetActive(active && description != ""); // activates description panel when on and not empty
            if (active && description != "") GetDefaultTextBox().GetComponent<TextboxManager>().SetText(description);
        }


        /// <summary>
        /// Activates the custom description text box(es)
        /// </summary>
        public void ShowTextBoxes(CommonTools commonTools)
        {
            if (_textBoxesList != null)
            {
                foreach (GameObject obj in _textBoxesList)
                {
                    if (obj)
                        obj.SetActive(false);
                }
            }

            if (commonTools.GetCustomDescriptionTextBoxesList() != null)
            {
                _textBoxesList = commonTools.GetCustomDescriptionTextBoxesList();
                foreach (GameObject obj in _textBoxesList)
                {
                    if (obj)
                    obj.SetActive(true);
                }
            }
        }
        public void ShowTextBoxes(List<GameObject> descriptionBoxesList)
        {

            if (_textBoxesList != null) foreach (GameObject obj in _textBoxesList) obj.SetActive(false);
            _textBoxesList = descriptionBoxesList;
            if (descriptionBoxesList == null || descriptionBoxesList.Count == 0) return;

            foreach (GameObject obj in descriptionBoxesList) obj.SetActive(true);
        }
        public List<GameObject> GetTexBoxesList() { return _textBoxesList; }
        /// <summary>
        /// Creates context menu
        /// </summary>
        public void ShowContextMenu(CommonTools commonTools)
        {
            List<ContextButtonsStructure> buttonsList = commonTools.GetContentButtonsList();
            if (ContextMenu == null) ContextMenu = GetSContextMenu();
            if (buttonsList == null) return;
            _contextMenuManager.ShowContextMenu(buttonsList, commonTools.GetContentButtonsSize());
        }
        public void ShowContextMenu(List<ContextButtonsStructure> buttonsList, Vector2 buttonSize)
        {
            if (ContextMenu == null) ContextMenu = GetSContextMenu();
            if (buttonsList == null) return;
            _contextMenuManager.ShowContextMenu(buttonsList, buttonSize);
        }
        public void HideContextMenu()
        {
            if (_contextMenuManager != null) _contextMenuManager.HideContextMenu();
        }
        public List<ContextButtonsStructure> GetPreviousContextMenuStructure() { return _contextMenuManager.GetButtonsStructuresList(); }

        public void ShowVideo(CommonTools commonTools)
        {
            if (commonTools.GetVideo() != null)
            {
                if (commonTools.GetVideo().useVideoPlayer)
                {
                    GetDefaultVideoPlayer().SetActive(true);
                    _defaultVideoPlayerManager.InitButton(commonTools.GetVideo(), DefaultVideoPlayer.transform.GetSiblingIndex());
                }
                else
                {
                    GetDefaultVideoButton().SetActive(true);
                    _defaultVideoButtonManager.InitButton(commonTools.GetVideo(), DefaultVideoButton.transform.GetSiblingIndex());
                }
            }
            else
            {
                if (DefaultVideoButton != null) DefaultVideoButton.SetActive(false);
                if (DefaultVideoPlayer != null) DefaultVideoPlayer.SetActive(false);
            }
        }

        public void ShowVideo(VideoStruct videoStruct)
        {
            if (videoStruct == null)
            {
                GetDefaultVideoButton().SetActive(false);
                return;
            }
            GetDefaultVideoButton().SetActive(true);
            _defaultVideoButtonManager.InitButton(videoStruct, DefaultVideoButton.transform.GetSiblingIndex());
        }

        public void ShowStateVideo() { ShowVideo(_currentcommonTools); }

        public VideoStruct GetVideo() { return (_defaultVideoButtonManager == null) ? null : _defaultVideoButtonManager.GetVideoStruct(); }

        public void ShowImage(CommonTools commonTools)
        {
            if (commonTools.GetImage() != null)
            {
                GetDefaultImageButton().SetActive(true);
                _defaultImageButtonManager.InitButton(commonTools.GetImage(), DefaultImageButton.transform.GetSiblingIndex());
            }
            else
            {
                if (DefaultImageButton != null) DefaultImageButton.SetActive(false);
            }
        }
        public void ShowImage(ImageStruct imageStruct)
        {
            if (imageStruct != null)
            {
                GetDefaultImageButton().SetActive(true);
                _defaultImageButtonManager.InitButton(imageStruct, DefaultImageButton.transform.GetSiblingIndex());
            }
            else
            {
                if (DefaultImageButton != null) DefaultImageButton.SetActive(false);
            }
        }
        public void ShowStateImage() { ShowImage(_currentcommonTools); }
        public void ShowSideMenu(CommonTools commonTools)
        {
            if (commonTools.GetVideosListFromSideMenu() != null)
            {
                GetSideMenu().SetActive(true);
                _sideMenuManager.StartCoroutine("OnVerticalGroup");
                _sideMenuManager.InitVideoButtons(new List<VideoStruct>(commonTools.GetVideosListFromSideMenu()));
            }

            if (commonTools.GetImagesListFromSideMenu() != null)
            {
                GetSideMenu().SetActive(true);
                _sideMenuManager.StartCoroutine("OnVerticalGroup");
                _sideMenuManager.InitImageButtons(new List<ImageStruct>(commonTools.GetImagesListFromSideMenu()));
            }

            if (commonTools.GetVideosListFromSideMenu() == null && commonTools.GetImagesListFromSideMenu() == null)
            {
                if (_sideMenuManager != null) _sideMenuManager.OffMenu();
            }
            if (_sideMenuManager != null && GetSideMenu().activeSelf) _sideMenuManager.StartCoroutine("OffVerticalGroup");
        }

        public void ShowSideMenu(List<VideoStruct> videos, List<ImageStruct>images)
        {
            if (videos != null)
            {
                GetSideMenu().SetActive(true);
                _sideMenuManager.StartCoroutine("OnVerticalGroup");
                _sideMenuManager.InitVideoButtons(new List<VideoStruct>(videos));
            }

            if (images != null)
            {
                GetSideMenu().SetActive(true);
                _sideMenuManager.StartCoroutine("OnVerticalGroup");
                _sideMenuManager.InitImageButtons(new List<ImageStruct>(images));
            }

            if (videos == null && images == null)
            {
                if (_sideMenuManager != null) _sideMenuManager.OffMenu();
            }
            if (_sideMenuManager != null) _sideMenuManager.StartCoroutine("OffVerticalGroup");
        }
        public void ShowStateSideMenu() { ShowSideMenu(_currentcommonTools); }

        public void ShowStateStatus(int stateNumber, int statesCount, string stateDescription)
        {
            string stateText = string.Format("State: {0}/{1}\n{2}", stateNumber + 1, statesCount, stateDescription);
            ShowDescription(true, stateText);
        }

        #region Return or creating missing components

        private GameObject GetBackButton()
        {
            if (BackButon == null)
            {
                GameObject pref = Resources.Load(Constants.BACK_BUTTON_PREFAB_PATH) as GameObject;
                BackButon = Instantiate(pref, GetMainCanvas().transform);
            }
            return BackButon;
        }

        private GameObject GetDefaultTextBox()
        {
            if (DefaultTextBox == null)
            {
                GameObject pref = Resources.Load(Constants.TEXT_BOX_PREFAB_PATH) as GameObject;
                DefaultTextBox = Instantiate(pref, GetMainCanvas().transform);
            }
            return DefaultTextBox;
        }

        private GameObject GetDefaultVideoButton()
        {
            if (DefaultVideoButton == null)
            {
                GameObject pref = Resources.Load(Constants.VIDEO_BUTTON_PREFAB_PATH) as GameObject;
                DefaultVideoButton = Instantiate(pref, GetMainCanvas().transform);
                _defaultVideoButtonManager = DefaultVideoButton.GetComponent<VideoButtonManager>();
            }
            return DefaultVideoButton;
        }

        private GameObject GetDefaultVideoPlayer()
        {
            if (DefaultVideoPlayer == null)
            {
                GameObject pref = Resources.Load(Constants.VIDEO_PLAYER_PREFAB_PATH) as GameObject;
                DefaultVideoPlayer = Instantiate(pref, GetMainCanvas().transform);
                _defaultVideoPlayerManager = DefaultVideoPlayer.GetComponent<VideoPlayerManager>();
            }
            return DefaultVideoPlayer;
        }

        private GameObject GetDefaultImageButton()
        {
            if (DefaultImageButton == null)
            {
                GameObject pref = Resources.Load(Constants.IMAGE_BUTTON_PREFAB_PATH) as GameObject;
                DefaultImageButton = Instantiate(pref, GetMainCanvas().transform);
                _defaultImageButtonManager = DefaultImageButton.GetComponent<ImageButtonManager>();
            }
            return DefaultImageButton;
        }

        private GameObject GetSideMenu()
        {
            if (SideMenu == null)
            {
                GameObject pref = Resources.Load(Constants.SIDE_MENU_PREFAB_PATH) as GameObject;
                SideMenu = Instantiate(pref, GetMainCanvas().transform);
                _sideMenuManager = SideMenu.GetComponent<SideMenuManager>();
            }
            return SideMenu;
        }

        private GameObject GetSContextMenu()
        {
            if (ContextMenu == null)
            {
                GameObject pref = Resources.Load(Constants.CONTEXT_MENU_PREFAB_PATH) as GameObject;
                ContextMenu = Instantiate(pref, GetMainCanvas().transform);
                _contextMenuManager = ContextMenu.GetComponent<ContextMenuManager>();
            }
            return ContextMenu;
        }

        public Canvas GetMainCanvas()
        {
            if (_mainCanvas == null)
            {
                throw new Exception("There no Canvas setted on Inspector");
            }
            return _mainCanvas;
        }
        public Vector2 GetCanvasScale() { return _mainCanvas.GetComponent<CanvasScaler>().referenceResolution; }
        #endregion


    }
}