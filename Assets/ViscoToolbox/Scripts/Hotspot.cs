﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Visco
{

#if UNITY_EDITOR
    using UnityEditor;

    [Serializable]

    public class Hotspot : MonoBehaviourEditorUI
#else 
public class Hotspot : MonoBehaviour
#endif
    {
        [Header("Required parameters")]
        public EventId PressHotSpotEvent;
        public GameObject CamObject;
        //public TrackingMode cameraTrackingMode;

        public GameObject LookAtObject;
        public TrackingMode lookAtObjectTrackingMode;

        private Vector3 LookAtObjectOriginalPos; // Used for tracking if lookAtObj is moving.
        private Vector3 HotspotObjectOriginalPos;

        [HideInInspector]
        public  Vector3 LookAtObjectDeltaMove; // CameraManager checks this and updates Cameras pos if it has moved and Hotspot has (followIfMoving = true)

        //public bool followIfMoving;
        public bool slaveCameraMode;   // Let camera be 100% controlled by LookAtObject.

        public HotspotMoveToMode hotspotMoveToMode;


        public HotspotPrefabNames buttonPrefab;
        public hotspotButtonShowMode buttonShowMode;

        public bool hideBackButton;
        
        [Header("Camera FOV")]
        [Tooltip("Specify FOV or leave at 0 for no change.")]
        public float cameraFov;

        [Header("Optional parameters")] [Tooltip("Prevent user from changing the cameras position, however still allow rotating camera")]

        public bool HideSiblingsWhenActive;
        public bool LockPosition;

        [Tooltip("This option together with Lock Position prevents the user from changing the camera in any way")]
        public bool LockRotation;


        [Tooltip("If Button not defined, button will be generated automatically, from prefab")]
        public GameObject Button;
        private HotspotButton hotspotButton;

        [Tooltip("If UseCameraAngelsLimit = true, camera manager use limits, setted below")]
        public bool UseCameraAnglesLimit;
        public float MinCameraAngle; // min camera angle, to avoid camera go thrue ground
        public float MaxCameraAngle; // max angle

        [Tooltip("If UseCameraZoomDistanceLimit = true, camera manager use zoom limits, setted below")] public bool UseCameraZoomDistanceLimit;
        public float MinZoomDistance; // min dist
        public float MaxZoomDistance; // max dist

        [Tooltip("If AllowUserInterruptCameraTransition = true, the  user can stop camera transition to this Hotspot by clicking or tapping on screen")]
        public bool AllowUserInterruptCameraTransition = true;

        [Tooltip("Maximum distance, on which we allow the user interupt camera transition, if zero - interrupt in  any distance")]
        public float DistanceToInterruptCameraTransition;

        public bool customPulse = false;
        public HotspotButtonPulseMode buttonPulseMode = HotspotButtonPulseMode.PulseAndFade;
        public float pulseMinSize = 3f;
        public float pulseMaxSize = 4.5f;
        public float pulseSpeed = 1.0f;

        public GameObject CustomButtonPosition;
        

        [Tooltip("Orbit camera around target objects Y-axis at this speed.")] public float autoOrbitSpeed;
        [Tooltip("Seconds to wait before starting autoOrbit")] public float autoOrbitDelay;
        public bool useCameraSettingFromHotSpotCamera;

        [Header("Editor mode settings")]

        public float GizmosRadius = .5f;
        public Color GizmoColor = Color.gray;
        public State State;



        
        [Tooltip("If ticked, the hotspot use its own attached ViscoComponents. Note - if component is empty - it will just remove similar state component")]
        [Header("Use components from hotspot instead of State")]
        public bool OverrideStateViscoComponents;




        // ------------------   Actions ------------------------------------------------------------

        public bool goToHotspotOnClick = true;


        public bool useCameraStartGoingToActions;
        [Header("Actions when camera starts going to this hotspot")]
        [Tooltip("These actions will be triggered when main camera starts it's journey to this hotspot")]
        public ReorderableEventList cameraStartGoingToActions;

        public bool useCameraArriveActions;
        [Header("Actions when camera arrives at this hotspot")]
        [Tooltip("These actions will be triggered when main camera arrives at this hotspot")]
        public ReorderableEventList cameraArriveActions;
                
        public bool useCameraLeaveActions;
        [Header("Actions when camera leaves this hotspot")]
        [Tooltip("These actions will be triggered when main camera leaves the hotspot.")]
        public ReorderableEventList cameraLeaveActions;
        
        public bool useBackButtonActions;
        [Header("Back-button Actions")]
        [Tooltip("These actions will be triggered when the user clicks the back-button.")]
        public ReorderableEventList backButtonActions;

        public bool useCameraGoToChildActions;
        [Header("Actions when camera leaves this hotspot")]
        [Tooltip("These actions will be triggered when main camera leaves the hotspot.")]
        public ReorderableEventList cameraGoToChildActions;

        public bool useCameraGoToSiblingActions;
        [Header("Actions when camera leaves this hotspot")]
        [Tooltip("These actions will be triggered when main camera leaves the hotspot.")]
        public ReorderableEventList cameraGoToSiblingActions;




        // ------------------   Actions ------------------------------------------------------------



        private Stack<Vector3> editedPositions = new Stack<Vector3>();

        [HideInInspector] public CommonTools MyCommonTools;
        
        // -----------------------------------------------------------------------------






        private void Awake()
        {
            if (CamObject == null)
            {
                CamObject = gameObject;     // If no "external CamObject" out in scene-tree, then use this hotspot itself
            }
        }

        private void Start()
        {

            if (LookAtObject == null)
            {
                throw new Exception("ERROR: Hotspot '" + gameObject.name + "' has no LookAtObject asigned\n");
            }


            CheckCameraLimits();

            if (buttonShowMode != hotspotButtonShowMode.Never)
            {
                Create2DButton();
            }

            MyCommonTools = gameObject.GetCommonTools();

            LookAtObjectOriginalPos = LookAtObject.transform.position;
            HotspotObjectOriginalPos = CamObject.transform.position;

            if (ButtonShowMode == hotspotButtonShowMode.Always)
                Visible = true;
        }

        public hotspotButtonShowMode ButtonShowMode
        {
            get { return buttonShowMode; }
            set
            {
                buttonShowMode = value;
                if (value == hotspotButtonShowMode.Never)
                {
                    if (Button)
                        Destroy(Button);
                }
            }
        }



        private void Update()
        {

        }


        public void ShowButton()
        {
            if (buttonShowMode != hotspotButtonShowMode.Never)
            {
                   HotspotButton.Visible = true;
            }
        }

        public void HideButton()
        {
            if (buttonShowMode != hotspotButtonShowMode.Always)
            { 
                    HotspotButton.Visible = false;
            }
        }

        /// <summary>
        /// Set visibility status of the hotspots 2D icon
        /// </summary>
        /// <param name="Visible"></param>
        public bool Visible
        {
            get { return HotspotButton.Visible;  }
            set
            {
                if (value == true && buttonShowMode != hotspotButtonShowMode.Never)
                {
                    HotspotButton.Visible = value;
                }
                if (value == false && buttonShowMode != hotspotButtonShowMode.Always)
                {
                    HotspotButton.Visible = value;
                }
            }
        }

        public HotspotButton HotspotButton
        {
            get  {
                if (!hotspotButton)
                    Create2DButton();
                return hotspotButton;
            }
            set { hotspotButton = value;     }
        }

        private void Create2DButton()
        {
            string prefabName = buttonPrefab.ToString();
            Button = (GameObject)Instantiate(Resources.Load("ToolboxPrefabs\\" + prefabName), UIMain.Instance.GetMainCanvas().transform);
            Button.name = name + "_Button";

            HotspotButton = Button.GetComponent<HotspotButton>();
            if (HotspotButton == null)
            {
                HotspotButton = Button.AddComponent<HotspotButton>();
            }

            if (customPulse)
            {
                HotspotButton.pulseMinSize = pulseMinSize;
                HotspotButton.pulseMaxSize = pulseMaxSize;
                HotspotButton.pulseMode = buttonPulseMode;
                HotspotButton.pulseSpeed = pulseSpeed;
            }

            HotspotButton.InitButton(HotspotButton_OnClick, CustomButtonPosition == null ? LookAtObject : CustomButtonPosition); //setting clicking callback and position
        }




        void CheckCameraLimits()
        {
            if (UseCameraAnglesLimit)
            {
                Vector3 lookVector = CamObject == null ? CamObject.transform.position - LookAtObject.transform.position : CamObject.transform.position - LookAtObject.transform.position;
                float angle = Vector3.Angle(lookVector, new Vector3(lookVector.x, 0, lookVector.z));

                if (lookVector.y < 0) angle = -angle;

                if (MinCameraAngle > angle)
                {
                    MinCameraAngle = angle;
                    Debug.Log(name + " new  MinCameraAngle assigned: " + MinCameraAngle);
                }

                if (MaxCameraAngle < angle)
                {
                    MaxCameraAngle = angle;
                    Debug.Log(name + " new  MaxCameraAngle assigned: " + MaxCameraAngle);
                }
            }

            if (UseCameraZoomDistanceLimit)
            {
                float distance = Vector3.Distance(CamObject == null ? CamObject.transform.position : CamObject.transform.position, LookAtObject.transform.position);

                if (distance < MinZoomDistance)
                {
                    MinZoomDistance = distance;
                    Debug.Log(name + " new MinZoomDistance assigned " + MinZoomDistance);
                }
                if (distance > MaxZoomDistance)
                {
                    MaxZoomDistance = distance;
                    Debug.Log(name + " new MaxZoomDistance assigned " + MaxZoomDistance);
                }
            }
        }

        // When the 2D button is clicked, pass on click to StatesManager
        public void HotspotButton_OnClick()
        {
            if (goToHotspotOnClick)
                Main.Instance.GetStatesManager().ReceiveClickFromHotSpot(gameObject);
        }



        public void ExecuteCameraArriveActions()
        {
                ExecuteEventList(cameraArriveActions, "cam-arrive-action");
        }
        public void ExecuteCameraLeaveActions()
        {
               ExecuteEventList(cameraLeaveActions, "cam-leave-action");
        }
        public void ExecuteCameraStartGoingToActions()
        {
            ExecuteEventList(cameraStartGoingToActions, "cam-start-go-to action");
        }
        public void ExecuteBackButtonActions()
        {
            ExecuteEventList(backButtonActions, "back-button action");
        }
        public void ExecuteCameraGoToChildActions()
        {
            ExecuteEventList(cameraGoToChildActions, "cam-go-to-child-hotspot action");
        }
        public void ExecuteCameraGoToSiblingActions()
        {
            ExecuteEventList(cameraGoToSiblingActions, "cam-go-to-sibling'-action");
        }





        public void ExecuteEventList(ReorderableEventList eventList, string listName)
        {
            foreach (var eventDelegate in eventList.List)
            {
                if (eventDelegate.mwaitForCamera == true || eventDelegate.mdelay > 0)
                {
                    throw new Exception("Hotspot " + gameObject.name + ": " + listName + " do not support 'Wait for cam' or delay. You must untick 'WaitForCam' and zero out 'delay' for action '" + eventDelegate.mEventName + "'");
                }
                {
                    if (!eventDelegate.isValid)
                    {
                        throw new Exception("Hotspot " + gameObject.name + " has an incomplete action (missing parameter?) in the action list '" + listName + "'");
                    }

                    string dbg = "Executing hotspot [" + gameObject.name + "] " + listName + ": [ " +  eventDelegate.target.name + "." + eventDelegate.methodName + "(";

                    if (eventDelegate.parameters != null)
                    {
                        foreach (EventDelegate.Parameter p in eventDelegate.parameters)
                        {
                            if (p.name != null)
                                dbg += p.name + "=" + p.value.ToString() + ",";
                        }
                        dbg = dbg.Substring(0, dbg.Length - 1);
                        
                    }
                    dbg += ") ]\n";



                    Debug.Log(dbg);

                    eventDelegate.Execute();
                }
            }
        }




#if UNITY_EDITOR

        bool call;
        bool disableNextUpdate;

        public void OnDrawGizmosSelected()
        {
            
            if (!call)
            {
                call = true;
                if (UseCameraAnglesLimit)
                {
                    Vector3 lookVector = CamObject == null ? transform.position - LookAtObject.transform.position : CamObject.transform.position - LookAtObject.transform.position;
                    float angle = Vector3.Angle(lookVector, new Vector3(lookVector.x, 0, lookVector.z));


                    if (lookVector.y < 0) angle = -angle;

                    if (MinCameraAngle > angle)
                    {
                        Debug.Log(name + " - MinCameraAngle is bad, use another limit angle or move up target object \n" + "MinCameraAngle is " + MinCameraAngle +
                                  " angle looking from target to lookAtObject is " + angle);
                    }

                    if (MaxCameraAngle < angle)
                    {
                        Debug.Log(name + " - MaxCameraAngle is bad, use another, bigger limit angle or move down target object\n" + "MinCameraAngle is " + MaxCameraAngle +
                                  " angle looking from target to lookAtObject is " + angle);
                    }
                }

                if (UseCameraZoomDistanceLimit)
                {
                    float distance = Vector3.Distance(CamObject == null ? transform.position : CamObject.transform.position, LookAtObject.transform.position);

                    if (distance < MinZoomDistance)
                    {
                        Debug.Log(name + "'s position is closer to lookAtObject than the assigned Min cam distance of " + MinZoomDistance + " units");
                    }
                    if (distance > MaxZoomDistance)
                    {
                        Debug.Log(name + "'s position is further away than the assigned Max cam distance of " + MaxZoomDistance + " units");
                    }
                }
            }
            disableNextUpdate = false;
        }

        public void OnDrawGizmos()
        {
            //Debug.Log("OnDrawGizmos:" + gameObject.name + "\n");

            if (disableNextUpdate)
            {
                call = false;
            }
            disableNextUpdate = true;

            Vector3 finalCamPos = (CamObject == null ? transform.position : CamObject.transform.position);
            Vector3 textlabelYoffset = new Vector3(0, GizmosRadius * 1.5f, 0);

            // Initialize new hotspots gizmocolor to grey;
            Color32 gizC = GizmoColor;
            int colNum = gizC.r + gizC.g + gizC.b + gizC.a;
            if (GizmoColor == null || colNum == 0)
            {
                GizmoColor = Color.gray;
            }

            // If hotspot is selected, always draw in bright green color
            if (Selection.activeGameObject == gameObject)
            {
                Gizmos.color = Color.green;
                Vector3 middleOfLine = (finalCamPos + LookAtObject.transform.position) / 2 + textlabelYoffset / 3;
                UnityEditor.Handles.Label(middleOfLine, "Dist = " + Vector3.Distance(finalCamPos, LookAtObject.transform.position).ToString());
            }
            else
            {
                Gizmos.color = GizmoColor;
            }

                        

            UnityEditor.Handles.Label(finalCamPos + textlabelYoffset, gameObject.name);
            Gizmos.DrawSphere(LookAtObject.transform.position, GizmosRadius);

            Gizmos.DrawWireSphere(finalCamPos, GizmosRadius);

            Gizmos.DrawLine(finalCamPos, LookAtObject.transform.position);
            if (CustomButtonPosition)
            {
                Gizmos.DrawWireCube(CustomButtonPosition.transform.position, new Vector3(GizmosRadius / 3f, GizmosRadius / 3f, GizmosRadius / 3f));
            }
        }


        





        public void LookAt()
        {
            GameObject activeGameobject = CamObject == null ? gameObject : CamObject.gameObject;
            Quaternion oldRotation = activeGameobject.transform.rotation;
            activeGameobject.transform.LookAt(LookAtObject.transform);
            activeGameobject.transform.rotation = oldRotation;
        }
        public GameObject ReturnCamObject() { return CamObject == null ? gameObject : CamObject.gameObject; }









        public void InitCustomButtonPosition(SerializedProperty pr)
        {
            if (!CustomButtonPosition)
            {
                GameObject go = new GameObject("_" + name + "CustomBtnPos");
                pr.objectReferenceValue = go;
                Debug.Log(pr.objectReferenceValue.name);
                go.transform.SetParent(LookAtObject.transform);

                Vector3 camPos = CamObject == null ? transform.position : CamObject.transform.position;

                Vector3 dir = LookAtObject.transform.position - camPos;
                go.transform.position = camPos + (dir / 2f);
                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(go.scene);
            }
            else
            {
                print("CustomButtonPosition  already assigned");
            }
        }
        public void AlignHotSpotToSceneCamera()
        {
            Vector3 oldpos = LookAtObject.transform.position;
            Quaternion oldRot = LookAtObject.transform.rotation;
            GameObject gameObject = ReturnCamObject();
            editedPositions.Push(gameObject.transform.position);
            AlignWithView(gameObject);
            LookAtObject.transform.position = oldpos;
            LookAtObject.transform.rotation = oldRot;
        }
        public void AlignViewToThisHotSpot()
        {
            GameObject activeGameobject = ReturnCamObject();
            Quaternion oldRotation = activeGameobject.transform.rotation;
            activeGameobject.transform.LookAt(LookAtObject.transform);
            AlignViewtoSelected(activeGameobject);
            activeGameobject.transform.rotation = oldRotation;
        }

        public void UndoAlignHotSpotToSceneCameraAction()
        {
            if (editedPositions.Count != 0)
            {
                Vector3 oldpos = LookAtObject.transform.position;
                Quaternion oldRot = LookAtObject.transform.rotation;
                transform.position = editedPositions.Pop();
                LookAtObject.transform.position = oldpos;
                LookAtObject.transform.rotation = oldRot;
            }
        }



        public void MarkSceneDirty(SerializedProperty pr)
        {
            if (!EditorApplication.isPlaying)
            { 
                GameObject go = new GameObject();
                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(go.scene);
                DestroyImmediate(go);
            }
        }

        /// <summary>
        /// aling scene camera view to this object
        /// </summary>
        public static void AlignViewtoSelected(GameObject go)
        {
            Selection.activeGameObject = go;
            EditorApplication.ExecuteMenuItem("GameObject/Align View to Selected");
        }
      


        // Struct to keep transform data for child-nodes when we move a parent, and want to preserve childrens global position
        public struct transformStruct
        {
            public Vector3 position;
            public Quaternion rotation;
            public Vector3 localScale;
        }

        public static void AlignWithView(GameObject go)
        {
            Selection.activeGameObject = go;

            Dictionary<GameObject, transformStruct> SavePos = new Dictionary<GameObject, transformStruct>();

            // Save childrens transform values so we can restore them after move
            foreach (Transform t in go.transform)
            {
                transformStruct tmp = new transformStruct();

                tmp.position = new Vector3(t.position.x, t.position.y, t.position.z);
                tmp.rotation = new Quaternion(t.rotation.x, t.rotation.y, t.rotation.z, t.rotation.w);
                tmp.localScale = new Vector3(t.localScale.x, t.localScale.y, t.localScale.z);

                SavePos.Add(t.gameObject, tmp);
            }

            EditorApplication.ExecuteMenuItem("GameObject/Align With View");

            foreach (KeyValuePair<GameObject, transformStruct> kvp in SavePos)
            {
                kvp.Key.transform.position = kvp.Value.position;
                kvp.Key.transform.rotation = kvp.Value.rotation;
                kvp.Key.transform.localScale = kvp.Value.localScale;
            }

        }

#endif
    }

    [Serializable]
    public enum HotspotButtonPulseMode
    {
        PulseAndFade, None
    }



    [Serializable]
    public enum HotspotMoveToMode
    {
        Slide, Teleport
    }


    [Serializable]
    public enum StatesHotspotMoveToMode
    {
       UseHotspotSetting, ForceSlide, ForceTeleport
    }
    


    [Serializable]
    public enum hotspotButtonShowMode
    {
        WhenParentActive, WhenParentOrSelfActive, Never, Always
    }

    [Serializable]
    public enum TrackingMode
    {
        Instant,
        Lerp,
        SmoothDamp
    }



}