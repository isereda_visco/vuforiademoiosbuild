﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Visco
{

#if UNITY_EDITOR
    using UnityEditor;

    public class State : MonoBehaviourEditorUI
#else
public class State : MonoBehaviour
#endif
    {
        [HideInInspector] public int StateIndex;



        [Tooltip("The short descriptive 'name' of this state. Usually displayed in top left corner")]
        public string StateDescription;
        public bool showStateDescription;



        [Tooltip("Comments and notes. Never shown in presentation!")]
        [TextArea(3, 25)]
        public string DevelopersOwnNotes;


        [Space(10)]
        [Header("Hotspot/viewpoint to move camera to for this state")]
        public GameObject MyHotspot; // todo hotspotmanager connect, to avoid assigning missmach object

        public StatesHotspotMoveToMode statesHotspotMoveToMode;

        [Space(10)]
        [Tooltip("These events will run when we arrive at this state")]
        [Header("Arrival events")]
        public ReorderableEventList OnArrivalEvents;



        [Space(10)]
        [Tooltip("These events will be triggered when the system starts transition to next state")]
        [Header("Events to run when user press 'Next'")]
        public ReorderableEventList ForwardEvents;
        public bool autoPlayForward;

        [Space(10)]

        [Tooltip("These events will be run when stepping backwards from the state AFTER this state")]
        [Header("Events to run when reversing this state")]
        public ReorderableEventList BackwardEvents;
        public bool autoPlayBackward;

        [Space(10)]

        private CommonTools MyCommonTools;

        private List<EventDelegate> _cameraWaitingDelegatesList;
        private List<EventDelegateStruct> _delayedDelegatesList;


        private List<IViscoAnimatable> animClips;
        

        [HideInInspector]
        public bool hasAnimations;


        private struct EventDelegateStruct
        {
            public EventDelegateStruct(EventDelegate eDelegate, float startTime)
            {
                Delegate = eDelegate;
                Delay = eDelegate.mdelay;
                StartTime = startTime;
            }
            public EventDelegate Delegate;
            public float Delay;
            public float StartTime;
        }

        [SerializeField]
        private bool _checkingInvokes;

        private void Awake()
        {
            MyCommonTools = gameObject.GetCommonTools();
        }

        private void Start()
        {
            InitializeAnimationClipCache();
        }

        /// <summary>
        /// If a State's child-gameobjects with Animations have changed since program start, call this funciton to update State's internal anim-clip cache.
        /// </summary>
        public void InitializeAnimationClipCache()
        {
            hasAnimations = false;
            animClips = new List<IViscoAnimatable>(transform.childCount);
            foreach (Transform t in transform)
            {
                IViscoAnimatable tmp = t.gameObject.GetComponent<IViscoAnimatable>();
                if (tmp != null)
                {
                    animClips.Add(tmp);
                    hasAnimations = true;
                }
            }
        }


        /// <summary>
        /// Begins executing the state, executing immediate funcatin calls and queing calls that has delay, begins playing any animation clips (or ques them)
        /// </summary>
        public void ExecuteStateForwardEventsWithOptionalDelays()
        {
            Main.Instance.GetMainCameraManager().SetArrivedCameraCallback(ExecuteCameraWaitEvents);

            _cameraWaitingDelegatesList = new List<EventDelegate>();
            _delayedDelegatesList = new List<EventDelegateStruct>();

            foreach (var eventDelegate in ForwardEvents.List)
            {
                if (eventDelegate.mwaitForCamera)
                {
                    _cameraWaitingDelegatesList.Add(eventDelegate);
                }
                else if (eventDelegate.mdelay != 0)
                {
                    _delayedDelegatesList.Add(new EventDelegateStruct(eventDelegate,Time.time));
                }
                else
                {
                    eventDelegate.Execute();
                }
            }

            if (_delayedDelegatesList.Count != 0) _checkingInvokes =true;


            // Start animations, or put them in que if they are to play with delay

            foreach (IViscoAnimatable clip in animClips)
            {
                if (!clip.WaitForCamera)
                {
                    if (clip.DelayBeforePlay == 0)
                    {
                        clip.PlayAnimationClip();
                    }
                    else
                    {
                        StartCoroutine(PlayAnimationWithDelay(clip));
                    }
                }
            }
        }


        /// <summary>
        /// FastForward state - run its event delegates and set animation clips to end position;
        /// </summary>
        public void ExecuteAllStateForwardEventsIgnoreAnyWait()
        {
            foreach (var eventDelegate in ForwardEvents.List)
            {
                 eventDelegate.Execute();
            }

            foreach (IViscoAnimatable clip in animClips)
            {
                clip.SetAnimationClipToEnd();
                clip.ExecuteAllForwardFrameEvents();
            }



        }

        public void ExecuteOnArrivalEventsIgnoreAnyWait()
        {
            foreach (var eventDelegate in OnArrivalEvents.List)
            {
                Debug.Log(gameObject.name + ": Execute OnArrivalEvent " + eventDelegate.mEventName + "\n");
                eventDelegate.Execute();
            }
        }

        /// <summary>
        /// Wait for specified time and then start playing animation clip.
        /// </summary>
        /// <param name="clip"></param>
        /// <returns></returns>
        IEnumerator PlayAnimationWithDelay(IViscoAnimatable clip)
        {
            GameObject goClip = clip.GameObject;
            Debug.Log("Qued animation clip: " + goClip.name + "  (Delayed for " + clip.DelayBeforePlay + "s)\n");
            yield return new WaitForSeconds(clip.DelayBeforePlay);

            // We can only play an animation if we are still at the state to whom the animation belongs. 
            // Because this code snippet sat and waited, the user might have klicked forward further states, in wich 
            // case we can not play this delayed clip.

            State tmpState = goClip.transform.parent.GetComponent<State>();
            
            if (tmpState == Main.Instance.GetStatesManager().currentState)
                clip.PlayAnimationClip();
        }




        public void ExecuteAllStateBackwardsEventsIgnoreAnyWait()
        {
            foreach (var eventDelegate in BackwardEvents.List)
            {
                    eventDelegate.Execute();
            }

            foreach (IViscoAnimatable clip in animClips)
            {
                clip.SetAnimationClipToBegin();
                clip.ExecuteAllBackwardsFrameEvents();
            }
        }




        public void ExecuteStateBackwardsEventsWithOptionalDelays()
        {
            //if (BackwardEvents.List.Count > 0) EventDelegate.Execute(BackwardEvents.List);
            _cameraWaitingDelegatesList = new List<EventDelegate>();
            _delayedDelegatesList = new List<EventDelegateStruct>();
            Main.Instance.GetMainCameraManager().SetArrivedCameraCallback(ExecuteCameraWaitEvents);
            foreach (var eventDelegate in BackwardEvents.List)
            {
                if (eventDelegate.mwaitForCamera)
                {
                    _cameraWaitingDelegatesList.Add(eventDelegate);
                }
                else if (eventDelegate.mdelay != 0)
                {
                    _delayedDelegatesList.Add(new EventDelegateStruct(eventDelegate, Time.time));
                    _checkingInvokes = true;
                }
                else
                {
                    eventDelegate.Execute();
                }
            }
            if (_delayedDelegatesList.Count != 0) _checkingInvokes = true;


            foreach (IViscoAnimatable clip in animClips)
            {
                clip.PlayAnimationClipBackwards();
            }
        }

        /// <summary>
        /// Instantly executes evens in the _cameraWaitingDelegatesList except those that also have a delay
        /// in which case they are added to _delayedDelegatesList
        /// </summary>
        private void ExecuteCameraWaitEvents()
        {
            foreach (var functionCall in _cameraWaitingDelegatesList)
            {
                if (functionCall.mdelay == 0)
                { 
                    functionCall.Execute();
                }
                else
                { 
                    _delayedDelegatesList.Add(new EventDelegateStruct(functionCall,Time.time));
                    _checkingInvokes = true;
                }
            }
            _cameraWaitingDelegatesList = new List<EventDelegate>();
        }
        /// <summary>
        /// Instantly executes all qued-up events on the state (events with delay and/or waitForCam)
        /// </summary>
        public void InstantlyExecuteAllQuedEvents()
        {
            _checkingInvokes = false;
            if (_delayedDelegatesList !=null)
            { 
                foreach (var functionCallStruct in _delayedDelegatesList)
                {
                    functionCallStruct.Delegate.Execute();
                }
                _delayedDelegatesList.Clear();
            }

            if (_cameraWaitingDelegatesList!=null)
            {
                foreach (var functionCall in _cameraWaitingDelegatesList)
                {
                    functionCall.Execute();
                }
                _cameraWaitingDelegatesList.Clear();
            }
            
        }


        public GameObject GetHotSpot() { return MyHotspot; }
        public CommonTools GetCommonTools() { return MyCommonTools; }


        /// <summary>
        /// Loop through and executes any "delay events" that have passed their trigger-time
        /// </summary>
        private void Update()
        {
            if (_checkingInvokes)
            {
                if (_delayedDelegatesList.Count == 0)
                {
                    _checkingInvokes = false;
                    return;
                }
                for (int i = 0; i < _delayedDelegatesList.Count; i++)
                {
                    if ((Time.time - _delayedDelegatesList[i].StartTime) >= _delayedDelegatesList[i].Delay)
                    {
                        Debug.Log("Executing delayed event '" + _delayedDelegatesList[i].Delegate.methodName + "' at time " + Time.time + " Pre-planned executin time was: " + _delayedDelegatesList[i].StartTime + _delayedDelegatesList[i].Delay + "\n");
                        _delayedDelegatesList[i].Delegate.Execute();
                        _delayedDelegatesList.RemoveAt(i);
                        i--;
                    }
                }
            }
        }


        public bool HasActiveAnimation
        {
            get
            {
                foreach (IViscoAnimatable clip in animClips)
                {
                    if (clip.WrapMode != WrapMode.Loop && clip.WrapMode != WrapMode.PingPong &&  clip.IsPlaying())
                    {
                        return true;
                    }
                }
                return false;
            }

        }



        public void SetAnimationClipsTime(float positionInClipToStartAt)
        {
            foreach (Transform child in transform)
            {
                GameObject go = child.gameObject;
                IViscoAnimatable animationClip = go.GetComponent<IViscoAnimatable>();

                if (animationClip != null)
                {
                    animationClip.SetTime = positionInClipToStartAt;
                }
            }
        }

#if UNITY_EDITOR

        //private GUIStyle style;

        public override bool OnInspectorGUI(Editor editor)
        {

            editor.DrawDefaultInspector();

            GUIStyle style;
            style = new GUIStyle(GUI.skin.button);
            style.alignment = TextAnchor.MiddleLeft;



            GUILayout.Space(10);

            EditorGUILayout.LabelField("Quick add...", EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Button List"))
            {
                gameObject.AddComponent<ViscoButtonList>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            if (GUILayout.Button("Description"))
            {
                gameObject.AddComponent<ViscoDescriptionManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Image      "))
            {
                gameObject.AddComponent<ViscoImageManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            if (GUILayout.Button("Video      "))
            {
                gameObject.AddComponent<ViscoVideoManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Side Menu  "))
            {
                gameObject.AddComponent<ViscoSideMenuManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            if (GUILayout.Button("Transparency"))
            {
                gameObject.AddComponent<ViscoTransparencyManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("StateIndex"));
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);


            



            editor.serializedObject.ApplyModifiedProperties();
            return false;

        }

        public void MarkSceneDirty(SerializedProperty pr)
        {
            GameObject go = new GameObject();
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(go.scene);
            DestroyImmediate(go);
        }

#endif
    }
}
