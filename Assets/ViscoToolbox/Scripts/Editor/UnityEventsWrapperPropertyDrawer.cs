﻿using UnityEngine;
using UnityEditor;

namespace Visco
{
    [CustomPropertyDrawer(typeof(UnityEventWrapper))]
    public class UnityEventsWrapperPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            float height = EditorGUIUtility.singleLineHeight;
            float headShift = 5f;
            SerializedProperty UseUnityEventsProp = property.FindPropertyRelative("UseUnityEvents");
            SerializedProperty UnityEventsProp = property.FindPropertyRelative("UnityEvents");
            SerializedProperty WaitForCameraProp = property.FindPropertyRelative("WaitForCamera");
            SerializedProperty DelayProp = property.FindPropertyRelative("Delay");

            EditorGUI.LabelField(new Rect(position.x - 20f, position.y - 10f, position.width + 30, 5), GUIContent.none, GUI.skin.horizontalSlider);
            EditorGUI.TextField(new Rect(position.x + 20f, position.y + headShift, 150f, EditorGUIUtility.singleLineHeight), "Use Unity Events");
            UseUnityEventsProp.boolValue = EditorGUI.Toggle(new Rect(position.x, position.y + headShift, position.width, EditorGUIUtility.singleLineHeight),
                UseUnityEventsProp.boolValue);
            if (UseUnityEventsProp.boolValue)
            {
                EditorGUI.PropertyField(new Rect(position.x, position.y + height + headShift, position.width, EditorGUIUtility.singleLineHeight), WaitForCameraProp);
                EditorGUI.PropertyField(new Rect(position.x, position.y + height * 2F + headShift, position.width, EditorGUI.GetPropertyHeight(DelayProp)), DelayProp);
                EditorGUI.PropertyField(new Rect(position.x, position.y + height * 3f + headShift, position.width, EditorGUI.GetPropertyHeight(UnityEventsProp)), UnityEventsProp);
            }
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (property.FindPropertyRelative("UseUnityEvents").boolValue)
            {
                SerializedProperty UnityEventsProp = property.FindPropertyRelative("UnityEvents");
                return EditorGUIUtility.singleLineHeight * 3f + EditorGUI.GetPropertyHeight(UnityEventsProp) + 10f;
            }
            return EditorGUIUtility.singleLineHeight + 10f; //
        }
    }
}