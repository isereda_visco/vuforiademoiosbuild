﻿using UnityEditor;

namespace Visco
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MonoBehaviourEditorUI), true)]
    public class MonoBehaviourEditorUIEditor : Editor
    {
        #region Overrides of Editor
        public override void OnInspectorGUI()
        {
            MonoBehaviourEditorUI mb = target as MonoBehaviourEditorUI;
            if (mb != null)
            {
                if (mb.OnInspectorGUI(this)) base.OnInspectorGUI();
            }
            else base.OnInspectorGUI();
        }
        #endregion
    }
}