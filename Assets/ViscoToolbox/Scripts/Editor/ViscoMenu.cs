﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

namespace Visco
{
    public static class ViscoMenu
    {
        [MenuItem("GameObject/Visco/State", false, 0)]
        static void CreateStateButton(MenuCommand menuCommand)
        {
            GameObject targetParent;
            if (GameObject.FindObjectOfType<StatesManager>())
            {
                targetParent = GameObject.FindObjectOfType<StatesManager>().gameObject;
            }
            else
            {
                targetParent = menuCommand.context as GameObject;
            }

            GameObject goState = new GameObject("State");
            State state = goState.AddComponent<State>();

            goState.transform.SetParent(targetParent.transform);
            Undo.RegisterCreatedObjectUndo(goState, "Create " + goState.name);

            Selection.activeObject = goState;
        }




        [MenuItem("GameObject/Visco/Hotspot", false, 1)]
        static void CreateHotspotInSceneTree(MenuCommand menuCommand)
        {
            GameObject goHotspot = CommonCreateHotspot("Hotspot");
            //GameObjectUtility.SetParentAndAlign(goHotspot, menuCommand.context as GameObject);
            goHotspot.transform.parent = (menuCommand.context as GameObject).transform;


            
            if (SceneView.currentDrawingSceneView != null)
            {
                //goHotspot.transform.position = SceneView.currentDrawingSceneView.camera.transform.position;
                goHotspot.transform.position = SceneView.currentDrawingSceneView.pivot;
                goHotspot.transform.rotation = SceneView.currentDrawingSceneView.rotation;
            }


            GameObject lookat = goHotspot.transform.GetChild(0).gameObject;
            lookat.transform.position = goHotspot.transform.position + goHotspot.transform.forward * 4f;
            goHotspot.transform.LookAt(lookat.transform);


            Undo.RegisterCreatedObjectUndo(goHotspot, "Create hotspot");
        }

        [MenuItem("GameObject/Visco/---------------------", false, 1)]
        static void Dummy1() { }

        [MenuItem("GameObject/Visco/Text Box", false, 1)]
        static void CreateTextBox(MenuCommand menuCommand)
        {
            var prefab = Resources.Load(Constants.TEXT_BOX_PREFAB_PATH);
            prefab.name = "TextBox";

            var varTextBox = PrefabUtility.InstantiatePrefab(prefab);
            PrefabUtility.DisconnectPrefabInstance(varTextBox);

            GameObjectUtility.SetParentAndAlign(varTextBox as GameObject, menuCommand.context as GameObject);

            Undo.RegisterCreatedObjectUndo(varTextBox, "Create text box");
        }


        [MenuItem("GameObject/Visco/Context Single Button", false, 1)]
        static void CreateContextButton(MenuCommand menuCommand)
        {
            var prefab = Resources.Load(Constants.CONTEXT_BUTTON_SINGLE_PREFAB_PATH);
            prefab.name = "ContextButton";

            var varContextButton = PrefabUtility.InstantiatePrefab(prefab);
            PrefabUtility.DisconnectPrefabInstance(varContextButton);

            GameObjectUtility.SetParentAndAlign(varContextButton as GameObject, menuCommand.context as GameObject);

            Undo.RegisterCreatedObjectUndo(varContextButton, "Create context button");
        }




        [MenuItem("GameObject/Visco/Video Button", false, 1)]
        static void CreateVideoButton(MenuCommand menuCommand)
        {
            var prefab = Resources.Load(Constants.VIDEO_BUTTON_PREFAB_PATH);
            prefab.name = "VideoButton";

            var varVideoButton = PrefabUtility.InstantiatePrefab(prefab);
            PrefabUtility.DisconnectPrefabInstance(varVideoButton);

            GameObjectUtility.SetParentAndAlign(varVideoButton as GameObject, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(varVideoButton, "Create context button");
        }






        [MenuItem("GameObject/Visco/Image Button", false, 1)]
        static void CreateImageButton(MenuCommand menuCommand)
        {
            Debug.Log("Adding image button ");
            var prefab = Resources.Load(Constants.IMAGE_BUTTON_PREFAB_PATH);
            prefab.name = "ImageButton";

            var varImageButton = PrefabUtility.InstantiatePrefab(prefab);
            PrefabUtility.DisconnectPrefabInstance(varImageButton);

            GameObjectUtility.SetParentAndAlign(varImageButton as GameObject, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(varImageButton, "Create context button");

        }



        [MenuItem("GameObject/Visco/Callout Canvas", false, 1)]
        static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            // Create a custom game object
            GameObject goCalloutCanvas = new GameObject("CalloutCanvas");

            Canvas canvas = goCalloutCanvas.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            goCalloutCanvas.AddComponent<GraphicRaycaster>();

            ViscoCalloutManager calloutManager = goCalloutCanvas.AddComponent<ViscoCalloutManager>();

            GameObject calloutOrbitCenter = new GameObject("CalloutOrbitCenter");
            calloutOrbitCenter.transform.SetParent(goCalloutCanvas.transform);
            calloutOrbitCenter.transform.localPosition = new Vector3(0, 0, 0);

            calloutManager.CalloutOrbitCenter = calloutOrbitCenter;

            GameObjectUtility.SetParentAndAlign(goCalloutCanvas, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(goCalloutCanvas, "Create callout canvas");

            Selection.activeObject = goCalloutCanvas;
        }


        [MenuItem("GameObject/Visco/Callout Point (3D position marker)", false, 1)]
        static void CreateCalloutPoint(MenuCommand menuCommand)
        {
            var prefab = Resources.Load(Constants.CALLOUT_POINT_PATH);
            prefab.name = "CalloutPoint";

            var varCalloutPoint = PrefabUtility.InstantiatePrefab(prefab);
            PrefabUtility.DisconnectPrefabInstance(varCalloutPoint);

            GameObjectUtility.SetParentAndAlign(varCalloutPoint as GameObject, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(varCalloutPoint, "Create callout point");
        }


        [MenuItem("GameObject/Visco/---------------------", false, 1)]
        static void Dummy2() { }

        [MenuItem("GameObject/Visco/Basic Scene Objects (no undo!)", false, 1)]
        static void CreateMain()
        {
            //Debug.Log("Adding main ");
            //var prefab = Resources.Load(Contants.MAIN_PREFAB_PATH);
            //prefab.name = "_main";

            //PrefabUtility.InstantiatePrefab(prefab);

            GameObject spacer0 = new GameObject("------------ Model ------------");
            GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObjectUtility.SetParentAndAlign(cube, plane);
            cube.transform.SetPositionAndRotation(new Vector3(0, 0.5f, 0), new Quaternion(33.3f, 90f, 45f, 0));

            GameObject spacer1 = new GameObject("--------------------------------");
            GameObject goStates = new GameObject("States");
            Main main = goStates.AddComponent<Main>();
            StatesManager statesManager = goStates.AddComponent<StatesManager>();


            GameObject goState = new GameObject("State1");
            State state = goState.AddComponent<State>();
            GameObjectUtility.SetParentAndAlign(goState, goStates);


            GameObject goCanvas = GameObject.Find("Canvas");
            if (goCanvas == null)
            {
                GameObject spacer2 = new GameObject("--------------------------------");
                goCanvas = new GameObject("Canvas");
                Canvas canvas = goCanvas.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                CanvasScaler canvasScaler = goCanvas.AddComponent<CanvasScaler>();
                canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                canvasScaler.referenceResolution = new Vector2(1920, 1080);
                goCanvas.AddComponent<GraphicRaycaster>();
            }

            if (!goCanvas.GetComponent<UIMain>())
            {
                goCanvas.AddComponent<UIMain>();
            }


            GameObject goEventSystem = GameObject.Find("EventSystem");
            if (!goEventSystem)
            {
                goEventSystem = new GameObject("EventSystem");
                goEventSystem.AddComponent<EventSystem>();
                goEventSystem.AddComponent<StandaloneInputModule>();
            }

            GameObject spacer3 = new GameObject("--------------------------------");
            GameObject goHotspotRoot = CommonCreateHotspot("HotspotRoot");

            statesManager.rootHotSpotObject = goHotspotRoot;


            GameObject goMainCamera = GameObject.Find("Main Camera");
            if (goMainCamera)
            {
                if (!goMainCamera.GetComponent<MainCameraManager>())
                {
                    goMainCamera.AddComponent<MainCameraManager>();
                }
                Camera cam = goMainCamera.GetComponent<Camera>();
                if (main.mainСamera == null)
                    main.mainСamera = cam;
            }

            state.MyHotspot = goHotspotRoot;
        }


        static GameObject CommonCreateHotspot(string hotspotName)
        {
            GameObject goHotspotRoot = new GameObject(hotspotName);
            Hotspot hotspotManager = goHotspotRoot.AddComponent<Hotspot>();
            goHotspotRoot.transform.localPosition = new Vector3(5, 2, -5);

            GameObject lookAtObject = new GameObject("lookAtObject_" + hotspotName);
            lookAtObject.transform.SetParent(goHotspotRoot.transform);

            hotspotManager.LookAtObject = lookAtObject;
            return goHotspotRoot;
        }

    }

}



