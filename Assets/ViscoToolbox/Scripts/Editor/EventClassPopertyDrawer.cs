﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Visco
{
    [CustomPropertyDrawer(typeof(EventPropertyClass))]
    public class EventClassDrawer : PropertyDrawer
    {

        private int argumentsCount;
        private static int count;
        private int _count;
        public EventClassDrawer() { _count = ++count; }

        private string currentMethodName = "";
        private string[] Methods;
        private List<MethodInfo> methodInfo;
        private ParameterInfo[] parameterInfo;
        private bool GuIChanged = true;
        private int index;
        private Color color = Color.black;
        private string paramsSting;
        private SerializedProperty methodNameSerializedproperty;
        private SerializedProperty valuesStringSerializedProperty;

        private List<object> paramsList = new List<object>();
        private float height;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * (GetArgsumentsCount(property) + 2); //note !! argCount (arguments count)not work propertly here
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            height = EditorGUIUtility.singleLineHeight;

            SerializedProperty gameobjectProperty = property.FindPropertyRelative("targetObject");

            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, height), gameobjectProperty, new GUIContent(_count.ToString()));

            if (gameobjectProperty.objectReferenceValue != null)
            {
                methodNameSerializedproperty = property.FindPropertyRelative("MethodName");
                valuesStringSerializedProperty = property.FindPropertyRelative("valuesString");

                MonoBehaviour[] mn = GetBehaviours(property);
                if (mn == null) return;
                SetMethods(mn);
                string value = methodNameSerializedproperty.stringValue;
                index = -1;

                if (Methods.Length == 0)
                {
                    EditorGUI.EndProperty();
                    return;
                }

                for (int i = 0; i < Methods.Length; i++)
                {
                    if (String.Compare(value, Methods[i], StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        index = i;
                        if (value != currentMethodName) //new method setted
                        {
                            currentMethodName = value;
                            SetParameterinfo(index);
                            DrawAttributesFields(position);
                            GetPropertyHeight(property, label);
                        }
                        break;
                    }
                }

                DrawAttributesFields(position);
                GuIChanged = GUI.changed;
                GUI.changed = false;

                index = EditorGUI.Popup(new Rect(position.x, (position.y + height), position.width, height), index, Methods);
                if (GUI.changed)
                {
                    string prev = methodNameSerializedproperty.stringValue;
                    methodNameSerializedproperty.stringValue = Methods[index];

                    DrawAttributesFields(position);
                    GetPropertyHeight(property, label);
                }
                EditorGUI.indentLevel = indent;
                GUI.changed = GuIChanged;
            }
            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        private int GetArgsumentsCount(SerializedProperty property)
        {
            int c = 0;
            if (index == -1) return c;
            if (methodInfo == null || methodInfo.Count == 0) return c;
            MonoBehaviour[] mono = GetBehaviours(property);
            List<string> methods = new List<string>();
            if (mono == null || mono.Length == 0) return c;
            foreach (MonoBehaviour VARIABLE in mono)
            {
                Type t = VARIABLE.GetType();
                MethodInfo[] mInfo = t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).ToArray();
                methods.AddRange(mInfo.Select(x => x.Name).ToList());
            }
            return methodInfo[index].GetParameters().Length;
        }

        private MonoBehaviour[] GetBehaviours(SerializedProperty property)
        {
            EventPropertyClass myDataClass = new EventPropertyClass();
            myDataClass = PropertyDrawerUtility.GetTargetObjectOfProperty(property) as EventPropertyClass;
            if (myDataClass == null || myDataClass.targetObject == null) return null;

            MonoBehaviour[] monoBehaviour = myDataClass.targetObject.GetComponents<MonoBehaviour>();
            return monoBehaviour;
        }

        private void SetMethods(MonoBehaviour[] monoBehaviour)
        {
            List<string> methods = new List<string>();
            List<MethodInfo> methodInfosList = new List<MethodInfo>();
            foreach (MonoBehaviour VARIABLE in monoBehaviour)
            {
                Type t = VARIABLE.GetType();

                MethodInfo[] mInfo = t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).ToArray();
                methodInfosList.AddRange(mInfo.ToArray());
                methods.AddRange(mInfo.Select(x => x.Name).ToList());

            }
            methodInfo = methodInfosList;
            Methods = methods.ToArray();
        }

        private void SetParameterinfo(int index)
        {
            if (index == -1) return;
            parameterInfo = methodInfo[index].GetParameters();

            if (!string.IsNullOrEmpty(valuesStringSerializedProperty.stringValue))
            {
                // todo need more security with the 
                List<object> list = valuesStringSerializedProperty.stringValue.UnparseArgumentsFromString().ToList();
                if (list.Count == parameterInfo.Length)
                {
                    paramsList = list;
                }
                else
                {
                    paramsList = new List<object>();
                    foreach (var VARIABLE in parameterInfo)
                    {
                        object obj = new object();
                        paramsList.Add(obj);
                    }
                }
            }
            else
            {
                paramsList = new List<object>();
                foreach (var VARIABLE in parameterInfo)
                {
                    object obj = new object();
                    paramsList.Add(obj);
                }
            }
            argumentsCount = parameterInfo.Length;
        }


        private void DrawAttributesFields(Rect position)
        {
            if (parameterInfo == null || parameterInfo.Length == 0) return;
            bool changed = GUI.changed;
            GUI.changed = false;

            for (int i = 0; i < parameterInfo.Length; i++)
            {
                Rect valuePos = new Rect(position.x + position.width * .35f, position.y + (height * (parameterInfo[i].Position + 2)), position.width * .65f,
                    position.height / (argumentsCount + 2));
                Rect namePos = new Rect(position.x, position.y + (height * (parameterInfo[i].Position + 2)), (position.width * .35f) - 10f, height);

                if (parameterInfo[i].ParameterType == typeof(string))
                {
                    string s = paramsList[i].ToString();
                    s = EditorGUI.TextField(valuePos, s);
                    paramsList[i] = s;
                    EditorGUI.TextField(namePos, parameterInfo[i].Name);
                }

                else if (parameterInfo[i].ParameterType == typeof(int))
                {
                    string s = paramsList[i].ToString();
                    int n = 0;
                    int.TryParse(s, out n);
                    paramsList[i] = EditorGUI.IntField(valuePos, n);
                    EditorGUI.TextField(namePos, parameterInfo[i].Name);
                }

                else if (parameterInfo[i].ParameterType == typeof(float))
                {
                    float f = 0;
                    string s = paramsList[i].ToString();
                    float.TryParse(s, out f);
                    paramsList[i] = EditorGUI.FloatField(valuePos, f);
                    EditorGUI.TextField(namePos, parameterInfo[i].Name);
                }
                else
                {
                    //UnityEngine.Object s = new UnityEngine.Object();
                    //paramsList[i] = EditorGUI.ObjectField(valuePos, s, parameterInfo[i].ParameterType, true);
                    EditorGUI.TextField(valuePos, "not implemented");
                    EditorGUI.TextField(namePos, parameterInfo[i].Name);
                }

            }

            if (GUI.changed) //parametes changes
            {
                SetParametersToString();
            }
            GUI.changed = changed;
        }

        void SetParametersToString()
        {
            string s = "";
            for (int i = 0; i < parameterInfo.Length; i++)
            {
                s = Extensions.Pack(s, parameterInfo[i], paramsList[i]);
            }
            paramsSting = s;
            valuesStringSerializedProperty.stringValue = paramsSting;
        }
    }

    public class PropertyDrawerUtility
    {
        public static T GetActualObjectForSerializedProperty<T>(FieldInfo fieldInfo, SerializedProperty property) where T : class
        {
            var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
            if (obj == null)
            {
                return null;
            }

            T actualObject = null;
            if (obj.GetType().IsArray)
            {
                var index = Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
                actualObject = ((T[]) obj)[index];
            }
            else
            {
                actualObject = obj as T;
            }
            return actualObject;
        }

        public static MonoBehaviour[] GetBehaviours(EventPropertyClass eventPropertyClass)
        {
            MonoBehaviour[] monoBehaviours = eventPropertyClass.targetObject.GetComponents<MonoBehaviour>();
            return monoBehaviours;
        }

        public static object GetTargetObjectOfProperty(SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("["));
                    var index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    obj = GetValue_Imp(obj, elementName, index);
                }
                else
                {
                    obj = GetValue_Imp(obj, element);
                }
            }
            return obj;
        }

        private static object GetValue_Imp(object source, string name)
        {
            if (source == null)
                return null;
            var type = source.GetType();

            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                if (f != null)
                    return f.GetValue(source);

                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p != null)
                    return p.GetValue(source, null);

                type = type.BaseType;
            }
            return null;
        }

        private static object GetValue_Imp(object source, string name, int index)
        {
            var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
            if (enumerable == null) return null;
            var enm = enumerable.GetEnumerator();

            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext()) return null;
            }
            return enm.Current;
        }
    }
}