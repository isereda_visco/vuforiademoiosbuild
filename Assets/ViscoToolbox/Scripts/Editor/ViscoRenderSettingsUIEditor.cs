﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Visco
{

    [CustomEditor(typeof(ViscoRenderSettings))]

    class ViscoRenderSettingsComponentInspector : Editor

    {

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Apply To Scene")) ApplyToScene();
            if (GUILayout.Button("Copy From Scene")) CopyFromScene();

            DrawDefaultInspector();
        }

        void ApplyToScene()
        {
            ViscoRenderSettings myTarget = (ViscoRenderSettings) target;


            RenderSettings.ambientEquatorColor = myTarget.ambientEquatorColor;
            RenderSettings.ambientGroundColor = myTarget.ambientGroundColor;
            RenderSettings.ambientIntensity = myTarget.ambientIntensity;
            RenderSettings.ambientLight = myTarget.ambientLight;
            RenderSettings.ambientMode = myTarget.ambientMode;
            RenderSettings.ambientProbe = myTarget.ambientProbe;
            RenderSettings.ambientSkyColor = myTarget.ambientSkyColor;
            RenderSettings.customReflection = myTarget.customReflection;
            RenderSettings.defaultReflectionMode = myTarget.defaultReflectionMode;
            RenderSettings.flareFadeSpeed = myTarget.flareFadeSpeed;
            RenderSettings.flareStrength = myTarget.flareStrength;
            RenderSettings.fog = myTarget.fog;
            RenderSettings.fogColor = myTarget.fogColor;
            RenderSettings.fogDensity = myTarget.fogDensity;
            RenderSettings.fogEndDistance = myTarget.fogEndDistance;
            RenderSettings.fogMode = myTarget.fogMode;
            RenderSettings.fogStartDistance = myTarget.fogStartDistance;
            RenderSettings.haloStrength = myTarget.haloStrength;
            RenderSettings.reflectionBounces = myTarget.reflectionBounes;
            RenderSettings.reflectionIntensity = myTarget.reflectionIntensity;
            RenderSettings.skybox = myTarget.skybox;
            RenderSettings.subtractiveShadowColor = myTarget.subtractiveShadowColor;
            RenderSettings.sun = myTarget.sun;


        }
        void CopyFromScene()
        {
            ViscoRenderSettings myTarget = (ViscoRenderSettings) target;

            myTarget.ambientEquatorColor = RenderSettings.ambientEquatorColor;
            myTarget.ambientGroundColor = RenderSettings.ambientGroundColor;
            myTarget.ambientIntensity = RenderSettings.ambientIntensity;
            myTarget.ambientLight = RenderSettings.ambientLight;
            myTarget.ambientMode = RenderSettings.ambientMode;
            myTarget.ambientProbe = RenderSettings.ambientProbe;
            myTarget.ambientSkyColor = RenderSettings.ambientSkyColor;
            myTarget.customReflection = RenderSettings.customReflection;
            myTarget.defaultReflectionMode = RenderSettings.defaultReflectionMode;
            myTarget.flareFadeSpeed = RenderSettings.flareFadeSpeed;
            myTarget.flareStrength = RenderSettings.flareStrength;
            myTarget.fog = RenderSettings.fog;
            myTarget.fogColor = RenderSettings.fogColor;
            myTarget.fogDensity = RenderSettings.fogDensity;
            myTarget.fogEndDistance = RenderSettings.fogEndDistance;
            myTarget.fogMode = RenderSettings.fogMode;
            myTarget.fogStartDistance = RenderSettings.fogStartDistance;
            myTarget.haloStrength = RenderSettings.haloStrength;
            myTarget.reflectionBounes = RenderSettings.reflectionBounces;
            myTarget.reflectionIntensity = RenderSettings.reflectionIntensity;
            myTarget.skybox = RenderSettings.skybox;
            myTarget.subtractiveShadowColor = RenderSettings.subtractiveShadowColor;
            myTarget.sun = RenderSettings.sun;


        }
    }
}