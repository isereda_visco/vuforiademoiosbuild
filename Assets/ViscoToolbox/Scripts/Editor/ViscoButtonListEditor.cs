﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Visco
{

    [CustomEditor(typeof(ViscoButtonList))]
    public class listExampleInspector2 : Editor
    {
        private ReorderableList reorderableList;
        private float elementSplitter = 35f;

        private ViscoButtonList listExample { get { return target as ViscoButtonList; } }

        ReorderableList CreateList(SerializedObject obj, SerializedProperty prop)
        {
            ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);

            list.elementHeightCallback = (index) =>
            {
                SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
                SerializedProperty b = element.FindPropertyRelative("useSecondaryButtonState");
                float height = EditorGUI.GetPropertyHeight(element.FindPropertyRelative("UnityEvents")) + elementSplitter;
                if (b.boolValue) height += EditorGUI.GetPropertyHeight(element.FindPropertyRelative("SecondaryUnityEvents")) + elementSplitter;
                return height;
            };

            return list;
        }

        private void OnEnable()
        {
            reorderableList = CreateList(serializedObject, serializedObject.FindProperty("ButtonsList"));

            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;

            reorderableList.onAddCallback += AddItem;
            reorderableList.onRemoveCallback += RemoveItem;
        }

        private void OnDisable()
        {
            // Make sure we don't get memory leaks etc.
            reorderableList.drawHeaderCallback -= DrawHeader;
            reorderableList.drawElementCallback -= DrawElement;

            reorderableList.onAddCallback -= AddItem;
            reorderableList.onRemoveCallback -= RemoveItem;
        }

        private void DrawHeader(Rect rect) { GUI.Label(rect, "Reorderable butons list - drag elements to change buttons order"); }

        /// <summary>
        /// Draws one element of the list (ListItemExample)
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="index"></param>
        /// <param name="active"></param>
        /// <param name="focused"></param>
        private void DrawElement(Rect rect, int index, bool active, bool focused)
        {
            float height = 20f;
            ContextButtonsStructure item = listExample.ButtonsList[index];
            SerializedProperty element = serializedObject.FindProperty("ButtonsList").GetArrayElementAtIndex(index);
            SerializedProperty EventsId = element.FindPropertyRelative("ButtonEvent");
            SerializedProperty SecondaryEventsId = element.FindPropertyRelative("SecondaryButtonEvent");
            SerializedProperty UnityEventsProp = element.FindPropertyRelative("UnityEvents");
            SerializedProperty SecondUnityEventsProp = element.FindPropertyRelative("SecondaryUnityEvents");

            // or use bottoms with EditorGUIUtility.isProSkin?
            EditorGUI.LabelField(new Rect(rect.x - 20f, rect.y - 11, rect.width + 25, 5), GUIContent.none, GUI.skin.horizontalSlider);
            EditorGUI.LabelField(new Rect(rect.x - 20f, rect.y - 9, rect.width + 25, 5), GUIContent.none, GUI.skin.horizontalSlider);
            EditorGUI.BeginChangeCheck();
            EditorGUI.LabelField(new Rect(rect.x, rect.y, 100, height), "Button name");
            item.ButtonName = EditorGUI.TextField(new Rect(rect.x, rect.y + height, 100, 20), item.ButtonName);
            EditorGUI.LabelField(new Rect(rect.x + 100, rect.y + height / 2f, 100, height), "Buttons event :");
            EditorGUI.PropertyField(new Rect(rect.x + 250, rect.y + height / 2f, rect.width - 250f, EditorGUI.GetPropertyHeight(EventsId)), EventsId, GUIContent.none);
            EditorGUI.PropertyField(new Rect(rect.x + 100, rect.y + height * 1.5f, rect.width - 100f, EditorGUI.GetPropertyHeight(UnityEventsProp)), UnityEventsProp,
                GUIContent.none);
            EditorGUI.LabelField(new Rect(rect.x, rect.y + height * 2.5f, 100, height * 2f), "use secondary\nbutton name");
            item.useSecondaryButtonState = EditorGUI.Toggle(new Rect(rect.x + 80f, rect.y + height * 3f, height, rect.height), item.useSecondaryButtonState);
            if (item.useSecondaryButtonState)
            {
                Rect sRect = new Rect(new Vector2(rect.position.x, rect.position.y + EditorGUI.GetPropertyHeight(UnityEventsProp) + elementSplitter - 10f),
                    new Vector2(rect.width, EditorGUI.GetPropertyHeight(SecondUnityEventsProp)));
                EditorGUI.LabelField(new Rect(sRect.x + 100, sRect.y + height / 2f, 150, height), "Second Buttons event :");
                EditorGUI.PropertyField(new Rect(sRect.x + 250, sRect.y + height / 2f, sRect.width - 250f, EditorGUI.GetPropertyHeight(SecondaryEventsId)), SecondaryEventsId,
                    GUIContent.none);
                EditorGUI.LabelField(new Rect(sRect.x, sRect.y, 100, height * 1.5f), "Second\nbutton name");
                item.SecondaryButtonName = EditorGUI.TextField(new Rect(sRect.x, sRect.y + height * 1.5f, 100, 20), item.SecondaryButtonName);
                EditorGUI.PropertyField(new Rect(sRect.x + 100, sRect.y + height * 2f, sRect.width - 100f, sRect.height), SecondUnityEventsProp, GUIContent.none);
            }

            if (EditorGUI.EndChangeCheck()) EditorUtility.SetDirty(target);


        }

        private void AddItem(ReorderableList list)
        {
            if (listExample == null)
            {
                Debug.Log("issue");
                return;
            }
            listExample.ButtonsList.Add(new ContextButtonsStructure());
            EditorUtility.SetDirty(target);
        }

        private void RemoveItem(ReorderableList list)
        {
            listExample.ButtonsList.RemoveAt(list.index);
            EditorUtility.SetDirty(target);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            reorderableList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}