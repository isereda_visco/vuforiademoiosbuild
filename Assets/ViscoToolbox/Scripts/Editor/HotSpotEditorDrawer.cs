﻿using UnityEditor;
using UnityEngine;
namespace Visco
{
    [CustomEditor(typeof(Hotspot))]
    public class HotSpotEditor : Editor
    {
        //Optional parameters
        //private bool e_HideSiblingsWhenActive;
        //private bool e_followIfMoving;
        //private bool e_LockPosition;
        //private bool e_LockRotation;

        //private GameObject e_Button;
        //private bool e_UseCameraAnglesLimit;
        //private float e_MinCameraAngle; // min camera angle, to avoid camera go thrue ground
        //private float e_MaxCameraAngle; // max angle
        //private bool e_UseCameraZoomDistanceLimit;
        //private float e_MinZoomDistance; // min dist
        //private float e_MaxZoomDistance; // max dist
        //private bool e_AllowUserInterruptCameraTransition = true;
        //private float e_DistanceToInterruptCameraTransition;
        //private float e_autoOrbitSpeed;
        //private float e_autoOrbitDelay;
        //private bool e_useCameraSettingFromHotSpotCamera;

        //private float e_GizmosRadius = .5f;
        //private Color e_GizmoColor = Color.gray;
        //private bool e_OverrideStateViscoComponents;

        //private bool e_EditorCamTrackLookatObject;
        //private bool e_slaveCameraMode;
        //private float e_cameraFov;

        //private hotspotButtonShowMode e_buttonShowMode;

        //private ReorderableEventList e_cameraStartGoingToActions;
        //private ReorderableEventList e_cameraArriveActions;
        //private ReorderableEventList e_cameraLeaveActions;


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // Rect rect = new Rect(EditorGUILayout.GetControlRect());
            Hotspot t = (Hotspot) target;
            GUI.backgroundColor = Color.gray;


            SerializedProperty e_PressHotSpotEvent = serializedObject.FindProperty("PressHotSpotEvent");
            SerializedProperty e_LookAtObject = serializedObject.FindProperty("LookAtObject");
            SerializedProperty e_CamObject = serializedObject.FindProperty("CamObject");
            
            SerializedProperty e_CustomButtonPosition = serializedObject.FindProperty("CustomButtonPosition");
            SerializedProperty e_State = serializedObject.FindProperty("State");
            SerializedProperty e_buttonShowMode = serializedObject.FindProperty("buttonShowMode");


            //SerializedProperty e_followIfMoving = serializedObject.FindProperty("followIfMoving");
            SerializedProperty e_slaveCameraMode = serializedObject.FindProperty("slaveCameraMode");
            SerializedProperty e_cameraFov = serializedObject.FindProperty("cameraFov");
            
            SerializedProperty e_HideSiblingsWhenActive = serializedObject.FindProperty("HideSiblingsWhenActive");
            SerializedProperty e_LockPosition = serializedObject.FindProperty("LockPosition");
            SerializedProperty e_LockRotation = serializedObject.FindProperty("LockRotation");

            SerializedProperty e_UseCameraAnglesLimit = serializedObject.FindProperty("UseCameraAnglesLimit");
            SerializedProperty e_MinCameraAngle = serializedObject.FindProperty("MinCameraAngle");
            SerializedProperty e_MaxCameraAngle = serializedObject.FindProperty("MaxCameraAngle");
            SerializedProperty e_UseCameraZoomDistanceLimit = serializedObject.FindProperty("UseCameraZoomDistanceLimit");
            SerializedProperty e_MinZoomDistance = serializedObject.FindProperty("MinZoomDistance");
            SerializedProperty e_MaxZoomDistance = serializedObject.FindProperty("MaxZoomDistance");
            SerializedProperty e_AllowUserInterruptCameraTransition = serializedObject.FindProperty("AllowUserInterruptCameraTransition");
            SerializedProperty e_DistanceToInterruptCameraTransition = serializedObject.FindProperty("DistanceToInterruptCameraTransition");
            SerializedProperty e_autoOrbitSpeed = serializedObject.FindProperty("autoOrbitSpeed");
            SerializedProperty e_autoOrbitDelay = serializedObject.FindProperty("autoOrbitDelay");
            SerializedProperty e_useCameraSettingFromHotSpotCamera = serializedObject.FindProperty("useCameraSettingFromHotSpotCamera");

            SerializedProperty e_GizmosRadius = serializedObject.FindProperty("GizmosRadius");
            SerializedProperty e_GizmoColor = serializedObject.FindProperty("GizmoColor");

            SerializedProperty e_OverrideStateViscoComponents = serializedObject.FindProperty("OverrideStateViscoComponents");








            
            SerializedProperty e_lookAtObjectTrackingMode = serializedObject.FindProperty("lookAtObjectTrackingMode");
            SerializedProperty e_buttonPrefab = serializedObject.FindProperty("buttonPrefab");
            SerializedProperty e_hideBackButton = serializedObject.FindProperty("hideBackButton");


            SerializedProperty e_hotspotMoveToMode = serializedObject.FindProperty("hotspotMoveToMode");
            SerializedProperty e_buttonPulseMode = serializedObject.FindProperty("buttonPulseMode");


            SerializedProperty e_customPulse = serializedObject.FindProperty("customPulse");
            SerializedProperty e_pulseMinSize = serializedObject.FindProperty("pulseMinSize"); 
            SerializedProperty e_pulseMaxSize = serializedObject.FindProperty("pulseMaxSize"); 
            SerializedProperty e_pulseSpeed = serializedObject.FindProperty("pulseSpeed");



            SerializedProperty e_cameraStartGoingToActions      = serializedObject.FindProperty("cameraStartGoingToActions");
            SerializedProperty e_cameraArriveActions            = serializedObject.FindProperty("cameraArriveActions");
            SerializedProperty e_cameraLeaveActions             = serializedObject.FindProperty("cameraLeaveActions");
            SerializedProperty e_backButtonActions              = serializedObject.FindProperty("backButtonActions");
            SerializedProperty e_CameraGoToChildActions         = serializedObject.FindProperty("cameraGoToChildActions");
            SerializedProperty e_CameraGoToSiblingActions       = serializedObject.FindProperty("cameraGoToSiblingActions");

            SerializedProperty e_useCameraStartGoingToActions     = serializedObject.FindProperty("useCameraStartGoingToActions");
            SerializedProperty e_useCameraArriveActions           = serializedObject.FindProperty("useCameraArriveActions");
            SerializedProperty e_useCameraLeaveActions            = serializedObject.FindProperty("useCameraLeaveActions");
            SerializedProperty e_useBackButtonActions             = serializedObject.FindProperty("useBackButtonActions");
            SerializedProperty e_useCameraGoToChildActions        = serializedObject.FindProperty("useCameraGoToChildActions");
            SerializedProperty e_useCameraGoToSiblingActions      = serializedObject.FindProperty("useCameraGoToSiblingActions");

            SerializedProperty e_goToHotspotOnClick               = serializedObject.FindProperty("goToHotspotOnClick");


            GUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("❤ Align with view ❤"))
            {
                t.AlignHotSpotToSceneCamera();
            }
            if (GUILayout.Button("Undo Align"))
            {
                t.UndoAlignHotSpotToSceneCameraAction();
            }
            if (GUILayout.Button("Preview"))
            {
                t.AlignViewToThisHotSpot();
            }
            EditorGUILayout.EndHorizontal();
            


            EditorGUILayout.PropertyField(e_PressHotSpotEvent, GUILayout.ExpandWidth(true));

            EditorGUILayout.PropertyField(e_CamObject, GUILayout.ExpandWidth(true));


            // Not using lerp on Camera pos yet.
            //if (e_CamObject.objectReferenceValue != null)
            //{
            //    EditorGUILayout.PropertyField(e_cameraTrackingMode);
            //}



            EditorGUILayout.PropertyField(e_LookAtObject, GUILayout.ExpandWidth(true));
            if (e_LookAtObject.objectReferenceValue != null)
            {
                EditorGUILayout.PropertyField(e_lookAtObjectTrackingMode);
            }

            
            //e_followIfMoving.boolValue = EditorGUILayout.Toggle("Follow LookatObject If Moving", e_followIfMoving.boolValue);
            GUILayout.Space(10);


            // Slave camera mode only possible if both Camera pos and camera lookat target defined.
            if (e_LookAtObject.objectReferenceValue != null && e_CamObject.objectReferenceValue != null)
            {
                e_slaveCameraMode.boolValue = EditorGUILayout.Toggle(Nicify("3DSMax slaveCameraMode"), e_slaveCameraMode.boolValue);
            }
            else
            {
                e_slaveCameraMode.boolValue = false;
            }


            
            EditorGUILayout.PropertyField(e_buttonPrefab);

            // Creates a dropdown box for buttonShowMode
            EditorGUILayout.PropertyField(e_buttonShowMode);


            e_goToHotspotOnClick.boolValue = EditorGUILayout.Toggle("Go to on click", e_goToHotspotOnClick.boolValue);

            EditorGUILayout.PropertyField(e_hotspotMoveToMode);
            

            e_hideBackButton.boolValue = EditorGUILayout.Toggle("Hide Back Button", e_hideBackButton.boolValue);

            e_cameraFov.floatValue = EditorGUILayout.FloatField("Camera FOV", e_cameraFov.floatValue);

            EditorGUILayout.LabelField(GUIContent.none, GUI.skin.horizontalSlider);
            e_HideSiblingsWhenActive.boolValue = EditorGUILayout.Toggle(Nicify("HideSiblingsWhenActive"), e_HideSiblingsWhenActive.boolValue);
            
            e_LockPosition.boolValue = EditorGUILayout.Toggle("Lock cam position", e_LockPosition.boolValue);
            e_LockRotation.boolValue = EditorGUILayout.Toggle("Lock cam rotation", e_LockRotation.boolValue);

            GUILayout.Space(10);
            e_UseCameraAnglesLimit.boolValue = EditorGUILayout.Toggle("Limit cam angles", e_UseCameraAnglesLimit.boolValue);
            if (e_UseCameraAnglesLimit.boolValue)
            {
                e_MinCameraAngle.floatValue = EditorGUILayout.FloatField("Min cam angle", e_MinCameraAngle.floatValue);
                e_MaxCameraAngle.floatValue = EditorGUILayout.FloatField("Max cam angle", e_MaxCameraAngle.floatValue);
            }
            GUILayout.Space(10);
            e_UseCameraZoomDistanceLimit.boolValue = EditorGUILayout.Toggle("Limit distance", e_UseCameraZoomDistanceLimit.boolValue);
            if (e_UseCameraZoomDistanceLimit.boolValue)
            {
                e_MinZoomDistance.floatValue = EditorGUILayout.FloatField("Min distance ", e_MinZoomDistance.floatValue);
                e_MaxZoomDistance.floatValue = EditorGUILayout.FloatField("Max distance ", e_MaxZoomDistance.floatValue);
            }
            GUILayout.Space(10);
            e_AllowUserInterruptCameraTransition.boolValue = EditorGUILayout.Toggle("Allow user abort cam-transition", e_AllowUserInterruptCameraTransition.boolValue);
            e_DistanceToInterruptCameraTransition.floatValue = EditorGUILayout.FloatField("When close than ",e_DistanceToInterruptCameraTransition.floatValue);

            GUILayout.Space(10);
            e_autoOrbitSpeed.floatValue = EditorGUILayout.FloatField("Auto orbit speed", e_autoOrbitSpeed.floatValue);
            e_autoOrbitDelay.floatValue = EditorGUILayout.FloatField("Auto orbit delay", e_autoOrbitDelay.floatValue);
            GUILayout.Space(10);

            e_useCameraSettingFromHotSpotCamera.boolValue = EditorGUILayout.Toggle(Nicify("useCameraSettingFromHotSpotCamera"), e_useCameraSettingFromHotSpotCamera.boolValue);


            
            e_GizmosRadius.floatValue = EditorGUILayout.FloatField(Nicify("GizmosRadius"), e_GizmosRadius.floatValue);
            e_GizmoColor.colorValue = EditorGUILayout.ColorField(Nicify("GizmosColor"), e_GizmoColor.colorValue);
            


            EditorGUILayout.PropertyField(e_State, GUILayout.ExpandWidth(true));
            

            EditorGUILayout.LabelField(GUIContent.none, GUI.skin.horizontalSlider);

           

            e_customPulse.boolValue = EditorGUILayout.Toggle("Custom Pulse", e_customPulse.boolValue);
            if (e_customPulse.boolValue)
            {
                EditorGUILayout.PropertyField(e_buttonPulseMode);
                e_pulseMinSize.floatValue = EditorGUILayout.FloatField("Min size", e_pulseMinSize.floatValue);
                e_pulseMaxSize.floatValue = EditorGUILayout.FloatField("Max size", e_pulseMaxSize.floatValue);
                e_pulseSpeed.floatValue = EditorGUILayout.FloatField("Pulse speed", e_pulseSpeed.floatValue);
            }




                EditorGUILayout.PropertyField(e_CustomButtonPosition, GUILayout.ExpandWidth(true));
            if (GUILayout.Button("  Create custom button pos  ", GUILayout.ExpandWidth(false)))
            {
                t.InitCustomButtonPosition(serializedObject.FindProperty("CustomButtonPosition"));
            }

            EditorGUILayout.LabelField(GUIContent.none, GUI.skin.horizontalSlider);


            EditorGUILayout.BeginVertical("box");

                    EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.BeginVertical("box");
                            e_useCameraStartGoingToActions.boolValue = EditorGUILayout.Toggle("Start going to actions", e_useCameraStartGoingToActions.boolValue);
                            e_useCameraArriveActions.boolValue = EditorGUILayout.Toggle("Arrive at actions", e_useCameraArriveActions.boolValue);
                            e_useCameraLeaveActions.boolValue = EditorGUILayout.Toggle("Leave actions", e_useCameraLeaveActions.boolValue);
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.BeginVertical("box");
                            e_useBackButtonActions.boolValue = EditorGUILayout.Toggle("Back button actions", e_useBackButtonActions.boolValue);
                            e_useCameraGoToChildActions.boolValue = EditorGUILayout.Toggle("Go to child actions", e_useCameraGoToChildActions.boolValue);
                            e_useCameraGoToSiblingActions.boolValue = EditorGUILayout.Toggle("Go to sibling actions", e_useCameraGoToSiblingActions.boolValue);
                        EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();



                    // e_cameraStartGoingToActions
            
                    if (e_useCameraStartGoingToActions.boolValue == true || ListNotEmpty( t.cameraStartGoingToActions))
                    {
                        EditorGUILayout.PropertyField(e_cameraStartGoingToActions, GUILayout.ExpandWidth(true));
                    }

                    // e_cameraArriveActions

            
                    if (e_useCameraArriveActions.boolValue == true || ListNotEmpty(   t.cameraArriveActions))
                    {
                        EditorGUILayout.PropertyField(e_cameraArriveActions, GUILayout.ExpandWidth(true));
                    }

                    // e_cameraLeaveActions
           
                    if (e_useCameraLeaveActions.boolValue == true || ListNotEmpty(t.cameraLeaveActions))
                    {
                        EditorGUILayout.PropertyField(e_cameraLeaveActions, GUILayout.ExpandWidth(true));
                    }

                    // e_backButtonActions
           
                    if (e_useBackButtonActions.boolValue == true || ListNotEmpty(t.backButtonActions))
                    {
                        EditorGUILayout.PropertyField(e_backButtonActions, GUILayout.ExpandWidth(true));
                    }

                    // e_CameraGoToChildActions
                    if (e_useCameraGoToChildActions.boolValue == true || ListNotEmpty( t.cameraGoToChildActions))
                    {
                        EditorGUILayout.PropertyField(e_CameraGoToChildActions, GUILayout.ExpandWidth(true));
                    }

                    // e_CameraGoToSiblingActions
            
                    if (e_useCameraGoToSiblingActions.boolValue == true || ListNotEmpty(t.cameraGoToSiblingActions))
                    {
                        EditorGUILayout.PropertyField(e_CameraGoToSiblingActions, GUILayout.ExpandWidth(true));
                    }

            EditorGUILayout.EndVertical();


            EditorGUILayout.LabelField(GUIContent.none, GUI.skin.horizontalSlider);

                      



            e_OverrideStateViscoComponents.boolValue = EditorGUILayout.Toggle("Override State's components", e_OverrideStateViscoComponents.boolValue);

            EditorGUILayout.LabelField("Quick add...");

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Button List"))
            {
                t.gameObject.AddComponent<ViscoButtonList>();
            }
            if (GUILayout.Button("Description"))
            {
                t.gameObject.AddComponent<ViscoDescriptionManager>();
            }
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Image      "))
            {
                t.gameObject.AddComponent<ViscoImageManager>();
            }
            if (GUILayout.Button("Video      "))
            {
                t.gameObject.AddComponent<ViscoVideoManager>();
            }
            EditorGUILayout.EndVertical();


            EditorGUILayout.BeginVertical("box");
            if (GUILayout.Button("Side Menu  "))
            {
                t.gameObject.AddComponent<ViscoSideMenuManager>();
            }
            if (GUILayout.Button("Transparency"))
            {
                t.gameObject.AddComponent<ViscoTransparencyManager>();
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);


            if (t.backButtonActions.List.Count > 0) e_useBackButtonActions.boolValue = true;

            //e_useBackButtonActions.boolValue            = t.backButtonActions.List.Count > 0;
            //e_useCameraStartGoingToActions.boolValue    = t.cameraStartGoingToActions.List.Count > 0;
            //e_useCameraArriveActions.boolValue          = t.cameraArriveActions.List.Count > 0;
            //e_useBackButtonActions.boolValue            = t.backButtonActions.List.Count > 0;
            //e_useCameraGoToChildActions.boolValue       = t.cameraGoToChildActions.List.Count > 0;
            //e_useCameraGoToSiblingActions.boolValue     = t.cameraGoToSiblingActions.List.Count > 0;


            serializedObject.ApplyModifiedProperties();
            if (GUI.changed)
            {
                t.MarkSceneDirty(serializedObject.FindProperty("ReturnTargetObject"));
            }
        }


        private bool ListNotEmpty(ReorderableEventList l)
        {
            if (l.List != null && l.List.Count > 0)
                return true;
            else
                return false;
        }

        private string Nicify(string name)
        {
            return ObjectNames.NicifyVariableName(name);
        }


    }
}
