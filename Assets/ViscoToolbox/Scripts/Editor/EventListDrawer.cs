﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Visco
{
    [CustomPropertyDrawer(typeof(EventId))]
    public class EventListDrawer : PropertyDrawer
    {
        private static readonly Dictionary<string, EventsList> _eventsList = new Dictionary<string, EventsList>();
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            SerializedProperty id = property.FindPropertyRelative("_id");
            string s = EditorSceneManager.GetActiveScene().name;
            //if (s == null) throw new ArgumentNullException("s");

            if (!_eventsList.ContainsKey(s))
            {

                string findString = "EventsList_" + s + " t:EventsList"; // "назва асету формату EventsList_ІмяСцени"
                string[] guids = AssetDatabase.FindAssets(findString);
                if (guids.Length == 0)
                {
                    EditorGUI.LabelField(position, "No EventsList_" + s + " object in the project");
                    EditorGUI.EndProperty();
                    return;
                }

                _eventsList[s] = AssetDatabase.LoadAssetAtPath<EventsList>(AssetDatabase.GUIDToAssetPath(guids[0]));
            }
            EventsList eList = _eventsList[s];
            string[] list = eList.List;
            if (list == null)
            {
                EditorGUI.LabelField(position, "! No EventsList_" + s + " object in the project !");

                EditorGUI.EndProperty();
                return;
            }
            string value = id.stringValue;
            int index = -1;

            if (list.Length == 0)
            {
                EditorGUI.LabelField(position, "EventsList_" + s + " is empty");
                EditorGUI.EndProperty();
                return;
            }

            for (int i = 0; i < list.Length; i++)
            {
                if (string.Compare(value, list[i], StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    index = i;
                    break;
                }
            }

            bool changed = GUI.changed;
            GUI.changed = false;
            index = EditorGUI.Popup(position, index, list);
            if (GUI.changed) id.stringValue = list[index];
            EditorGUI.indentLevel = indent;
            GUI.changed = changed;

            EditorGUI.EndProperty();
        }
    }

}