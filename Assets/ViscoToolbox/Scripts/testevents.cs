﻿using UnityEngine;

namespace Visco
{
    public class testevents : MonoBehaviour
    {
        public ReorderableEventList OnEnableEvents;

        void OnEnable()
        {
            if (OnEnableEvents.List.Count > 0) EventDelegate.Execute(OnEnableEvents.List);
        }

    }
}
