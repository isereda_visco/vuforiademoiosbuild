﻿using UnityEngine;

namespace Visco
{
#if UNITY_EDITOR
    using UnityEditor;

    public class Main : MonoBehaviourEditorUI
#else
public class Main : MonoBehaviour
#endif
    {
        public static Main Instance;
        private StatesManager _statesManager;
        private MainCameraManager _mainCameraManager;

        public Camera mainСamera;


        private void Awake()
        {
            Debug.Log("Main Awake\n");
            Instance = this;
            if (!mainСamera)
            {
                mainСamera = Camera.main;
                Debug.Log("Warning: No camera assigned to component Main on gameobject " + name + " - using default first active camera\n");
            }
            _mainCameraManager = GetMainCamera().GetComponent<MainCameraManager>();
        }

        public void PointerDonwUI() { _mainCameraManager.SetUIPressed(true); }

        public void PointerUpUI() { _mainCameraManager.SetUIPressed(false); }

        public StatesManager GetStatesManager()
        {
            if (_statesManager == null) _statesManager = GetComponent<StatesManager>();
            if (_statesManager == null)
            {
                throw new System.Exception("There no StatesManager Attached to " + gameObject.name);
            }
            return _statesManager;
        }

        public Camera GetMainCamera()
        {
            if (mainСamera == null)
            {
                throw new System.Exception("There no Camera Attached to " + gameObject.name);
            }
            return mainСamera;
        }

        public MainCameraManager GetMainCameraManager()
        {
            return GetMainCamera().GetComponent<MainCameraManager>();
        }

#if UNITY_EDITOR
        public override bool OnInspectorGUI(Editor editor)
        {

            editor.DrawDefaultInspector();
            editor.DrawPreview(new Rect());
            if (GUILayout.Button("Add Visco Input Manager"))
            {
                gameObject.AddComponent<ViscoInputManager>();
                MarkSceneDirty(editor.serializedObject.FindProperty("mainСamera"));

            }

            if (GUILayout.Button("Add Visco Event Switch"))
            {
                //gameObject.AddComponent<ViscoEventSwitch>();
                MarkSceneDirty(editor.serializedObject.FindProperty("mainСamera"));

            }
            editor.serializedObject.ApplyModifiedProperties();
            return false;

        }

        public void MarkSceneDirty(SerializedProperty pr)
        {
            GameObject go = new GameObject();
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(go.scene);
            DestroyImmediate(go);
        }

#endif
    }
}