﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Visco
{
    public class VideoButtonManager : BaseClickButton, IChangePlace
    {
        [SerializeField] private Image ThumbNailImage;
        private RawImage _rawImage;
        private VideoStruct _myVideoStruct;
        private VideoClip _myVideoClip;
        private VideoPlayer _videoPlayer;

        private bool _isPlaying;
        public bool ShowName;
        public string Name;
        private Text _nameText;

        private Vector2 _startPosition, _centeredPosition, _targetPosition;
        private Vector3 _startScale, _showScale, _targetScale;
        private RectTransform _myRectTransform;
        private RectTransform _CanvasRect;
        [SerializeField] private Transform _myParent;
        public float Speed;
        public float ScaleSpeed;
        public float ScaleDuration;
        private int _siblingIndex;
        private VideoAspectRatio _myAspectRatio;

        private new void Awake()
        {
            base.Awake();
            Init();
        }

        void Init()
        {
            InitNewPosition();

            Speed = Speed != 0 ? Speed : 3f;
            ScaleSpeed = ScaleSpeed != 0 ? ScaleSpeed : 3f;
            ScaleDuration = ScaleDuration != 0 ? ScaleDuration : 0.5f;
        }
        public void ResetVideoStruct() { _myVideoStruct = null; }
        public VideoStruct GetVideoStruct() { return _myVideoStruct; }

        public void InitButton(VideoStruct vStruct, int sibling)
        {
            _myVideoStruct = vStruct;
            _myAspectRatio = vStruct.VidApectRatio;
            if (vStruct.ThumbnailSprite != null) ThumbNailImage.sprite = vStruct.ThumbnailSprite;
            if (vStruct == null)
            {
                throw new System.Exception(name + "vStruct");
            }
            _siblingIndex = sibling;
            _myVideoClip = vStruct.VideoClip;

            ShowName = vStruct.ShowName;
            Name = vStruct.VideoName;
            StartCoroutine(InitVideo());
        }

        public IEnumerator InitVideo()
        {
            if (ShowName)
            {
                _nameText = GetComponentInChildren<Text>();
                _nameText.text = Name;
            }
            // _videoPlayer.playOnAwake = true;
            _videoPlayer.clip = _myVideoClip;

            RenderTexture rt = new RenderTexture(256, 256, 0, RenderTextureFormat.Default);
            _rawImage.texture = rt;
            _videoPlayer.targetTexture = rt;
            ulong length = _myVideoClip.frameCount;
            _videoPlayer.frame = (long) length / 2;
            yield return new WaitForEndOfFrame();
            //  _videoPlayer.Stop();
            _videoPlayer.Pause();
            _videoPlayer.aspectRatio = _myAspectRatio;
        }

        public override void Click()
        {
            _isPlaying = !_isPlaying;
            _targetPosition = _isPlaying ? _centeredPosition : _startPosition;
            _targetScale = _isPlaying ? _showScale : _startScale;

            if (_isPlaying)
            {
                _myRectTransform.SetParent(_CanvasRect);
                _startPosition = _myRectTransform.position;
                _myRectTransform.SetAsLastSibling();
                if (ShowName && _nameText != null) _nameText.gameObject.SetActive(false);



                _myRectTransform.DOMove(_targetPosition, ScaleDuration);
                _myRectTransform.DOScale(_targetScale, ScaleDuration).OnComplete(PlayVideo);
            }
            else
            {
                StartCoroutine(StopVideo());
            }
        }


        void PlayVideo()
        {
            if (ThumbNailImage != null) ThumbNailImage.gameObject.SetActive(false);
            _videoPlayer.Play();
            // _videoPlayer.aspectRatio = VideoAspectRatio.FitHorizontally;
        }

        IEnumerator StopVideo()
        {
            _videoPlayer.Stop();
            yield return new WaitForEndOfFrame();
            if (ThumbNailImage != null) ThumbNailImage.gameObject.SetActive(true);
            if (ShowName && _nameText != null)
            {
                _nameText.gameObject.SetActive(true);
            }
            _myRectTransform.DOMove(_targetPosition, ScaleDuration).OnComplete(EndMove);
            _myRectTransform.DOScale(_targetScale, ScaleDuration);
        }
        void EndMove()
        {
            _myRectTransform.SetParent(_myParent);
            _myRectTransform.SetSiblingIndex(_siblingIndex);
        }

        public void InitNewPosition()
        {
            if (_videoPlayer == null) _videoPlayer = GetComponent<VideoPlayer>();
            if (_rawImage == null) _rawImage = GetComponent<RawImage>();
            if (_myRectTransform == null) _myRectTransform = GetComponent<RectTransform>();
            if (_CanvasRect == null) _CanvasRect = UIMain.Instance.GetMainCanvas().GetComponent<RectTransform>();
            _myParent = _myRectTransform.parent;
            _startPosition = _myRectTransform.position;
            _centeredPosition = _CanvasRect.TransformPoint(Vector3.zero); // Vector2.zero;

            _targetPosition = _centeredPosition;
            _startScale = _myRectTransform.localScale;
            _targetScale = _startScale;
            _showScale = new Vector3(UIMain.Instance.GetCanvasScale().x / _myRectTransform.sizeDelta.x, UIMain.Instance.GetCanvasScale().y / _myRectTransform.sizeDelta.y);
        }
    }
}