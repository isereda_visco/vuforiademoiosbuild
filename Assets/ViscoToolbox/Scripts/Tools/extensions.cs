﻿using System.Collections.Generic;

namespace Visco
{

    public static class Extensions
    {
        public static char PARAM_SEPARATOR = '|'; // Forexzmple |param1_value_of_param1|param2_value_of_param2

        public static char VALUE_SEPARATOR = '_';

        private static string _STRING = "string";
        private static string _FLOAT = "float";
        private static string _INT = "int";


        public static string Pack(string str, System.Reflection.ParameterInfo parameterInfo, object value)
        {
            string paramTypeName = "";

            if (parameterInfo.ParameterType == typeof(int)) paramTypeName = _INT;
            else if (parameterInfo.ParameterType == typeof(float)) paramTypeName = _FLOAT;
            else if (parameterInfo.ParameterType == typeof(string)) paramTypeName = _STRING;

            string s = str + paramTypeName + VALUE_SEPARATOR + value + PARAM_SEPARATOR;
            return s;
        }

        public static string PackFloat(string str, float value)
        {
            string s = str + _FLOAT + VALUE_SEPARATOR + value + PARAM_SEPARATOR;
            return s;
        }

        public static string PackInt(string str, float value)
        {
            string s = str + _FLOAT + VALUE_SEPARATOR + value + PARAM_SEPARATOR;
            return s;
        }

        public static string PackString(string str, string value) { return str + _STRING + VALUE_SEPARATOR + value + PARAM_SEPARATOR; }

        public static object[] UnparseArgumentsFromString(this string s)
        {
            string[] stringsArray = s.Split(PARAM_SEPARATOR);
            List<object> obj = new List<object>();
            for (int i = 0; i < stringsArray.Length - 1; i++)
            {
                string[] ss = stringsArray[i].Split(VALUE_SEPARATOR);
                if (ss[0] == _STRING)
                {
                    string param = "";
                    param = ss[1];
                    obj.Add(param);
                }
                else if (ss[0] == _INT)
                {
                    int param = 0;
                    int.TryParse(ss[1], out param);
                    obj.Add(param);
                }
                else if (ss[0] == _FLOAT)
                {
                    float param = 0;
                    float.TryParse(ss[1], out param);
                    obj.Add(param);
                }
            }
            return obj.ToArray();
        }
    }

}