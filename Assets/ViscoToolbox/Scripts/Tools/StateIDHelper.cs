﻿using System;
using System.Linq;

namespace Visco
{
    public static class StateIDHelper
    {
        public static bool Contains_EventID(this EventId[] eventIds, EventId id)
        {
            return eventIds.Any(t => string.Compare(id.Value, t.Value, StringComparison.InvariantCultureIgnoreCase) == 0);
        }
    }
}