﻿using UnityEngine;

namespace Visco
{
    public static class ViscoToolsHelper
    {
        /// <summary>
        /// create newcommon tools class, getting components from current gameobject
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static CommonTools GetCommonTools(this GameObject gameObject)
        {
            return new CommonTools(gameObject.GetComponent<ViscoButtonList>(), gameObject.GetComponent<ViscoDescriptionManager>(),
                gameObject.GetComponent<ViscoVideoManager>(), gameObject.GetComponent<ViscoImageManager>(), gameObject.GetComponent<ViscoSideMenuManager>());
        }
    }
}
