﻿using UnityEngine;
using UnityEngine.Events;

namespace Visco
{

    public class MainCameraManager : MonoBehaviour
    {
        private Transform prevTarget;

        private float distance = 5.0f;

        public float xSpeed = 1.0f;  // Multipliers for input-sensitivity.
        public float ySpeed = 1.0f;
        public float zSpeed = 1.0f;

        public float smoothTime = 2f;  // Affects camera rotation-speed fade out. Should be a value in seconds

        private float y_Rotation;   // The cameras current rotation angle around LookAtObject.
        private float x_Rotation;

        private float velocityX;  // The momentum of the cameras rotation
        private float velocityY;
        private float velocityZoom;

        private bool _uiPressed;

        private float newTargetDistance;
        private GameObject transitionDesiredCamera;
        private GameObject navigationDesiredCamera;


        // For transitions
        public float TransitionTime = 1f;  // In seconds.

        private Vector3 transitionVelocity = Vector3.zero;  // Stored the camera velocity during hotspot-to-hotspot transitions.

        bool isMovingToCameraTargetPos;   // Flag. Reset at SetNewTarget, when arrived at target then flipped.

        [HideInInspector]
        public GameObject cameraGhostTarget; // This is the object we point the camera at. Then we let this object smoothly move between viewpoints

        [Header("For debugging only")]
        public Hotspot hotspot;


        private float timeSinceUserInput;
        private bool cameraHasArrivedAtHotspot;

        private UnityAction _arrivedAtHotspotCallbackAction;
        private Camera mainCamera;


        public bool CameraHasArrivedAtHotspot
        {
            get { return cameraHasArrivedAtHotspot; }
        }

        public Hotspot Hotspot
        {
            get { return hotspot; }
        }

        public void SetArrivedCameraCallback(UnityAction arrivedAction)
        {
            _arrivedAtHotspotCallbackAction = arrivedAction;
        }

        private void InvokeArrivedAction()
        {
            Debug.Log("Cam arrived at " + hotspot.name + "\n");

            // Notify StatesManager camera is arrived.
            Main.Instance.GetStatesManager().CameraArrivedAtViewpoint(hotspot.gameObject);

            // Set local flag
            cameraHasArrivedAtHotspot = true;

            // Call registered delegates:
            // If the state that requested Cam to go to the Hotspot has actions/events registered on it that have the flag "Wait for cam",
            // then those actions are called here.

            if (_arrivedAtHotspotCallbackAction != null)
            {
                _arrivedAtHotspotCallbackAction();  // Where do we jump to from here?
            }
            _arrivedAtHotspotCallbackAction = null;
        }

        /// <summary>
        /// stops camera inputs when ui is pressed
        /// </summary>
        public void SetUIPressed(bool uiPressed)
        {
            _uiPressed = uiPressed;
        }


        void Start()
        {
            mainCamera = Main.Instance.GetMainCamera();

            transitionDesiredCamera = new GameObject();
            transitionDesiredCamera.name = "";
            navigationDesiredCamera = new GameObject();
            navigationDesiredCamera.name = "navigationDesiredCamera";

            cameraGhostTarget = new GameObject { name = "_CameraTracksThisObject" };  // Create a 'ghost' object that the camera tracks. This object we let sliden from one hotspot-lookat-obj to another.
            UpdateOrbitParamsFromCameraPos();
            timeSinceUserInput = Time.time;
        }


        private void UpdateOrbitParamsFromCameraPos()
        {
            distance = Vector3.Distance(cameraGhostTarget.transform.position, transform.position);
            Vector3 angles = transform.eulerAngles;
            y_Rotation = angles.y;
            x_Rotation = angles.x;
        }


        void LateUpdate()
        {
            if (!hotspot) return; // Before we have gotten a hotspot assigned
            
            // Lerp camera ic to target hotspots fov if the hotspot has a fov assigned
            if (hotspot.cameraFov > 0 && Mathf.Abs(mainCamera.fieldOfView - hotspot.cameraFov) > 0.001)
            {
                mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView, hotspot.cameraFov, Time.deltaTime / TransitionTime);
            }

            if (isMovingToCameraTargetPos)
                ManageCameraTransition();
            else
                ManageUserNavigation();

        }



        private void ManageCameraTransition()
        {


            // Update the cameras position
            transform.position = Vector3.SmoothDamp(transform.position, hotspot.CamObject.transform.position, ref transitionVelocity, TransitionTime);
            //transitionDesiredCamera.transform.position  = Vector3.SmoothDamp(transform.position, hotspot.CamObject.transform.position, ref transitionVelocity, TransitionTime);

            // Move cameras ghost-target.
            cameraGhostTarget.transform.position = Vector3.Lerp(cameraGhostTarget.transform.position, hotspot.LookAtObject.transform.position, Time.deltaTime / TransitionTime);

            // Align camera    
            transform.LookAt(cameraGhostTarget.transform.position);
            //transitionDesiredCamera.transform.LookAt(cameraGhostTarget.transform.position);

            float remainingDistance = Vector3.Distance(transform.position, hotspot.CamObject.transform.position);
            if (remainingDistance < 0.2f) // Have we arrived yet?
            {
                isMovingToCameraTargetPos = false;
                InvokeArrivedAction(); // new callback action here
            }

            UpdateOrbitParamsFromCameraPos();
        }

        private void ManageUserNavigation()
        {

            // This camera/hotspot is exported from 3DSMax
            // There are 2 objects in max - lookAtObject and Camera(pos).
            if (hotspot.slaveCameraMode)
            {
                transform.position = hotspot.CamObject.transform.position;
                transform.LookAt(hotspot.LookAtObject.transform.position);
                return;
            }



            // In case LookAtObject has moved - move the cameras Ghost-LookatObject too.
            switch (hotspot.lookAtObjectTrackingMode)
            {
                case TrackingMode.Instant:
                    cameraGhostTarget.transform.position = hotspot.LookAtObject.transform.position;
                    break;
                case TrackingMode.Lerp:
                    cameraGhostTarget.transform.position = Vector3.Lerp(cameraGhostTarget.transform.position, hotspot.LookAtObject.transform.position, Time.deltaTime / TransitionTime);
                    break;
                case TrackingMode.SmoothDamp: // TODO: Implement proper SmoothDamp
                    cameraGhostTarget.transform.position = Vector3.Lerp(cameraGhostTarget.transform.position, hotspot.LookAtObject.transform.position, Time.deltaTime / TransitionTime);
                    break;
                default:
                    break;
            }



            if (hotspot.LockPosition && hotspot.LockRotation)
                return;


            // Handle mouse/touc imput
            if (Input.touchCount > 0)
            {
                timeSinceUserInput = Time.time;
                if (Input.touchCount == 2)
                {
                    // Store both touches.
                    Touch touchZero = Input.GetTouch(0);
                    Touch touchOne = Input.GetTouch(1);

                    // Find the position in the previous frame of each touch.
                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    // Find the difference in the distances between each frame.
                    float deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag) / 100;

                    velocityZoom += deltaMagnitudeDiff / 2f;
                }

                if (Input.touchCount == 1)
                {
                    if (!isMovingToCameraTargetPos)
                    {
                        Touch t = Input.GetTouch(0);

                        velocityX += t.deltaPosition.x / 20;
                        velocityY += t.deltaPosition.y / 20;
                    }

                    // The case of the eager user who wants to freenavigate before camtransition is finished.
                    CheckForUserInterruptionOfCamTransition(); // Let user skip last bit of transition if they want
                }
            }
            else // Now handle mouse input instead
            {

                velocityZoom -= zSpeed * Input.GetAxis("Mouse ScrollWheel");

                if (Input.GetMouseButton(0))
                {
                    timeSinceUserInput = Time.time;
                    if (!isMovingToCameraTargetPos && !_uiPressed)
                    {
                        velocityX += xSpeed * Input.GetAxis("Mouse X"); // * 0.01f;
                        velocityY += ySpeed * Input.GetAxis("Mouse Y"); // * 0.01f;
                    }

                    // The case of the eager user who wants to free navigate before camtransition is finished.
                    CheckForUserInterruptionOfCamTransition(); // Let user skip last bit of transition if they want
                }
            }


            // Clamp zoom velocoity;
            velocityZoom = Mathf.Clamp(velocityZoom, -3.5f, 3.5f);


            if (!isMovingToCameraTargetPos)
            {
                distance += velocityZoom;
            }


            y_Rotation += velocityX;
            x_Rotation -= velocityY;



            // Clamp cameras distance from lookAtObject to within range specified by hotspot.
            if (hotspot.UseCameraZoomDistanceLimit)
            {
                distance = Mathf.Clamp(distance, hotspot.MinZoomDistance, hotspot.MaxZoomDistance);
            }

            if (hotspot.UseCameraAnglesLimit)
            {
                x_Rotation = WrapAngle(x_Rotation, hotspot.MinCameraAngle, hotspot.MaxCameraAngle);
            }

            Quaternion rotation = Quaternion.Euler(x_Rotation, y_Rotation, 0);
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);


            //------------ Final camera position and rotation
            transform.position = cameraGhostTarget.transform.position + rotation * negDistance;
            transform.rotation = rotation;




            // Handle auto-orbit. If no user-unput for specified time, then orbit
            // otherwise, just fade all camera rotation velocity to zero.
            if (hotspot.autoOrbitSpeed != 0 && Time.time > (timeSinceUserInput + hotspot.autoOrbitDelay))
            {
                velocityX = Mathf.Lerp(velocityX, hotspot.autoOrbitSpeed, Time.deltaTime * 0.05f);
            }
            else
            {
                velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
            }
            velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
            velocityZoom = Mathf.Lerp(velocityZoom, 0, Time.deltaTime * smoothTime);
        }



        private void CheckForUserInterruptionOfCamTransition()
        {
            if (isMovingToCameraTargetPos)
            {
                if (hotspot.AllowUserInterruptCameraTransition)
                {
                    float remainingCameraTravelDistance = Vector3.Distance(hotspot.CamObject.transform.position, transform.position);
                    if (remainingCameraTravelDistance < hotspot.DistanceToInterruptCameraTransition || hotspot.DistanceToInterruptCameraTransition == 0)
                    {
                        Debug.Log("User-interrupted camera transition\n");
                        UpdateOrbitParamsFromCameraPos();
                        isMovingToCameraTargetPos = false;
                        StatesManager statesManager = Main.Instance.GetStatesManager();
                        statesManager.CameraArrivedAtViewpoint(hotspot.gameObject);
                        InvokeArrivedAction();
                    }
                }
            }
        }

        /// <summary>
        /// Sets a new viewpoint target for Main Camera. Input parameter: Camera gameobject decorated with initialized CameraLookatScript, 
        /// </summary>

        public void SetNewTarget(Hotspot h, bool teleportMode)
        {
            Debug.Log("CameraManager: Go to hotspot '" + h.name + "'\n");

            // Execute previous hotspots CameraLeaveActions...
            if (hotspot)
                hotspot.ExecuteCameraLeaveActions();

            hotspot = h;


            newTargetDistance = Vector3.Distance(hotspot.CamObject.transform.position, hotspot.LookAtObject.transform.position);


            // Execute new target hotspot actions.
            hotspot.ExecuteCameraStartGoingToActions();

            // Reset last-user-input time.
            timeSinceUserInput = Time.time;


            if (prevTarget == null)
            {
                cameraGhostTarget.transform.position = hotspot.LookAtObject.transform.position;
            }

            
            cameraHasArrivedAtHotspot = false;

            prevTarget = hotspot.gameObject.transform;


            if (teleportMode)
            {
                transform.position = hotspot.CamObject.transform.position;
                cameraGhostTarget.transform.position = hotspot.LookAtObject.transform.position;

                transform.LookAt(hotspot.LookAtObject.transform.position);
                cameraHasArrivedAtHotspot = true;
                UpdateOrbitParamsFromCameraPos();
            }
            else
            {
                isMovingToCameraTargetPos = true;
            }
            
        }


        private float WrapAngle(float angle, float min, float max)
        {
            if (angle > 180f) angle -= 360f;
            angle = Mathf.Clamp(angle, min, max);
            return angle;
        }
    }
}