﻿using UnityEngine;

namespace Visco
{
    public abstract class MonoBehaviourEditorUI : MonoBehaviour
    {
#if UNITY_EDITOR

        /// Override this if you need custom inspector UI for MonoBehaviour
        ///<returns>Returns To draw original UI or not</returns>
        public virtual bool OnInspectorGUI(UnityEditor.Editor editor)
        {
            return true;
        }
#endif
    }
}