﻿using UnityEngine;
using UnityEngine.Events;
namespace Visco
{

    public class ViscoCameraPositionEventTrigger : MonoBehaviour
    {

        public float CameraTriggerYpos;

        public bool TriggerAtProgramStart;
        private bool startTriggerExecuted;

        public UnityEvent OverEvents;
        public UnityEvent UnderEvents;


        private Vector3 prevPos;



        void Start()
        {
            prevPos = new Vector3();
            startTriggerExecuted = false;
            prevPos = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (TriggerAtProgramStart && !startTriggerExecuted)
            {
                if (transform.position.y >= CameraTriggerYpos)
                    OverEvents.Invoke();
                else
                    UnderEvents.Invoke();
                startTriggerExecuted = true;
            }


            if (prevPos.y < CameraTriggerYpos && transform.position.y >= CameraTriggerYpos)
            {
                OverEvents.Invoke();
            }

            if (prevPos.y >= CameraTriggerYpos && transform.position.y < CameraTriggerYpos)
            {
                UnderEvents.Invoke();
            }
            prevPos = transform.position;

        }




        public bool RenderSettings_Fog
        {
            get
            {
                return RenderSettings.fog;
                ;
            }
            set { RenderSettings.fog = value; }
        }


        //Passes color in 0xRRGGBBAA format
        public int RenderSettings_FogColor { get { return Color32ToInt(RenderSettings.fogColor); } set { RenderSettings.fogColor = IntToColor32(value); } }


        public float RenderSettings_FogDensity { get { return RenderSettings.fogDensity; } set { RenderSettings.fogDensity = value; } }




        public float RenderSettings_fogEndDistance { get { return RenderSettings.fogEndDistance; } set { RenderSettings.fogEndDistance = value; } }



        public float RenderSettings_fogStartDistance { get { return RenderSettings.fogStartDistance; } set { RenderSettings.fogStartDistance = value; } }

        public void RenderSettings_fogMode_Exponential() { RenderSettings.fogMode = FogMode.Exponential; }

        public void RenderSettings_fogMode_Linear() { RenderSettings.fogMode = FogMode.Linear; }

        public void RenderSettings_fogMode_ExponentialSquared() { RenderSettings.fogMode = FogMode.ExponentialSquared; }






        public int RenderSettings_ambientLight { get { return Color32ToInt(RenderSettings.ambientLight); } set { RenderSettings.ambientLight = IntToColor32(value); } }





        private Color32 IntToColor32(int col)
        {
            byte r = (byte) ((col >> 24) & 0xff);
            byte g = (byte) ((col >> 16) & 0xff);
            byte b = (byte) ((col >> 8) & 0xff);
            byte a = (byte) (col & 0xff);
            return new Color32(r, g, b, a);
        }

        private int Color32ToInt(Color32 c)
        {
            int col = (int) c.r << 24 + (int) c.g << 16 + (int) c.b << 8 + (int) c.a;
            return col;
        }

    }
}
