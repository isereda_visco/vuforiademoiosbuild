﻿using UnityEngine;

namespace Visco
{
    public class TransparencyManager : MonoBehaviour
    {
        [SerializeField] public EventId[] SetTransparencyEvents;
        public EventId[] SetOpaqueEvents;
        [Tooltip("If defined transparent materials not defined, they will be copied with setted  'Transparency Level'")] public Material[] DefinedTransparentMaterials;
        private Material[] OpaqueMaterials;

        [Tooltip("Set Transparency")] [Range(0f, 1f)] public float TransparencyLevel = 0.5f;
        private MeshRenderer _meshRenderer;

        private void Awake()
        {
            _meshRenderer = GetComponent<MeshRenderer>();

            OpaqueMaterials = new Material[_meshRenderer.sharedMaterials.Length];

            for (int i = 0; i < OpaqueMaterials.Length; i++)
            {
                OpaqueMaterials[i] = new Material(_meshRenderer.materials[i]);
            }

            if (DefinedTransparentMaterials == null || DefinedTransparentMaterials.Length == 0)
            {
                DefinedTransparentMaterials = new Material[_meshRenderer.materials.Length];

                for (int i = 0; i < _meshRenderer.sharedMaterials.Length; i++)
                {
                    Color col = _meshRenderer.sharedMaterials[i].color;
                    DefinedTransparentMaterials[i] = new Material(_meshRenderer.sharedMaterials[i]) {color = new Color(col.r, col.g, col.b, TransparencyLevel),};
                }
            }
        }

        /// <summary>
        /// nint !!!
        /// </summary>
        /// <param name="transparency"></param>
        public void ApplyCustomTransparency(float transparency)
        {
            DefinedTransparentMaterials = new Material[_meshRenderer.materials.Length];

            for (int i = 0; i < _meshRenderer.sharedMaterials.Length; i++)
            {
                Color col = _meshRenderer.sharedMaterials[i].color;
                DefinedTransparentMaterials[i] = new Material(_meshRenderer.sharedMaterials[i]) {color = new Color(col.r, col.g, col.b, transparency),};
            }
            SetTransparency(true);
        }

        private void OnEnable() { StatesManager.SharedSpotAction += SharedActionActivated; }

        private void OnDisable() { StatesManager.SharedSpotAction -= SharedActionActivated; }

        private void SharedActionActivated(EventId @event)
        {
            if (SetOpaqueEvents != null && SetOpaqueEvents.Contains_EventID(@event)) SetTransparency(false);
            else if (SetTransparencyEvents != null && SetTransparencyEvents.Contains_EventID(@event)) SetTransparency(true);
        }

        private void SetTransparency(bool transparent)
        {
            Material[] mat = _meshRenderer.sharedMaterials;
            for (int i = 0; i < _meshRenderer.sharedMaterials.Length; i++)
            {
                mat[i] = transparent ? DefinedTransparentMaterials[i] : OpaqueMaterials[i];
            }
            _meshRenderer.sharedMaterials = mat;
        }


        public void ResetState() { }
    }
}