﻿using System.Collections.Generic;

namespace Visco
{

    public class SimpleReorderableList { }

    public class ReorderableList<T> : SimpleReorderableList
    {
        public List<T> List;
    }

    [System.Serializable]
    public class ReorderableEventList : ReorderableList<EventDelegate> { }
}