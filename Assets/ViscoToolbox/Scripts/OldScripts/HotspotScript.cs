﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Visco
{
    public class HotspotScript : MonoBehaviour
    {
        public GameObject LookAtObject;
        public GameObject OptionalButton2dLocation;
        public GameObject Button2d;


        private float pulseStartTime;
        private StateScript stateScript;
        private bool isHidden = false;

        public bool IsHidden
        {
            get { return isHidden; }
            set
            {
                isHidden = value;
                if (isHidden && Button2d != null)
                    Button2d.transform.localScale = new Vector3(0, 0, 0);
            }
        }




        // Use this for initialization
        void Start()
        {

            Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
            if (canvas == null)
            {
                //return; 
                throw new System.Exception("Error: Can not find gameObject 'Canvas' that hotspot buttons need to be moved to");
            }


            GameObject mainScriptHolder = GameObject.Find("MainScriptHolder");
            if (mainScriptHolder == null)
            {
                throw new System.Exception("Error - can not find MainScripHolder");
            }
            stateScript = mainScriptHolder.GetComponent<StateScript>();


            if (LookAtObject == null)
            {
                throw new System.Exception("Error - LookAtObject not set on hotspot/viewpoint");
            }

            pulseStartTime = Time.time;

            if (Button2d == null)
            {
                GameObject prefab = (GameObject) Instantiate(Resources.Load("HotspotPrefab"), canvas.transform);
                prefab.name = this.name + "_AutoGen2dButton";
                Button2d = prefab;
                Button tmpButton = prefab.GetComponent<Button>();
                if (tmpButton == null)
                    throw new System.Exception("Error - prefab has no button component");

                // Add callback for when the button is clicked.
                EventTrigger.Entry entry = new EventTrigger.Entry();
                entry.eventID = EventTriggerType.PointerClick;
                entry.callback = new EventTrigger.TriggerEvent();
                entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(HotspotButton_OnClick));
                tmpButton.GetComponent<EventTrigger>().triggers.Add(entry);
                //isHidden = false;
            }
            else
            {
                Button2d.transform.SetParent(canvas.transform, true);
            }
            //isHidden = true;

        }



        // Update is called once per frame
        void Update()
        {
            if (Button2d == null)
                return;

            if (!isHidden)
            {
                // Hotspots are 2d UI elements - convert 3d space to 2d space for X,Y
                float size = (Mathf.Sin(Time.time - pulseStartTime) + 3f);
                Button2d.transform.position = Camera.main.WorldToScreenPoint(LookAtObject.transform.position);
                Button2d.transform.localScale = new Vector3(size, size, size);
            }
        }


        public void HotspotButton_OnClick(BaseEventData eventData)
        {
            Debug.Log("Hotspot clicked: " + name + " which has " + this.transform.childCount.ToString() + " children");

            ViscoTransparencyManager ts = GetComponent<ViscoTransparencyManager>();
            if (ts)
            {
                ts.FadeToTransparrentIfOpaque();
            }

            stateScript.SetActiveHotspot(this.gameObject);
        }


        public void HotspotButton_OnPointerEnter(BaseEventData evntData) { Debug.Log("HotspotButton_OnPointerEnter()"); }


        public void HotspotButton_OnPointerExit(PointerEventData evntData)
        {
            //throw new System.Exception("on exit");
            Debug.Log("HotspotButton_OnPointerExit()");
        }

    }
}