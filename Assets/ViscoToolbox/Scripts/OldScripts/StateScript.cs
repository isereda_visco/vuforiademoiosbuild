﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Visco
{
    public class StateScript : MonoBehaviour
    {
        public GameObject cam1;
        public GameObject cam2;
        public GameObject cam3;
        public GameObject cam4;
        public GameObject cam5;
        //public GameObject cam6;
        //public GameObject cam7;
        //public GameObject cam8;


        public GameObject mainCamera;
        public GameObject statusText;
        public GameObject hotspotRootNode;

        public GameObject Battery_Cap;
        public GameObject IsolatePartsScripHolder;
        public GameObject IsolateALL_exceptBattery;


        public delegate void StateSpecificCodeDelegate(); // Trivial delegate for custom functin that each state can call


        public class SceneState
        {
            public GameObject CameraTemplate { get; set; }
            public string Description { get; set; }
            public float AnimationStart { get; set; }
            public float AnimationEnd { get; set; }
            public bool BatteryCapTransparrent { get; set; }
            public bool IsolateOneRack { get; set; }
            public StateSpecificCodeDelegate CustomFunction { get; set; }

            public SceneState(GameObject cameraTemplate, string description, float animationStart, float animationEnd, bool batteryCapTransparrent, bool isolateOneRack,
                              StateSpecificCodeDelegate customFunction = null)
            {
                CameraTemplate = cameraTemplate;
                Description = description;
                AnimationStart = animationStart;
                AnimationEnd = animationEnd;
                CustomFunction = customFunction;
                BatteryCapTransparrent = batteryCapTransparrent;
                IsolateOneRack = isolateOneRack;
            }

            public void RunStateCustomFunction()
            {
                if (CustomFunction != null)
                {
                    CustomFunction();
                }
            }
        }



        // private Animation animation;
        private List<SceneState> States = new List<SceneState>();
        private SceneState currentState;
        private float AnimationNextStopTime;
        private GameObject currentHotspot;



        // Use this for initialization
        void Start()
        {

            //  animation = SiemensModel.GetComponent<Animation>();
            //  AnimationNextStopTime = animation["Take 001"].length;

            Debug.Log("Anim length = " + AnimationNextStopTime.ToString());

            // Starting position
            States.Add(new SceneState(cam1, "Overview", 0, 0, false, false, StartStateCustomFunction));

            // Go closer and open door
            States.Add(new SceneState(cam2, "1. Hotspot 1", 0, 2, false, false));

            States.Add(new SceneState(cam3, "2. Hotspot 2", 0, 2, false, false));

            States.Add(new SceneState(cam4, "3. Hotspot 3", 0, 2, false, false));

            States.Add(new SceneState(cam5, "4. Hotspot 4", 0, 2, false, false));



            //// Isolate the middle rack (fade out all others)
            //States.Add(new SceneState(cam3, "3. Single rack",2,2,false, true));

            ////  Slide out battery
            //States.Add(new SceneState(cam3, "4. Battery slide out", 2, 5, false , true ));

            //// Show battery electronics. 
            //States.Add(new SceneState(cam3, "5. Battery electronics", 5, 5, true, true, State5CustomFunction));

            //// Show battery connectors
            //States.Add(new SceneState(cam5, "6. Battery connectors backside", 5, 4.02f, true, true, State6CustomFunction)); // notice the negative animation range here - we play backwards a bit


            //States.Add(new SceneState(cam6, "7. Battery connectors backside", 4.02f, 3.02f, true, true)); // notice the negative animation range here - we play backwards a bit


            //// Show backside with flow
            //States.Add(new SceneState(cam4, "8. Flow", 3.02f, 0f, true, true));


            // Pipes underneath
            //States.Add(new SceneState(cam7, "9. Flow", 0f, 0f, false, false));



            ApplyState(States[0]);

            Debug.Log("Setting inital hotspot visibility status");
            SetHotspotVisibilityRecursive(hotspotRootNode, false, 9); // First hide ALL hotspots
            SetHotspotVisibilityRecursive(hotspotRootNode, true, 1); // then SHOW only 1-st level children of root node
        }




        // Update is called once per frame
        // The only thing we do here is check how far the animatinon has run and stop it if it is getting to the next AnimationNextStopTime

        void Update()
        {

            //if (animation["Take 001"].speed > 0.00001)
            //{
            //    if (animation["Take 001"].time > AnimationNextStopTime)
            //    {
            //        animation["Take 001"].speed = 0;
            //    }
            //}
            //else if (animation["Take 001"].speed < -0.00001)
            //{
            //    if (animation["Take 001"].time < AnimationNextStopTime)
            //    {
            //        animation["Take 001"].speed = 0;
            //    }
            //}

        }






        public void GoNextState()
        {
            int curIdx = States.IndexOf(currentState);
            SceneState nextState = States[(curIdx + 1) % States.Count];
            ApplyState(nextState);
        }


        public void GoPrevState()
        {
            int curIdx = States.IndexOf(currentState);
            SceneState prevState = States[(curIdx + States.Count - 1) % States.Count];
            ApplyState(prevState);
        }



        // Set up scene according to selected state
        // Possibly calling the states custom function
        private void ApplyState(SceneState state)
        {
            string txt = "Apply state " + state.Description + "\nHotspot name = " + state.CameraTemplate.name;
            Debug.Log(txt);
            currentState = state;



            // Set camera transition and orbit object etc.
            var MouseRotationScript = mainCamera.GetComponent<MouseRotationScript>();




            Debug.Log("State script: Set cam target pos: " + state.CameraTemplate.transform.position);
            MouseRotationScript.SetNewTarget(state.CameraTemplate);



            // Set status text
            statusText.GetComponentInChildren<Text>().text = state.Description;


            // Hide/Show battery
            //TransparencyScript batteryScript = Battery_Cap.GetComponent<TransparencyScript>();
            //if (state.BatteryCapTransparrent)
            //    batteryScript.FadeToTransparrentIfOpaque();
            //else 
            //    batteryScript.FadeToOpaqueIfTransparrent();


            // Hide/show 'Other' racks
            //TransparencyScript IsolateOneRackScript = IsolatePartsScripHolder.GetComponent<TransparencyScript>();
            //if (state.IsolateOneRack)
            //    IsolateOneRackScript.FadeToTransparrentIfOpaque();
            //else
            //    IsolateOneRackScript.FadeToOpaqueIfTransparrent();



            // Start animation? 
            // There is only one state that has backwards-playing animation, so we tailor the reverse speed for that state.
            //animation["Take 001"].time = state.AnimationStart;
            //AnimationNextStopTime = state.AnimationEnd;

            //if (state.AnimationStart == state.AnimationEnd)
            //{
            //    animation["Take 001"].speed = 0;
            //}
            //else
            //{
            //    if (state.AnimationEnd < state.AnimationStart)
            //        animation["Take 001"].speed = -0.25f;
            //    else
            //        animation["Take 001"].speed = 1;

            //    animation.Play(PlayMode.StopAll);
            //}

            //state.RunStateCustomFunction();
        }





        // When a hotspot is clicked on it calls this function (passing as argument itself)

        public void HotspotBackbutton()
        {
            if (currentHotspot != hotspotRootNode)
            {
                SetActiveHotspot(currentHotspot.transform.parent.gameObject);
            }
        }

        public void SetActiveHotspot(GameObject activeHotspot)
        {
            Debug.Log("Setting hotspot: " + activeHotspot.name);


            // Hide all hotspots, then re-show just the current children
            SetHotspotVisibilityRecursive(hotspotRootNode, false);
            SetHotspotVisibilityRecursive(activeHotspot, true, 1);

            // And hide the active one - the one we clicked on
            activeHotspot.GetComponent<HotspotScript>().IsHidden = true;


            // Send signal to MouseRotationScript to do camera transition to the clicked hotspot.
            Camera main = Camera.main;
            MouseRotationScript script = main.gameObject.GetComponent<MouseRotationScript>();
            script.SetNewTarget(activeHotspot);


            // Show optional button bar for the clicked hotspot.
            ActionButtons scriptActionButton;
            if (currentHotspot)
            {
                // Hide the previous hotspots Action Buttons
                scriptActionButton = currentHotspot.GetComponent<ActionButtons>();
                if (scriptActionButton)
                    scriptActionButton.SetVisible(false);
            }

            // Show CURRENT hotspot Action Buttons
            scriptActionButton = activeHotspot.GetComponent<ActionButtons>();
            if (scriptActionButton)
                scriptActionButton.SetVisible(true);


            currentHotspot = activeHotspot;
        }


        // Utility function used to set visibility states of all hotspots recursively.
        private void SetHotspotVisibilityRecursive(GameObject node, bool Visible, int depth = 9)
        {
            if (node != hotspotRootNode)
            {
                if (node.GetComponent<HotspotScript>().IsHidden == Visible)
                {
                    Debug.Log("Setting hotspot " + node.name + ".isHidden=" + (!Visible).ToString() + "\n");
                    node.GetComponent<HotspotScript>().IsHidden = !Visible;
                }
            }

            if (depth > 0)
            {
                foreach (Transform child in node.transform)
                {
                    HotspotScript script2 = child.GetComponent<HotspotScript>();
                    if (script2 != null)
                    {
                        SetHotspotVisibilityRecursive(child.gameObject, Visible, depth - 1);
                    }
                }

            }
        }



        //-------------------------   Custom functions for states.


        private void StartStateCustomFunction()
        {
            Debug.Log("Start state custom function. Reset animations");
            //animation.Rewind();
            //animation["Take 001"].speed = 0;
            //animation.Play(PlayMode.StopAll);
        }

        private void State2CustomFunction()
        {
            Debug.Log("State2 custom function - start nimation");
            //animation["Take 001"].speed = 1;
            //animation.Play("Take 001");
        }

        private void State5CustomFunction()
        {
            Debug.Log("State5 custom function ");

            // TransparencyScript script = IsolateALL_exceptBattery.GetComponent<TransparencyScript>();
            // script.FadeToTransparrentIfOpaque();

            // Comented out this functio because it requires a bit of extra logic - some pipes are semi-transparent already at import.
            // It they are to be fasded out/i completely, we need to store the original transparaency valuyes somewhere so we can get it back.


        }
        private void State6CustomFunction()
        {
            Debug.Log("State5 custom function ");

            //TransparencyScript script = IsolateALL_exceptBattery.GetComponent<TransparencyScript>();
            //script.FadeToOpaqueIfTransparrent();
        }

    }
}









