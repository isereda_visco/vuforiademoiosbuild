﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Visco
{

    public class MouseRotationScript : MonoBehaviour
    {
        public Transform target; // The object that the camera looks at in orbit mode
        Transform ViewpointCamPosTarget; // The position that the camera should move to
        Transform ViewpointLookatPos; // The position the camera shall look at and orbit around

        public GameObject statusText;

        public float distance = 5.0f;
        public float xSpeed = 1.0f;
        public float ySpeed = 1.0f;
        public float zSpeed = 2.0f;

        public float yMinLimit = -20f;
        public float yMaxLimit = 80f;

        public float distanceMin = .5f;
        public float distanceMax = 15f;

        public float smoothTime = 2f;

        float rotationYAxis = 0.0f;
        float rotationXAxis = 0.0f;

        float velocityX = 0.0f;
        float velocityY = 0.0f;
        float velocityZoom = 0.0f;

        // For transitions
        public float TransitionTime = 1f;
        private Vector3 transitionVelocity = Vector3.zero;



        bool ViewpointTransitionActive = false;



        GameObject CameraLookatObject; // This is the object we point the camera at. Then we let this object smoothly move between viewpoints

        // Use this for initialization
        void Start()
        {
            Vector3 angles = transform.eulerAngles;

            Debug.Log("Initial cam:" + transform.position.ToString());
            rotationYAxis = angles.y;
            rotationXAxis = angles.x;


            if (ViewpointCamPosTarget == null)
            {
                ViewpointCamPosTarget = new GameObject().transform;
            }

            if (ViewpointLookatPos == null)
            {
                ViewpointLookatPos = new GameObject().transform;
            }


            if (CameraLookatObject == null)
            {
                CameraLookatObject = new GameObject();
            }
            CameraLookatObject.transform.position = target.transform.position;
            target = CameraLookatObject.transform;

            Debug.Log("MouseRotation scrip Init READY");
        }


        void LateUpdate()
        {
            // We handle two things here free camera orbiting AND  A -> B transitions

            float remainingDistance = Vector3.Distance(transform.position, ViewpointCamPosTarget.position);


            if (ViewpointTransitionActive)
            {
                // Set position of the camera itself
                transform.position = Vector3.SmoothDamp(transform.position, ViewpointCamPosTarget.position, ref transitionVelocity, TransitionTime);
                //Debug.Log("Moving camera to pos:" + ViewpointCamPosTarget.position);
                // Move the invisible camera-lookat target from A to B
                CameraLookatObject.transform.position = Vector3.Lerp(CameraLookatObject.transform.position, ViewpointLookatPos.position, Time.deltaTime);

                // Smootly rotate the camera towards the invisible camera-lookat target (rather than snapping it to the object)
                //GameObject go = new GameObject();
                //go.transform.position = transform.position;
                //go.transform.LookAt(CameraLookatObject.transform.position);
                //transform.rotation = Quaternion.Lerp(transform.rotation, go.transform.rotation, Time.deltaTime * 2);
                //Destroy(go);

                transform.LookAt(CameraLookatObject.transform.position);

                if (remainingDistance < .1f) // Have we arrived yet?
                {
                    ViewpointTransitionActive = false;
                    distance = Vector3.Distance(CameraLookatObject.transform.position, transform.position);
                }

                Vector3 angles = transform.eulerAngles;
                rotationYAxis = angles.y;
                rotationXAxis = angles.x;

            }


            // Camera orbit
            if (target) // && !ViewpointTransitionActive)   // Free navigatin mode
            {

                if (Input.touchCount > 0)
                {
                    if (Input.touchCount == 2)
                    {
                        // Store both touches.
                        Touch touchZero = Input.GetTouch(0);
                        Touch touchOne = Input.GetTouch(1);

                        // Find the position in the previous frame of each touch.
                        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                        // Find the magnitude of the vector (the distance) between the touches in each frame.
                        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                        // Find the difference in the distances between each frame.
                        float deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag) / 100;

                        velocityZoom += deltaMagnitudeDiff / 2f;
                    }

                    if (Input.touchCount == 1)
                    {

                        Touch t = Input.GetTouch(0);

                        velocityX += t.deltaPosition.x / 20;
                        velocityY += t.deltaPosition.y / 20;

                        // The case of the eager user who wants to freenavigate before camtransition is finished.
                        if (remainingDistance < 20.2f) // Let user skip last bit of transition if they want
                        {
                            distance = Vector3.Distance(CameraLookatObject.transform.position, transform.position);
                            ViewpointTransitionActive = false;
                        }


                        //statusText.GetComponentInChildren<Text>().text = t.position.ToString() + " - " + t.deltaPosition.ToString();

                    }
                }
                else // Now handle mouse input instead
                {

                    velocityZoom -= zSpeed * Input.GetAxis("Mouse ScrollWheel");

                    if (Input.GetMouseButton(0))
                    {
                        velocityX += xSpeed * Input.GetAxis("Mouse X"); // * 0.01f;
                        velocityY += ySpeed * Input.GetAxis("Mouse Y"); // * 0.01f;

                        // The case of the eager user who wants to free navigate before camtransition is finished.
                        if (remainingDistance < 20.5f) // Let user skip last bit of transition if they want
                        {
                            distance = Vector3.Distance(CameraLookatObject.transform.position, transform.position);
                            ViewpointTransitionActive = false;
                        }

                    }

                }


                if (velocityZoom > 3.5)
                    velocityZoom = 3.5f;

                if (velocityZoom < -3.5)
                    velocityZoom = -3.5f;


                rotationYAxis += velocityX;
                rotationXAxis -= velocityY;

                distance += velocityZoom;

                if (distance < distanceMin)
                {
                    distance = distanceMin;
                }

                if (distance > distanceMax)
                {
                    distance = distanceMax;
                }


                rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);


                Quaternion rotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);

                Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);

                if (!ViewpointTransitionActive)
                {
                    Vector3 position = CameraLookatObject.transform.position + rotation * negDistance;
                    transform.position = position;
                }
                transform.rotation = rotation;


                velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
                velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
                velocityZoom = Mathf.Lerp(velocityZoom, 0, Time.deltaTime * smoothTime);

            }

        }



        private string prevText = "";
        private void PrintOnce(string text)
        {
            if (text != prevText)
            {
                Debug.Log(text);
                prevText = text;
            }
        }

        /// <summary>
        /// Sets a new viewpoint target for Main Camera. Input parameter: Camera gamobject decorated with initialized CameraLookatScript, 
        /// </summary>
        /// <param name="cameraTemplate"></param>
        public void SetNewTarget(GameObject cameraTemplate)
        {
            if (ViewpointCamPosTarget == null)
            {
                Start();
            }

            GameObject NewLookatObject = cameraTemplate.GetComponent<HotspotScript>().LookAtObject;
            Vector3 lookatPos = NewLookatObject.transform.position;

            transitionVelocity = Vector3.zero;

            ViewpointCamPosTarget.position = cameraTemplate.transform.position;
            ViewpointCamPosTarget.rotation = cameraTemplate.transform.rotation;

            ViewpointLookatPos.position = lookatPos;

            target = cameraTemplate.GetComponent<HotspotScript>().LookAtObject.transform;


            ViewpointTransitionActive = true;
        }
        public void SetNewTarget(GameObject cameraTemplate, GameObject lookat)
        {
            if (ViewpointCamPosTarget == null)
            {
                Start();
            }

            GameObject NewLookatObject = lookat;
            Vector3 lookatPos = NewLookatObject.transform.position;

            transitionVelocity = Vector3.zero;

            ViewpointCamPosTarget.position = cameraTemplate.transform.position;
            ViewpointCamPosTarget.rotation = cameraTemplate.transform.rotation;

            ViewpointLookatPos.position = lookatPos;

            target = lookat.transform;


            ViewpointTransitionActive = true;
        }

        public void PrintCurrentCamera()
        {

            // Debug.Log("pos:" + transform.position + " and rot: " + transform.rotation);


        }

        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F)
                angle += 360F;
            if (angle > 360F)
                angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

        public void Ping() { Debug.Log("Ping!"); }
    }

}
