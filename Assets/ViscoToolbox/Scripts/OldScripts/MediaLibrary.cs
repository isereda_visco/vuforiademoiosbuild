﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visco
{
    public class MediaLibrary : MonoBehaviour
    {

        public string FolderName;
        public bool AutoScanFolder = false;

        public List<MediaFile> MediaFiles;

        [System.Serializable]
        public class MediaFile
        {
            public string Filename;
            public string IconFile;
        }


        // Use this for initialization
        void Start() { }

        // Update is called once per frame
        void Update() { }
    }

}