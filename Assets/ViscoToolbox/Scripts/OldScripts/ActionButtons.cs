﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Visco
{
    public class ActionButtons : MonoBehaviour
    {


        //public bool ShowOpenButton;
        //public GameObject[] OpenButton_targets;

        //public bool ShowIsolateButton;
        //public string IsolateSceneName;

        //public bool ShowFlowButton;
        //public GameObject[] FlowButton_targets;

        //public bool ShowCompareButton;
        //public GameObject CompareButton_target;


        [System.Serializable]
        public enum ButtonType
        {
            OpenClose,
            ShowHide,
            Flow,
            Isolate,
            Compare
        }


        [System.Serializable]
        public class BarButton
        {
            public bool ShowText = true;
            public string ButtonText;
            public GameObject[] targets;
            public ButtonType Type;
            [Tooltip("Optional custom button template (Should be a prefab in Resources folder)")] public GameObject ButtonTemplate;
        }


        [Tooltip("Optional custom positioned/designed panel to hold the buttons.")] public GameObject CustomButtonHolderPanel;

        public BarButton[] Buttons;

        private GameObject container;

        // Use this for initialization



        /// <summary>
        /// Changes the vis of the whole container with child buttons
        /// </summary>
        /// <param name="visibility"></param>
        public void SetVisible(bool visibility)
        {
            container.SetActive(visibility);
        }

        void Start()
        {

            Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
            if (canvas == null)
            {
                //return; 
                throw new System.Exception("Error: ActionButtons: Can not find gameObject 'Canvas' that buttons need to be moved to");
            }


            float startPos = (float) (-(((double) Buttons.Length - 0.5) * 155.0 / 2.0));
            GameObject panelButtonBar = GameObject.Find("PanelButtonBar");

            if (CustomButtonHolderPanel == null)
                container = (GameObject) Instantiate(panelButtonBar, panelButtonBar.transform);
            else
                container = CustomButtonHolderPanel;




            foreach (BarButton b in Buttons)
            {
                GameObject prefab;
                if (b.ButtonTemplate == null)
                    prefab = (GameObject) Instantiate(Resources.Load("BarButton"), container.transform);
                else
                    prefab = (GameObject) Instantiate(b.ButtonTemplate, container.transform);


                Vector3 pos = prefab.transform.position;
                pos.x += startPos;
                startPos += 155;
                prefab.transform.position = pos;


                prefab.name = "BarButton_" + b.Type.ToString();
                Button tmpButton = prefab.GetComponent<Button>();

                if (b.ShowText)
                    prefab.GetComponentInChildren<Text>().text = b.ButtonText;
                else
                    prefab.GetComponentInChildren<Text>().gameObject.SetActive(false);

                //EventTrigger.Entry entry = new EventTrigger.Entry();
                //entry.eventID = EventTriggerType.PointerClick;
                //entry.callback = new EventTrigger.TriggerEvent();
                //entry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(ButtonClicked));
                //tmpButton.GetComponent<EventTrigger>().triggers.Add(entry);

                tmpButton.onClick.AddListener(() => { ButtonClicked(b); });

            }

            // Todo - make it fit content nicely
            //container.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTranform.Axis.Vertical, myHeight);
            container.SetActive(false);
        }

        // Update is called once per frame
        void Update() { }

        //public void ButtonClicked(BaseEventData eventData)
        //{
        //    Debug.Log("Button clicked");
        //}


        public void ButtonClicked(BarButton button)
        {
            Debug.Log(button.Type.ToString() + " button with text label " + button.ButtonText + " clicked\n");



            if (button.Type == ButtonType.ShowHide)
            {
                GameObject g = new GameObject();
                g.AddComponent<ViscoTransparencyManager>();
                ViscoTransparencyManager s = g.GetComponent<ViscoTransparencyManager>();
                s.FadeParts.AddRange(button.targets);
                s.FadeToTransparrentIfOpaque();
            }


            if (button.Type == ButtonType.Isolate)
            {
                // Open isolate scene.


            }


        }

    }
}