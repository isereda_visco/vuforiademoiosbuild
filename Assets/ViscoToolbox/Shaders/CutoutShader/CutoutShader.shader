﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Reveal" {
    Properties {
        _MainTex ("Base (RGB) Reveal (A)", 2D) = "white" {}
		_backdrop ("Backdrop (always visible)", 2D) = "gray" {}
        _Cutoff ("Cutoff", Range(-0.5, 0.55)) = 0
		_Cutoff2 ("Cutoff2", Range(0.0, 1.0)) = 0
    }
    SubShader {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 100
        Alphatest Greater 0
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
       
        Pass {
           
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"
               
                struct v2f {
                    float4 pos : SV_POSITION;
                    float2 uv_MainTex : TEXCOORD0;
					float2 uv_Backdrop: TEXCOORD1;
					float test: TEXCOORD2;
                };
               
                float4 _MainTex_ST;
                float4 _backdrop_ST;

                v2f vert(appdata_base v) {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv_MainTex = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }
               
                sampler2D _MainTex;
				sampler2D _backdrop;
                fixed _Cutoff;
				fixed _Cutoff2;
               
                float4 frag(v2f IN) : COLOR {
                    half4 c = half4(1.0, 0.0, 0.0, 1.0); 
					
					c = tex2D (_MainTex, IN.uv_MainTex  );
					
					if (IN.uv_MainTex.x > _Cutoff2)
					{
						 c = tex2D (_backdrop,  IN.uv_MainTex);
					}
                    return c;

                }
            ENDCG
        }
    }
}