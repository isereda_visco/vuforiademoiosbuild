﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace Visco {
    [Serializable]
    public class ScrubberTimelinePart
    {
        public GameObject goAnimClip;
        public bool isLoopClip;

        [HideInInspector]
        public float accumulatedLenghts;    // The accumulated length of all the clips before this clip.

        [HideInInspector]
        public AnimationClip clip;          // USed to probe Animation object if the specific clip is playing
    }

    

    public class ViscoAnimationProgressBar : MonoBehaviour,  IPointerClickHandler
    {

        public Material progressBarMaterial;

        public new Animation animation;

        //public GameObject[] AnimationClips;
        public List<ScrubberTimelinePart> ScrubberTimelineParts;


        // To be hidden - these 2 are shown now for development purposes only
        [HideInInspector]
        public float totalLengthAllClips;
        
        
        private AnimationClip theClip;
        private RectTransform pbRect;
        private Texture2D tex1, tex2;
        // Use this for initialization
        void Start()
        {
            totalLengthAllClips = 0;
            //for (int i = 0; i < ScrubberTimelineParts.Count; i++)
            foreach (ScrubberTimelinePart p in ScrubberTimelineParts)
            {
                p.clip = p.goAnimClip.GetComponent<IViscoAnimatable>().AnimationClip; // Cache the animation clip
                p.accumulatedLenghts = totalLengthAllClips;
                if (!p.isLoopClip)
                {
                    totalLengthAllClips += p.clip.length;
                }
            }


            // Programmatically build the graphical animatin timeline
            pbRect = GetComponent<RectTransform>();
            int width = 1024;
            int height = 60;

            tex1 = new Texture2D(width, height, TextureFormat.ARGB32, false);
            tex2 = new Texture2D(width, height, TextureFormat.ARGB32, false);

            // First just the horizontal line
            for (int x = 0; x < width; x++)
            {
                tex1.SetPixel(x, height / 2 -4, Color.gray);
                tex1.SetPixel(x, height / 2 -5, Color.gray);
                tex1.SetPixel(x, height / 2 +4 , Color.gray);
                tex1.SetPixel(x, height / 2 + 5, Color.gray);

                tex2.SetPixel(x, height / 2, Color.white);
                tex2.SetPixel(x, height / 2 - 1, Color.white);
                tex2.SetPixel(x, height / 2 + 1, Color.white);
                tex2.SetPixel(x, height / 2 - 2, Color.white);
                tex2.SetPixel(x, height / 2 + 2, Color.white);
                tex2.SetPixel(x, height / 2 - 3, Color.white);
                tex2.SetPixel(x, height / 2 + 3, Color.white);

            }

            // Then the vertical marks - the "busstops"
            for (int count = 0; count < ScrubberTimelineParts.Count; count++)
            {
                if (ScrubberTimelineParts[count].isLoopClip)
                {
                    float dist = ScrubberTimelineParts[count].accumulatedLenghts;
                    float x = (dist / totalLengthAllClips) * (float)width; 
                    for (int y = 0; y < height; y++)
                    {
                        tex1.SetPixel((int)x, y, Color.gray);
                        tex1.SetPixel((int)x+1, y, Color.gray);
                        tex1.SetPixel((int)x-1, y, Color.gray);

                        tex2.SetPixel((int)x, y, Color.white);
                        tex2.SetPixel((int)x + 1, y, Color.white);
                        tex2.SetPixel((int)x - 1, y, Color.white);
                    }
                }
            }


            // DEBUG MARKER - red dot at each clip
            for (int count = 0; count < ScrubberTimelineParts.Count; count++)
            {
                float dist = ScrubberTimelineParts[count].accumulatedLenghts;
                float x = (dist / totalLengthAllClips) * (float)width;
                tex1.SetPixel((int)x, 1, Color.red);
                tex1.SetPixel((int)x, 2, Color.red);
                tex1.SetPixel((int)x, 3, Color.red);
                tex1.SetPixel((int)x, 4, Color.red);

                tex2.SetPixel((int)x, 1, Color.blue);
                tex2.SetPixel((int)x, 2, Color.blue);
                tex2.SetPixel((int)x, 3, Color.blue);
                tex2.SetPixel((int)x, 4, Color.blue);
            }

            tex1.Apply();
            tex2.Apply();
            

            //GetComponent<Renderer>().material.mainTexture = tex1;

            //Sprite sprite1 = Sprite.Create(tex1, new Rect(0, 0, tex1.width, tex1.height), new Vector2(0.5f, 0.5f), width);



            //GetComponent<Image>().overrideSprite = sprite1;

            progressBarMaterial.SetTexture("_backdrop", tex1);
            progressBarMaterial.SetTexture("_MainTex", tex2);
            progressBarMaterial.SetFloat("_Cutoff2", 0);


        }

        // Update is called once per frame
        void Update()
        {
            ScrubberTimelinePart currentPlayingPart =  null;

            // Find currently playing clip index;
            foreach (ScrubberTimelinePart p in ScrubberTimelineParts)
            {
                if (animation.IsPlaying(p.clip.name))
                {
                    currentPlayingPart = p;
                    break;
                }
            }

            if (currentPlayingPart == null)
                return;


            // Find accumulated animation time up to current point
            float accTime = currentPlayingPart.accumulatedLenghts;  // accumulated lengths of all the previous clips 

            if (currentPlayingPart.isLoopClip == false)  // and if we are currenlty playing a non-loop clip, then add on the time-progress within that clip too, to the progresbar 
                accTime += animation[currentPlayingPart.clip.name].time;  


            float progressPercent = accTime / totalLengthAllClips;

            progressBarMaterial.SetFloat("_Cutoff2", progressPercent);
           // Debug.Log("Anim: " + theClip.name + "     t=" + animation[theClip.name].time + "\n" );
        }



        public void OnPointerClick(PointerEventData eventData)
        {

            Vector2 c = eventData.pressPosition;

            Vector2 localPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(pbRect, c, null, out localPos);

            float leftToClickDelta = localPos.x - pbRect.rect.xMin;
            float totalWidth = pbRect.rect.xMax - pbRect.rect.xMin;
            float clickedRatio = leftToClickDelta / totalWidth;
            float clickedLengthInSeconds = clickedRatio * totalLengthAllClips;

            int idx = ScrubberTimelineParts.Count-1;
            while (ScrubberTimelineParts[idx].accumulatedLenghts >= clickedLengthInSeconds)
            {
                idx--;
            }

            AnimationClip clickedClip = ScrubberTimelineParts[idx].clip;

            float rest = clickedLengthInSeconds - ScrubberTimelineParts[idx].accumulatedLenghts;
            
            Debug.Log("Use clip at pos " + idx + " and go to " + rest + "s of it's total length of " + clickedClip.length + "s \n");
            
            StatesManager sm = Main.Instance.GetStatesManager();
            sm.AnimationScrubberJumpRequest(ScrubberTimelineParts[idx].goAnimClip, rest);


        }
    }
}