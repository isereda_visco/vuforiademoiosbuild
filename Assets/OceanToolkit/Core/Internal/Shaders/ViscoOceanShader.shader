﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Ocean Toolkit/Visco Ocean Shader"
{
    Properties
    {
        ot_NormalMap0 ("Normal Map 0", 2D) = "blue" {}
        ot_NormalMap1 ("Normal Map 1", 2D) = "blue" {}
        ot_FoamMap ("Foam Map", 2D) = "white" {}

        ot_AbsorptionCoeffs ("Absorption Coeffs", Vector) = (3.0, 20.0, 50.0, 1.0)
        ot_DetailFalloffStart ("Detail Falloff Start", float) = 60.0
        ot_DetailFalloffDistance ("Detail Falloff Distance", float) = 40.0
        ot_DetailFalloffNormalGoal ("Detail Falloff Normal Goal", float) = 0.2
        ot_AlphaFalloff ("Alpha Falloff", float) = 1.0
        ot_FoamFalloff ("Foam Falloff", float) = 1.5
        ot_FoamStrength ("Foam Strength", float) = 1.2
        ot_FoamAmbient ("Foam Ambient", float) = 0.3
        ot_ReflStrength ("Reflection Strength", float) = 0.7
        ot_RefrStrength ("Refraction Strength", float) = 1.0
        ot_RefrColor ("Refraction Color", Color) = (1.0, 0.0, 0.0, 1.0)
        ot_FresnelPow ("Fresnel Pow", float) = 4.0
        ot_SunColor ("Sun Color", Color) = (1.0, 0.95, 0.6)
        ot_SunPow ("Sun Power", float) = 100.0
        ot_ClipExtents ("Clip Extents", Vector) = (50.0, 50.0, 0.0, 0.0)
		ot_SsrSampleCount ("SSR Sample Count", float) = 16.0
		ot_SsrStride("SSR Stride", float) = 8.0
		ot_SsrWorldDistance ("SSR World Distance", float) = 50.0
		ot_SsrZThickness("SSR Z Thickness", float) = 1.0

			g_WaterbodyLightColor("Waterbody Light Color", Color) = (0.085, 0.25, 0.48, 1.0)
			  g_WaterbodyDarkColor("Waterbody Dark Color", Color) = (0.475, 0.549, 0.796, 1.0)
				g_WaterDensity("Water Density", float) = 0.025
				g_ScatteringOffset("Scattering Offset", float) = 0.02
				g_WaterScatterColor("Water Scatter Color", Color) = (0.58, 0.85, 0.79, 1.0)
				g_WaterColorIntensity("Water Color Intensity", float) = 0.4

			  [Toggle]g_FuturisticStyle("Futuristic Style", float) = 1
			  g_GridThikness("Grid Thikness", float) = 1.0
			  g_FlatShading("Flat Shading", float) = 0.0
			  g_FlatShadingRim("Flat Shading Rim", float) = 0.0
			  g_FrontFadeMin("Front Fade Min", float) = 10.0
			  g_FrontFadeMax("Front Fade Max", float) = 20.0
			  g_WaterFadeMin("Water Fade Min", float) = 800.0
			  g_WaterFadeMax("Water Fade Max", float) = 1200.0
			  g_GridSizeX("Grid Size X", float) = 3.0
			  g_GridSizeY("Grid Size Y", float) = 3.0
			  g_GridIntensity("Grid Intensity", float) = 0
			  g_RefractionDistortScale("Refraction Distort", float) = 0.01
			  g_RimColor("Rim Color", Color) = (0.685, 0.854, 0.992, 1.0)
			  g_RimPower("Rim Power", float) = 20.0
			  g_NormalStrength("Normal Strength", float) = 2.0


	 }

    SubShader
    {
        Tags
        {
            "RenderType"="Transparent"
            "Queue"="Transparent-100"
            "ForceNoShadowCasting"="True"
            "IgnoreProjector"="True"
        }

        GrabPass { "_Refraction" }

        Pass
        {
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Back

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #pragma shader_feature OT_REFL_OFF OT_REFL_SKY_ONLY OT_REFL_SSR
            #pragma shader_feature OT_REFR_OFF OT_REFR_COLOR OT_REFR_NORMAL_OFFSET
			#pragma shader_feature OT_CLIP_OFF OT_CLIP_ON
            #include "CommonOceanToolkit.cginc"

            // Currently set by script, not material
            uniform float3  ot_LightDir;

            uniform float4x4    ot_Proj;
            uniform float4x4    ot_InvView;
            uniform float4      ot_QkCorner0; // view.xyz1 / proj.w
            uniform float4      ot_QkCorner1;
            uniform float4      ot_QkCorner2;
            uniform float4      ot_QkCorner3;

            // Currently set by material
            uniform sampler2D   _Refraction;
            uniform float4      _Refraction_TexelSize;

            uniform sampler2D ot_NormalMap0;
            uniform sampler2D ot_NormalMap1;
            uniform sampler2D ot_FoamMap;

            uniform float4 ot_NormalMap0_ST;
            uniform float4 ot_NormalMap1_ST;
            uniform float4 ot_FoamMap_ST;

            uniform float4  ot_AbsorptionCoeffs;
            uniform float   ot_DetailFalloffStart;
            uniform float   ot_DetailFalloffDistance;
            uniform float   ot_DetailFalloffNormalGoal;
            uniform float   ot_AlphaFalloff;
            uniform float   ot_FoamFalloff;
            uniform float   ot_FoamStrength;
            uniform float   ot_FoamAmbient;

            uniform float   ot_ReflStrength;
            uniform float   ot_RefrStrength;
            uniform float4  ot_RefrColor;
            uniform float   ot_FresnelPow;
            uniform float3  ot_SunColor;
            uniform float   ot_SunPow;
            uniform float2  ot_ClipExtents;
			uniform float	ot_SsrSampleCount;
			uniform float	ot_SsrStride;
			uniform float	ot_SsrWorldDistance;
			uniform float	ot_SsrZThickness;

			uniform float4		g_WaterbodyLightColor;
			uniform float4		g_WaterbodyDarkColor;
			uniform float		g_WaterDensity;
			uniform float		g_ScatteringOffset;
			uniform float4    g_WaterScatterColor;
			uniform float		g_WaterColorIntensity;

			uniform float		g_FuturisticStyle;
			uniform float		g_GridThikness;
			uniform float		g_FlatShading;
			uniform float		g_FlatShadingRim;

			uniform float		g_FrontFadeMin;
			uniform float		g_FrontFadeMax;
			uniform float		g_WaterFadeMin;
			uniform float		g_WaterFadeMax;

			uniform float		g_GridSizeX;
			uniform float		g_GridSizeY;
			uniform float		g_GridIntensity;
			uniform float	   g_RefractionDistortScale;

			uniform float4		g_RimColor;
			uniform float		g_RimPower;
			uniform float		g_NormalStrength;

            struct VertOutput
            {
                float4 position : SV_POSITION;
                float4 screenPos : TEXCOORD0;
                float4 worldPos : TEXCOORD1;
                float4 worldNormal : TEXCOORD2;
				float4 localPos : TEXCOORD3;
            };

            VertOutput vert(appdata_base input)
            {
                VertOutput output;

                float4 left = lerp(ot_QkCorner0, ot_QkCorner3, input.vertex.y);
                float4 right = lerp(ot_QkCorner1, ot_QkCorner2, input.vertex.y);
                float4 viewVertex = lerp(left, right, input.vertex.x);
                viewVertex *= 1.0 / viewVertex.w;

                float4 worldVertex = mul(ot_InvView, viewVertex);

				// Lookup height
				float height;
				float3 worldNormal;
				waveHeight(worldVertex.xyz, height, worldNormal);

				float detailFalloff = saturate((distance(worldVertex.xyz, _WorldSpaceCameraPos) - ot_DetailFalloffStart) / ot_DetailFalloffDistance);
				worldVertex.y = ot_OceanPosition + height * (1.0 - detailFalloff);

				float4 projVertex = mul(UNITY_MATRIX_VP, worldVertex);

                output.position = projVertex;
                output.screenPos = ComputeScreenPos(projVertex);
                output.worldPos = worldVertex;
                output.worldNormal = float4(worldNormal, 1.0);

				output.localPos = mul(unity_WorldToObject, worldVertex);

                return output;
            }

            inline float3 calcReflDir(float3 viewDir, float3 normal)
            {
                float3 reflDir = reflect(viewDir, normal);
				reflDir.y = abs(reflDir.y);
                return reflDir;
            }

				inline float D3DXFresnelTerm(float costheta, float refractionindex)
				{
					float g = sqrt(refractionindex * refractionindex + costheta * costheta);
					float a = g + costheta;
					float d = g - costheta;
					float result = (costheta * a - 1.0) * (costheta * a - 1.0) / ((costheta * d + 1.0) * (costheta * d + 1.0)) + 1.0;
					result = result * 0.5 * d * d / (a * a);
					return result;
				}


            float4 frag(VertOutput input) : SV_Target
            {
                #if defined(OT_CLIP_ON)
                clip(ot_ClipExtents.xy - abs(input.localPos.xz));
                #endif

                float screenZ = input.screenPos.w;
				float3 normal = input.worldNormal.xyz;
				// DEBUG: return float4(normal.x * 0.5 + 0.5, 0.0, normal.z * 0.5 + 0.5, 1.0);

                // Get fine normal from normal maps
                float2 normalUv0 = TRANSFORM_TEX(input.worldPos.xz, ot_NormalMap0);
                float3 fineNormal0 = UnpackNormal(tex2D(ot_NormalMap0, normalUv0)).xzy;
                float2 normalUv1 = TRANSFORM_TEX(input.worldPos.xz, ot_NormalMap1);
                float3 fineNormal1 = UnpackNormal(tex2D(ot_NormalMap1, normalUv1)).xzy;
				float3 fineNormal = fineNormal0 + fineNormal1;

				// Fade normal towards the horizon
				float detailFalloff = saturate((screenZ - ot_DetailFalloffStart) / ot_DetailFalloffDistance);
				fineNormal = normalize(lerp(fineNormal, float3(0.0, 2.0, 0.0), saturate(detailFalloff - ot_DetailFalloffNormalGoal)));
				normal = normalize(lerp(normal, float3(0.0, 1.0, 0.0), detailFalloff));

                // Transform fine normal to world space
                float3 tangent = cross(normal, float3(0.0, 0.0, 1.0));
                float3 bitangent = cross(tangent, normal);
                normal = tangent * fineNormal.x + normal * fineNormal.y + bitangent * fineNormal.z;
				// DEBUG: return float4(normal.xzy * 0.5 + 0.5, 1.0);

					 normal.y /= g_NormalStrength;
					 normal = normalize(normal);


                float3 viewDir = normalize(input.worldPos - _WorldSpaceCameraPos.xyz);
                float3 reflDir = calcReflDir(viewDir, normal);
                float viewDotNormal = saturate(-dot(viewDir, normal));

                // ---
                // Sun
                // ---
				float3 sun = pow(saturate(dot(reflDir, ot_LightDir)), ot_SunPow) * ot_SunColor;

                // ----------
                // Reflection
                // ----------
                float3 refl = float3(0.0, 1.0, 1.0);

                #if defined(OT_REFL_SKY_ONLY)
				refl = sampleSky(reflDir);
                #endif

                #if defined(OT_REFL_SSR)
                float2 hitCoord;
                float hitZ;

                if (raytrace2d(input.worldPos.xyz, reflDir, ot_SsrWorldDistance, UNITY_MATRIX_V, UNITY_MATRIX_P, ot_SsrSampleCount, ot_SsrStride, ot_SsrZThickness, hitCoord, hitZ))
                {
                    #if defined(UNITY_UV_STARTS_AT_TOP)
                    if (_Refraction_TexelSize.y < 0.0 && _ProjectionParams.x >= 0.0)
                    {
                        hitCoord.y = 1.0 - hitCoord.y;
                    }
                    #endif

                    refl = tex2Dlod(_Refraction, float4(hitCoord, 0.0, 0.0)).xyz;
                    sun *= 0.0;
                }
                else
                {
					refl = sampleSky(reflDir);
                }
                #endif

                refl *= ot_ReflStrength;

                // ----------
                // Refraction
                // ----------
                float3 refr = float3(0.0, 1.0, 1.0);

                float2 uv = input.screenPos.xy / input.screenPos.w;
                float depthBelowSurface = sampleDepth(uv) * _ProjectionParams.z - screenZ;
                float refrDepthBelowSurface = depthBelowSurface;

                #if defined(OT_REFR_COLOR)
                refr = ot_RefrColor.xyz;
                #endif

                // ----
                // Foam
                // ----

                // Depth-based
                // Wave-tops, pre-computed depth in the future?
                float foamShade = saturate(ot_FoamAmbient + dot(normal, ot_LightDir));
                float foamMask = 1.0 - pow(saturate(refrDepthBelowSurface / ot_FoamFalloff), 4.0);
                float2 foamUv = TRANSFORM_TEX(input.worldPos.xz, ot_FoamMap);
                float foam = foamMask * ot_FoamStrength * foamShade * tex2D(ot_FoamMap, foamUv).w;

                // ------------------
                // Combine everything
                // ------------------
                float fresnel = 0.0;

                #if defined(OT_REFL_OFF)
                fresnel = 0.0;
                #else
                #if defined(OT_REFR_OFF)
                fresnel = 1.0;
                #else
                fresnel = pow(1.0 - max(viewDotNormal, 0.0), ot_FresnelPow);
                #endif
                #endif

                float3 color = (1.0 - foam) * (fresnel * refl + (1.0 - fresnel) * refr + sun) + foam.xxx;
                float alpha = saturate(depthBelowSurface / ot_AlphaFalloff);

					 //
					 //visco changed
					 //

					 float distanceToCam = length(input.worldPos - _WorldSpaceCameraPos.xyz);
					 float distanceToCamProj = length(input.worldPos - float3(_WorldSpaceCameraPos.x, input.worldPos.y, _WorldSpaceCameraPos.z));

					 float cos_angle = dot(normal, -viewDir);
					 float3 body_color = lerp(g_WaterbodyDarkColor.rgb, g_WaterbodyLightColor.rgb, cos_angle) * g_WaterColorIntensity;

					 if (g_FuturisticStyle != 0)// && g_Underwater == 0)
					 {
						 //rim
						 float distanceCoef = 1.0 - clamp(0.0, 1.0, max(0.01, distanceToCamProj) / (g_WaterFadeMin + g_WaterFadeMax));// 
						 float rim = pow(1.01 - max(0.0, viewDotNormal), g_RimPower);
						 rim = lerp(0.0, 1.0, rim);
						 rim *= distanceCoef;

						 if (g_GridIntensity > 0.0)
						 {
							 float2 sc = input.position.xy / float2(g_GridSizeX, g_GridSizeY);
							 sc = sc - (floor(sc));
							 const float pointsDistance = 0.6;
							 float fp = pointsDistance - clamp(0.0, pointsDistance, 2.0 * distance(float2(sc.x, 0.5), float2(0.5, 0.5)));
							 fp *= distanceCoef;
							 rim *= (fp * g_GridIntensity);
						 }

						 body_color = lerp(body_color, g_RimColor.rgb, rim);

						 float frontFade = clamp(0.05, 1.0, max(1.0, (distanceToCam - g_FrontFadeMin)) / g_FrontFadeMax);
						 //body_color *= frontFade;
					 }

#if defined(OT_REFR_NORMAL_OFFSET)
					 // calc distance from camera to point
					 float fOwnDepth = LinearEyeDepth(input.screenPos.z / input.screenPos.w);
					 // read depth
					 float fSampledDepth = sampleDepth(uv) * _ProjectionParams.z;
					 // obtain water depth
					 float fLinearDepth = abs(fSampledDepth - fOwnDepth);
					 // translate to exponential
					 float fExpDepth = 1.0 - exp(-g_WaterDensity * fLinearDepth);
					 float fExpDepthHIGH = 1.0 - exp(-0.95 * fLinearDepth);

					 // amount of distortion: deepper - more distortion
					 float fDistortScale = g_RefractionDistortScale * fExpDepth;
					 float2 vDistort = normal.zx * fDistortScale; // texcoord offset
																				 // read depth in distorded coords
					 float fDistortedDepth = sampleDepth(uv + vDistort) * _ProjectionParams.z;
					 float waterDensity = /*g_Underwater == 0.0 ? */g_WaterDensity/* : 0.0005*/;
					 // translate to exponential
					 float fDistortedExpDepth = 1.0 - exp(-waterDensity * (fDistortedDepth - fOwnDepth));
					 // now compare distances - if distance to water is bigger than to read depth then skip distortion
					 if (fOwnDepth > fDistortedDepth)
					 {
						 vDistort = (float2)0.0;
						 fDistortedExpDepth = fExpDepth;
					 }
					 
					 float2 refrUv = uv + vDistort;
					 #if defined(UNITY_UV_STARTS_AT_TOP)
					 if (_Refraction_TexelSize.y < 0.0 && _ProjectionParams.x >= 0.0)
					 {
						 refrUv.y = 1.0 - refrUv.y;
					 }
					 #endif

					 refr = tex2D(_Refraction, refrUv).xyz;

					 float oceanFade = fDistortedExpDepth;

					 if (g_FuturisticStyle)
					 {
						 oceanFade = 1.0 - clamp(max(1.0, (distanceToCamProj - g_WaterFadeMin)) / g_WaterFadeMax, 0.0, 1.0);
						 oceanFade = oceanFade * oceanFade;
						 oceanFade = oceanFade * (clamp(fDistortedExpDepth, 0.0, 1.0));
					 }

					 refrDepthBelowSurface = oceanFade;
#endif

					 // paint it with color of water depending on depth
					 body_color = lerp(refr, body_color, saturate(refrDepthBelowSurface.xxx / ot_AbsorptionCoeffs.xyz));
					 body_color *= ot_RefrStrength;

					 color = (1.0 - foam) * (fresnel * refl + (1.0 - fresnel) * body_color + sun) + foam.xxx;

					 //subsurafce scattering

					 if (g_ScatteringOffset > 0.0)// && g_Underwater == 0.0)
					 {
						 // simulating scattering/double refraction: light hits the side of wave, travels some distance in water, and leaves wave on the other side
						 // it's difficult to do it physically correct without photon mapping/ray tracing, so using simple but plausible emulation below

						 float3 lightDirWS = ot_LightDir;

						 // only the crests of water waves generate double refracted light
						 float scatter_factor = max(0, input.localPos.y * g_ScatteringOffset + 0.2);

						 // the waves that lie between camera and light projection on water plane generate maximal amount of double refracted light 
						 scatter_factor *= /*shadow_factor**/pow(max(0.0, dot(normalize(float3(lightDirWS.x, 0.0, lightDirWS.z)), viewDir)), 2.0);

						 // the slopes of waves that are oriented back to light generate maximal amount of double refracted light 
						 scatter_factor *= pow(max(0.0, 1.0 - dot(lightDirWS, normal)), 8.0);

						 // water crests gather more light than lobes, so more light is scattered under the crests
						 scatter_factor *= /*shadow_factor**/max(0, input.localPos.z * g_ScatteringOffset) *
							 // the scattered light is best seen if observing direction is normal to slope surface
							 max(0, dot(-viewDir, normal));

						 // fading scattered light out if viewing direction is vertical to avoid unnatural look
						 scatter_factor *= max(0, 1 + viewDir.y);

#if defined(OT_REFR_NORMAL_OFFSET)
						 // fading scattered light out at distance
						 //scatter_factor *= (300.0 / (300 + fOwnDepth));

						 // fading scatter out by 90% near shores so it looks better
						 //scatter_factor *= 0.1 + 0.9*saturate(fExpDepth);
#endif

						 color += g_WaterScatterColor.rgb * scatter_factor * g_WaterColorIntensity;
					 }
#if defined(OT_REFR_NORMAL_OFFSET)

                //return float4(fLinearDepth.xxx, 1.0);
#endif
					 return float4(color, 1.0);
				}
            ENDCG
        }
    }

    CustomEditor "OceanToolkit.OceanShaderEditor"
}