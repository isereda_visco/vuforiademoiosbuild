﻿Shader "Hidden/ViscoOcean/updateDisplacementPS"
{
	Properties
	{
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "oceanSimulatorCBs.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			// The following three should contains only real numbers. But we have only C2C FFT now.
			StructuredBuffer<float2>	g_InputDxyz;

			float4 frag (v2f i) : SV_Target
			{
				uint index_x = (uint)(i.uv.x * (float)g_OutWidth);
				uint index_y = (uint)(i.uv.y * (float)g_OutHeight);
				uint addr = g_OutWidth * index_y + index_x;

				// cos(pi * (m1 + m2))
				int sign_correction = ((index_x + index_y) & 1) ? -1 : 1;

				float dx = g_InputDxyz[addr + g_DxAddressOffset].x * sign_correction * g_ChoppyScale;
				float dy = g_InputDxyz[addr + g_DyAddressOffset].x * sign_correction * g_ChoppyScale;
				float dz = g_InputDxyz[addr].x * sign_correction;

				return float4(dx, dy, dz, 1);
			}
			ENDCG
		}
	}
}
