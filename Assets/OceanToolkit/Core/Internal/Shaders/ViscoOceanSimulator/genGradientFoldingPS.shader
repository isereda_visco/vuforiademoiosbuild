﻿Shader "Hidden/ViscoOcean/genGradientFoldingPS"
{
	Properties
	{
		g_samplerDisplacementMap("DisplacementMap", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "oceanSimulatorCBs.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			// Textures and sampling states
			sampler2D g_samplerDisplacementMap;

			// Displacement -> Normal, Folding
			float4 frag (v2f i) : SV_Target
			{
				// Sample neighbour texels
				float2 one_texel = float2(1.0f / (float)g_OutWidth, 1.0f / (float)g_OutHeight);

				float2 tc_left = float2(i.uv.x - one_texel.x, i.uv.y);
				float2 tc_right = float2(i.uv.x + one_texel.x, i.uv.y);
				float2 tc_back = float2(i.uv.x, i.uv.y - one_texel.y);
				float2 tc_front = float2(i.uv.x, i.uv.y + one_texel.y);

				float3 displace_left = tex2D(g_samplerDisplacementMap, tc_left).xyz;
				float3 displace_right = tex2D(g_samplerDisplacementMap, tc_right).xyz;
				float3 displace_back = tex2D(g_samplerDisplacementMap, tc_back).xyz;
				float3 displace_front = tex2D(g_samplerDisplacementMap, tc_front).xyz;

				// Do not store the actual normal value. Using gradient instead, which preserves two differential values.
				float2 gradient = { -(displace_right.z - displace_left.z), -(displace_front.z - displace_back.z) };

				// Calculate Jacobian corelation from the partial differential of height field
				float2 Dx = (displace_right.xy - displace_left.xy) * g_ChoppyScale * g_GridLen;
				float2 Dy = (displace_front.xy - displace_back.xy) * g_ChoppyScale * g_GridLen;
				float J = (1.0f + Dx.x) * (1.0f + Dy.y) - Dx.y * Dy.x;

				// Practical subsurface scale calculation: max[0, (1 - J) + Amplitude * (2 * Coverage - 1)].
				float fold = max(1.0f - J, 0);

				// Output
				return float4(gradient, 0, fold);
			}
			ENDCG
		}
	}
}
