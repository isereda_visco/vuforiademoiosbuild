﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClippPlaneTools : MonoBehaviour {


    public GameObject MeshRootObject;
    public GameObject Quad;

    private List<GameObject> nodeList = new List<GameObject>();
    

    // Use this for initialization
    void Start () {
        nodeList.AddRange(FindChildrenByNameSubstring(MeshRootObject));

        foreach(GameObject g in nodeList )
        {
            
            g.AddComponent<OnePlaneCuttingController>();
            OnePlaneCuttingController ctlr = g.GetComponent<OnePlaneCuttingController>();
            ctlr.plane = Quad;
        }
    }


    //     Update is called once per frame
	void Update () {

	}




    private List<GameObject> FindChildrenByNameSubstring(GameObject parentNode)
    {
        List<GameObject> result = new List<GameObject>();
        foreach (Transform childTransform in parentNode.transform)
        {
            GameObject go = childTransform.gameObject;
            if (go.GetComponent<MeshRenderer>())
            {
                result.Add(go);
            }
            result.AddRange(FindChildrenByNameSubstring(go));
        }

        return result;
    }

}
